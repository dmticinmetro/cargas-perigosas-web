<%@ page errorPage="/main/error.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- <c:import url="/include/check_login.jsp" /> -->
<c:import url="/include/header.jsp" />
<c:import url="/include/oia/left.jsp" />

<div class="main">

<p>Arquivo ZIP</p>
<form action="/cargasperigosas/UploadControl" method="post" enctype="multipart/form-data">
    <p>Arquivo:</p>
    <p><input type="file" name="file" /></p>
    <p><input type="submit" value="Enviar Inspe��o"/></p>
</form>

<p>Arquivo XML</p>
<form action="/cargasperigosas/UploadXmlControl" method="post">
    <p>Arquivo:</p>
    <p><input type="file" name="file" /></p>
    <p><input type="submit" value="Enviar Inspe��o"/></p>
</form>

<tr><td align="center">
	<c:if test="${requestScope.msg != null}">
		<p><c:out value="${requestScope.msg}" /></p>
	</c:if>
</td></tr>

</div>

<c:import url="/include/oia/right.jsp" />
<c:import url="/include/footer.jsp" />
