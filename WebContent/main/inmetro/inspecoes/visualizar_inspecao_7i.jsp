<%@page import="util.FormatUtil"%>
<%@page import="model.EnsaioValvulas7i"%>
<%@page import="model.Inspecao7i"%>
<%@page import="control.ErrorControl"%>
<%@ page errorPage="/main/error.jsp" %>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Head"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

<!-- <c:import url="/include/check_login.jsp" /> -->
<c:import url="/include/header.jsp" />
<c:import url="/include/inmetro/left.jsp" />
<tr><td>

<div class="main">

<%
	int cont = 0;
	Inspecao7i inspecao = new Inspecao7i();	
	boolean flag = false;
	session = request.getSession();
	if(session.getAttribute("inspecao") != null){
		inspecao = (Inspecao7i) session.getAttribute("inspecao");
		flag = true;
	}else if(request.getAttribute("inspecao") != null){
		inspecao = (Inspecao7i) request.getAttribute("inspecao");
		flag = true;
	}
	if(flag){
		if(request.getAttribute("msg") != null)	out.print("<p class='msg'>" + request.getAttribute("msg") + "</p>");
%>

<h4 align="center">Inspe��o 7i</h4>

<div class="inspecao">

<table class="inspecao">
	<tr><td class="marco" align="center" width="400">
		<table>
			<tr><td align="center" width="400">Identifica��o da Inspe��o</td></tr>
			<tr><td align="center"><table border="1">
				<tr><th width="120">OIA</th><td width="280"><%= inspecao.getHead().getOia() %></td></tr>
				<tr><th>CIPP</th><td><%= inspecao.getHead().getCipp() %></td></tr>
				<tr><th>Tipo de Ensaio</th><td><%= inspecao.getHead().getTipoEnsaio() %></td></tr>
				<tr><th>Relat�rio/RNC</th><td><%= inspecao.getHead().getRelatorio() %></td></tr>
				<tr><th>Data da Inspe��o</th><td><%= FormatUtil.formatDate(inspecao.getHead().getDataInspecao()) %></td></tr>
				<tr><th>Data da Pr�xima Inspe��o</th><td><%= FormatUtil.formatDate(inspecao.getHead().getDataProximaInspecao()) %></td></tr>
				<tr><th>Lacre</th><td><%= inspecao.getHead().getLacre() %></td></tr>
				<tr><th>Supervisor</th><td><%= inspecao.getHead().getSupervisor() %></td></tr>
				<tr><th>Inspetor</th><td><%= inspecao.getHead().getInspetor() %></td></tr>
				<tr><th>Cliente</th><td><%= inspecao.getHead().getCliente() %></td></tr>
				<tr><th>Local da Inspe��o</th><td><%= inspecao.getHead().getLocalInspecao() %></td></tr>
				<tr><th>N�mero do Equipamento</th><td><%= inspecao.getHead().getEquipamento() %></td></tr>
				<tr><th>N�mero do Renavam</th><td><%=inspecao.getHead().getRenavam()%></td></tr>
				<tr><th>N�mero do Chassi</th><td><%= inspecao.getHead().getChassi() %></td></tr>
				<tr><th>Placa do Ve�culo</th><td><%= inspecao.getHead().getPlaca() %></td></tr>
			</table></td></tr>
			<tr><td align="center">
				<table>
					<tr><td align="center" width="400">Ensaio V�lvulas</td></tr>
					<tr><td align="center"><table border="1">
					<%
						int i = 1;
						for(EnsaioValvulas7i e : inspecao.getEnsaioValvulas7i()){
					%>
							<tr><th>V�lvula <%= i %></th></tr>
							<tr><th width="350">V�lvula de Al�vio - Press�o Abertura</th><td width="50"><%= e.getValvulaAlivio().getPressaoAbertura() %></td></tr>
							<tr><th>V�lvula de Al�vio - Press�o Fechamento</th><td><%= e.getValvulaAlivio().getPressaoFechamento() %></td></tr>
							<tr><th>V�lvula de V�cuo - Press�o Abertura</th><td><%= e.getValvulaVacuo().getPressaoAbertura() %></td></tr>
							<tr><th>V�lvula de V�cuo - Press�o M�xima</th><td><%= e.getValvulaVacuo().getPressaoMaxima() %></td></tr>
					<%
							i++;
						}
					%>
					</table></td></tr>
				</table>
			</td></tr>
			
		</table>
	</td><td class="marco" align="center" width="400">
		<table class="marco"><tr><td align="center">
			<table>
				<tr><td align="center" width="400">Identifica��o do Equipamento</td></tr>
				<tr><td align="center"><table border="1">
					<tr><th width="200">Fabricante</th><td width="200"><%= inspecao.getMarcoIdentificacaoEquipamento7i().getFabricante() %></td></tr>
					<tr><th>Data da Fabrica��o</th><td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getDataFabricacao() %></td></tr>
					<tr><th>Tipo do Equipamento</th><td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getTipo() %></td></tr>
					<tr><th>Press�o do Projeto</th><td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getPressaoProjeto() %></td></tr>
					<tr><th>Press�o do Ensaio de Press�o</th><td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getPressaoEnsaio() %></td></tr>
					<tr><th>Material do Costado</th><td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getMaterialCostado() %></td></tr>
					<tr><th>Material da Calotas</th><td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getMaterialCalotas() %></td></tr>
					<tr><th>N�mero de Compartimentos</th><td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getNumCompartimentos() %></td></tr>
					<tr><th>Tanque Cil�ndrico</th><td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getTanqueCilindrico() %></td></tr>
					<tr><th>Diametro Interno</th><td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getDiametroInterno() %></td></tr>
					<tr><th>Tanque Polic�ntrico</th><td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getTanquePolicentrico() %></td></tr>
					<tr><th>Raio de Curvatura</th><td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getRaioCurvatura() %></td></tr>
					<tr><th>Tanque Revestido</th><td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getTanqueRevestido() %></td></tr>
					<tr><th>Espessura do Costado</th><td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getEspessuraCostado() %></td></tr>
					<tr><th>Espessura da Calotas</th><td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getEspessuraCalotas() %></td></tr>
					<tr><th>Sobreespessura de Corrosao</th><td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getSobreespessuraCorrosao() %></td></tr>
					<tr><th>Volume do Tanque</th><td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getVolumeTanque() %></td></tr>
				</table></td></tr>
				<tr><td align="center">
				<table class="marco">
				<tr><td align="center" width="400">Ensaio Hidrost�tico / Pneum�tico</td></tr>
				<tr><td align="center"><table border="1">
						<tr><th width="200">Press�o de Ensaio</th><td width="200"><%= inspecao.getMarcoEnsaioHidrostaticoPneumatico7i().getPressaoEnsaio() %></td></tr>
						<tr><th>Ensaio Executado</th><td><%= inspecao.getMarcoEnsaioHidrostaticoPneumatico7i().getEnsaioExecutado() %></td></tr>
						<tr><th>Resultado</th><td><%= inspecao.getMarcoEnsaioHidrostaticoPneumatico7i().getResultadoEnsaio() %></td></tr>
						<tr><th>Tempo de Dura��o</th><td><%= inspecao.getMarcoEnsaioHidrostaticoPneumatico7i().getTempoDuracao() %></td></tr>
					</table></td></tr>
				</table>
				</td></tr>
				<tr><td align="center">
					<table>
						<tr><td align="center" width="400">Apto a Transportar Grupos</td></tr>
						<tr><td align="center"><table border="1">
							<tr>
							<%
								if(inspecao.getAptoTransportarGrupos() != null){
									for(String g : inspecao.getAptoTransportarGrupos().getGrupo()){
										if(cont == 6){
							%>
											</tr><tr><td class="grupo" width="50"><%= g %></td>
							<%
											cont = 0;
										}else{
							%>
											<td class="grupo" width="50"><%= g %></td>
							<%
										}
										cont++;
									}
								}else{
							%>
										<td>Nenhum grupo cadastrado</td>
							<%
								}
							%>
							</tr>
						</table></td></tr>
					</table>
				</td></tr>
			</table>
		</td></tr></table>
	</td></tr>
</table>

<table class="marco">
	<tr><td align="center" width="800">Imagens da Inspe��o</td></tr>
	<tr><td align="center"><table border="1">
		<tr>
			<th width="100">Imagem Panor�mica do Equipamento</th>
			<th width="100">Imagem da Placa do Equipamento</th>
			<th width="100">Imagem Inicial Ensaio Hidrost�tico / Pneum�tico</th>
			<th width="100">Imagem Final Ensaio Hidrost�tico / Pneum�tico</th>
			<th width="100">Imagem Externa da Inspe��o</th>
			<th width="100">Imagem Interna Inicial da Inspe��o</th>
			<th width="100">Imagem Interna Final da Inspe��o</th>
		</tr>
		<tr>
			<%
				String repositoryPath = "C:\\CargasPerigosasWeb\\InspecoesValidadas\\";
		        String errorPath = "C:\\CargasPerigosasWeb\\InspecoesNaoValidadas\\";
				for(String s : inspecao.getTodasEvidencias()){
					s = repositoryPath + inspecao.getHead().getCipp() + "\\" + s;
			%>
					<td align="center"><a href="#"><img alt="" src="<%= s %>" width="100" /></a></td>
			<%
				}
			%>
		</tr>
	</table></td></tr>
</table>

</div>

<%
	}else{
%>
	<p>Inspe��o inv�lida ou ocorreu um erro.</p>
<%
	}
%>
</div>

</td></tr>

<tr><td align="center">
	<p><input type="button" value="Voltar" onclick="history.go(-1)"/></p>
</td></tr>


<c:import url="/include/inmetro/right.jsp" />
<c:import url="/include/footer.jsp" />