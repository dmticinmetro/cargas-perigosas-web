<%@page import="util.FormatUtil"%>
<%@page import="java.io.File"%>
<%@page import="model.Inspecao1i"%>
<%@page import="control.ErrorControl"%>
<%@ page errorPage="/main/error.jsp" %>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Head"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

<!-- <c:import url="/include/check_login.jsp" /> -->
<c:import url="/include/header.jsp" />
<c:import url="/include/inmetro/left.jsp" />
<tr><td>

<div class="main">

<%
	int cont = 0;
	Inspecao1i inspecao = new Inspecao1i();	
	boolean flag = false;
	session = request.getSession();
	if(session.getAttribute("inspecao") != null){
		inspecao = (Inspecao1i) session.getAttribute("inspecao");
		flag = true;
	}else if(request.getAttribute("inspecao") != null){
		inspecao = (Inspecao1i) request.getAttribute("inspecao");
		flag = true;
	}
	if(flag){
		if(request.getAttribute("msg") != null)	out.print("<p class='msg'>" + request.getAttribute("msg") + "</p>");
%>

<h4 align="center">Inspe��o 1i</h4>

<div class="inspecao">

<table class="inspecao">
	<tr><td class="marco" align="center" width="400">
		<table>
			<tr><td align="center" width="400">Identifica��o da Inspe��o</td></tr>
			<tr><td><table border="1">
				<tr><th width="120">OIA</th><td width="280"><%= inspecao.getHead().getOia() %></td></tr>
				<tr><th>CIPP</th><td><%= inspecao.getHead().getCipp() %></td></tr>
				<tr><th>Tipo de Ensaio</th><td><%= inspecao.getHead().getTipoEnsaio() %></td></tr>
				<tr><th>Relat�rio/RNC</th><td><%= inspecao.getHead().getRelatorio() %></td></tr>
				<tr><th>Data da Inspe��o</th><td><%= FormatUtil.formatDate(inspecao.getHead().getDataInspecao()) %></td></tr>
				<tr><th>Data da Pr�xima Inspe��o</th><td><%= FormatUtil.formatDate(inspecao.getHead().getDataProximaInspecao()) %></td></tr>
				<tr><th>Lacre</th><td><%= inspecao.getHead().getLacre() %></td></tr>
				<tr><th>Supervisor</th><td><%= inspecao.getHead().getSupervisor() %></td></tr>
				<tr><th>Inspetor</th><td><%= inspecao.getHead().getInspetor() %></td></tr>
				<tr><th>Cliente</th><td><%= inspecao.getHead().getCliente() %></td></tr>
				<tr><th>Local da Inspe��o</th><td><%= inspecao.getHead().getLocalInspecao() %></td></tr>
				<tr><th>N�mero do Equipamento</th><td><%= inspecao.getHead().getEquipamento() %></td></tr>
				<tr><th>N�mero do Renavam</th><td><%=inspecao.getHead().getRenavam()%></td></tr>
				<tr><th>N�mero do Chassi</th><td><%= inspecao.getHead().getChassi() %></td></tr>
				<tr><th>Placa do Ve�culo</th><td><%= inspecao.getHead().getPlaca() %></td></tr>
			</table></td></tr>
			<tr><td align="center">
				<table class="marco">
					<tr><td align="center" width="400">Enpessuras M�nimas</td></tr>
					<tr><td><table border="1">
						<tr><th width="350">Espessura M�nima do Costado</th><td width="50"><%= inspecao.getEspessurasMinimas().getEspMinCostado() %></td></tr>
						<tr><th>Espessura M�nima da Calotas Dianteira</th><td><%= inspecao.getEspessurasMinimas().getEspMinCalotasDianteira() %></td></tr>
						<tr><th>Espessura M�nima da Calotas Traseira</th><td><%= inspecao.getEspessurasMinimas().getEspMinCalotasTraseira() %></td></tr>
						<tr><th>Espessura M�nima da Tampa de Boca de Vista</th><td><%= inspecao.getEspessurasMinimas().getEspMinTampaBoca() %></td></tr>
					</table></td></tr>
				</table>	
			</td></tr>
			
		</table>
	</td><td class="marco" align="center" width="400">
		<table class="marco"><tr><td align="center">
			<table>
				<tr><td align="center" width="400">Identifica��o do Equipamento</td></tr>
				<tr><td><table border="1">
					<tr><th width="200">Press�o do Projeto</th><td width="200"><%= inspecao.getIdentificacaoEquipamento().getPressaoProjeto() %></td></tr>
					<tr><th>Press�o do Ensaio Hidrost�tico</th><td><%= inspecao.getIdentificacaoEquipamento().getPressaoEnsaioHidrostatico() %></td></tr>
					<tr><th>Material do Costado</th><td><%= inspecao.getIdentificacaoEquipamento().getMaterialCostado() %></td></tr>
					<tr><th>Material da Calotas</th><td><%= inspecao.getIdentificacaoEquipamento().getMaterialCalotas() %></td></tr>
					<tr><th>Norma de Fabrica��o</th><td><%= inspecao.getIdentificacaoEquipamento().getNormaFabricacao() %></td></tr>
					<tr><th>Temperatura de Projeto</th><td><%= inspecao.getIdentificacaoEquipamento().getTemperaturaProjeto() %></td></tr>
					<tr><th>Press�o de Opera��o</th><td><%= inspecao.getIdentificacaoEquipamento().getPressaoOperacao() %></td></tr>
					<tr><th>Espessura da Costado</th><td><%= inspecao.getIdentificacaoEquipamento().getEspessuraCostado() %></td></tr>
					<tr><th>Espessura da Calotas</th><td><%= inspecao.getIdentificacaoEquipamento().getEspessuraCalotas() %></td></tr>
					<tr><th>Di�metro Interno</th><td><%= inspecao.getIdentificacaoEquipamento().getDiametroInterno() %></td></tr>
					<tr><th>Press�o de Abertura da V�lvula de Seguran�a</th><td><%= inspecao.getIdentificacaoEquipamento().getPressaoAberturaValvulaSeg() %></td></tr>
					<tr><th>Comprimento do Tanque</th><td><%= inspecao.getIdentificacaoEquipamento().getComprimentoTanques() %></td></tr>
					<tr><th>Capacidade Geom�trica</th><td><%= inspecao.getIdentificacaoEquipamento().getCapacidadeGeometrica() %></td></tr>
				</table></td></tr>
				<tr><td align="center">
				<table>
					<tr><td align="center" width="400">V�lvulas de Al�vio</td></tr>
					<tr><td><table border="1">
						<tr><th width="180">Al�vio de Fechamento</th><td width="220"><%= inspecao.getValvulaAlivio().getAlivioFechamento() %></td></tr>
						<tr><th>Al�vio de Abertura</th><td><%= inspecao.getValvulaAlivio().getAlivioAbertura() %></td></tr>
						<tr><th>Nome do Laborat�rio</th><td><%= inspecao.getValvulaAlivio().getNomeLaboratorio() %></td></tr>
					</table></td></tr>
				</table>
			</td></tr>
			<tr><td align="center">
				<table>
					<tr><td align="center" width="400">Apto a Transportar Grupos</td></tr>
					<tr><td align="center"><table border="1">
						<tr>
						<%
							if(inspecao.getAptoTransportarGrupos() != null){
								for(String g : inspecao.getAptoTransportarGrupos().getGrupo()){
									if(cont == 6){
						%>
										</tr><tr><td class="grupo" width="50"><%= g %></td>
						<%
										cont = 0;
									}else{
						%>
										<td class="grupo" width="50"><%= g %></td>
						<%
									}
									cont++;
								}
							}else{
						%>
									<td>Nenhum grupo cadastrado</td>
						<%
							}
						%>
						</tr>
					</table></td></tr>
				</table>
			</td></tr>
			</table>
		</td></tr></table>
</table>

<table class="marco">
	<tr><td align="center" width="800">Imagens da Inspe��o</td></tr>
	<tr><td align="center"><table border="1">
		<tr>
			<th width="150">Imagem Panor�mica do Equipamento</th>
			<th width="150">Imagem da Placa do Equipamento</th>
			<th width="150">Imagem Externa da Inspe��o</th>
			<th width="150">Imagem Interna Inicial da Inspe��o</th>
			<th width="150">Imagem Interna Final da Inspe��o</th>
		</tr>
		<tr>
			<%
				String repositoryPath = "C:\\CargasPerigosasWeb\\InspecoesValidadas\\";
		        String errorPath = "C:\\CargasPerigosasWeb\\InspecoesNaoValidadas\\";
				for(String s : inspecao.getTodasEvidencias()){
					s = repositoryPath + inspecao.getHead().getCipp() + "\\" + s;
			%>
					<td align="center"><a href="#"><img alt="" src="./cargasperigosas/Desert.jpg" width="150" /></a></td>
			<%
					System.out.println(s);
				}
			%>
		</tr>
	</table></td></tr>
</table>

</div>

<%
	}else{
%>
	<p>Inspe��o inv�lida ou ocorreu um erro.</p>
<%
	}
%>
</div>

</td></tr>

<tr><td align="center">
	<p><input type="button" value="Voltar" onclick="history.go(-1)"/></p>
</td></tr>


<c:import url="/include/inmetro/right.jsp" />
<c:import url="/include/footer.jsp" />