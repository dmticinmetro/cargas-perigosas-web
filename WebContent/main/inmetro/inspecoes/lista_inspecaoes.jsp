<%@page import="model.Head"%>
<%@page import="model.Inspecao7i"%>
<%@page import="model.Inspecao6i"%>
<%@page import="util.FormatUtil"%>
<%@page import="control.ErrorControl"%>
<%@page import="model.Inspecao1i"%>
<%@page import="model.Inspecao3i"%>
<%@page import="java.util.ArrayList"%>
<%@ page errorPage="/main/error.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- <c:import url="/include/check_login.jsp" /> -->
<c:import url="/include/header.jsp" />
<c:import url="/include/inmetro/left.jsp" />

<tr><td class="tituloNoticiaHP" width="100%" height="17" align="center">

<%
	try{
		ArrayList<Inspecao1i> insp = (ArrayList<Inspecao1i>) request.getAttribute("lista1i");
		System.out.print("Inspecao: " + insp);
		System.out.print("Quant.: " + insp.size());
	}catch(Exception e){
		new ErrorControl(e);
	}
%>

<div class="main">
	<table border="1" class="lista_inspecao">
		<tr>
			<th class="listaInspecao" width="60">CIPP</th>
			<th class="listaInspecao" width="360">OIA</th>
			<th class="listaInspecao" width="30">Tipo</th>
			<th class="listaInspecao" width="100">Data da Inspe��o</th>
			<th class="listaInspecao" width="50"></th>
		</tr>
		
		<% for(Head head : (ArrayList<Head>) request.getAttribute("head")){ %>
			<tr>
				<td class="listaInspecao"><%= head.getCipp() %></td>
				<td class="listaInspecao"><%= head.getOia() %></td>
				<td class="listaInspecao"><%= head.getTipoEnsaio() %></td>
				<td class="listaInspecao"><%= FormatUtil.formatDate(head.getDataInspecao()) %></td>
				<td class="listaInspecao"><a href="LinkControl?action=visualizar_inspecao_<%= head.getTipoEnsaio() %>_inmetro&cipp=<%= head.getCipp() %>&head=<%= head.getId() %>">Visualizar</a></td>
			</tr>
		<% } %>
	</table>
</div>

</td></tr>

<tr><th width="100%" align="center"><p>Refinamento</p></th></tr>
<tr><td width="100%" align="center"  width="600">
	<table><tr><td width="300" align="center">
		<form action="Control" method="post">
			<input type="hidden" name="action" value="refinarListaInspecoes">
				Tipo de Inspe��o: <select name="inspecao">
						      	      <option value="0" <c:if test="${requestScope.inspecao == 0} ">SELECTED</c:if> >Todos</option>
						      		  <option value="7" <c:if test="${requestScope.inspecao == 7}">SELECTED</c:if>>Inspe��o 7i</option>
							  		  <option value="6" <c:if test="${requestScope.inspecao == 6}">SELECTED</c:if>>Inspe��o 6i</option>
							  	 	  <option value="3" <c:if test="${requestScope.inspecao == 3}">SELECTED</c:if>>Inspe��o 3i</option>
							  		  <option value="1" <c:if test="${requestScope.inspecao == 1}">SELECTED</c:if>>Inspe��o 1i</option>
						  	      </select>
			</td><td width="300" align="center">
				OIA: <input type="text" name="oia" value='<c:out value="${requestScope.oia}" />' />
			</td>
			<!--
			<tr><td align="center">
				Data Inicio: <select name="diainicio">
							     <option value="0">Dia</option>
							     <% for(int i = 1; i <= 31; i++){ %><option value="<%= i %>"><%= i %></option><% } %>
							 </select>
							 <select name="mesinicio">
							     <option value="0">M�s</option>
							     <% for(int i = 1; i <= 12; i++){ %> <option value="<%= i %>"><%= i %></option><% } %>
							 </select>
							 <select name="anoinicio">
							     <option value="0">Ano</option>
							     <% for(int i = 2010; i <= 2015; i++){ %> <option value="<%= i %>"><%= i %></option><% } %>
							 </select>
			</td><td align="center">
				Data Fim: <select name="diafim">
						      <option value="0">Dia</option>
						      <% for(int i = 1; i <= 31; i++){ %> <option value="<%= i %>"><%= i %></option><% } %>
						  </select>
						  <select name="mesfim">
						      <option value="0">M�s</option>
						      <% for(int i = 1; i <= 12; i++){ %> <option value="<%= i %>"><%= i %></option><% } %>
						  </select>
						  <select name="anofim">
						      <option value="0">Ano</option>
						      <% for(int i = 2010; i <= 2015; i++){ %> <option value="<%= i %>"><%= i %></option><% } %>
						  </select>
			</td></tr>
			-->
	</table>
</td></tr>
<tr><td align="center">
	<input type="submit" value="Refinar" />
	</form>
</td></tr>


<tr><td align="center">
	<c:if test="${requestScope.msg != null}">
		<h3><c:out value="${requestScope.msg}" /></h3>
	</c:if>
</td></tr>

<tr><td align="center">
	<p><input type="button" value="Voltar" onclick="history.go(-1)" /></p>
</td></tr>

<c:import url="/include/inmetro/right.jsp" />
<c:import url="/include/footer.jsp" />