<%@page import="util.FormatUtil"%>
<%@page import="model.Inspecao6i"%>
<%@page import="control.ErrorControl"%>
<%@ page errorPage="/main/error.jsp" %>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Head"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

<!-- <c:import url="/include/check_login.jsp" /> -->
<c:import url="/include/header.jsp" />
<c:import url="/include/inmetro/left.jsp" />
<tr><td>

<div class="main">

<%
	int cont = 0;
	Inspecao6i inspecao = new Inspecao6i();	
	boolean flag = false;
	session = request.getSession();
	if(session.getAttribute("inspecao") != null){
		inspecao = (Inspecao6i) session.getAttribute("inspecao");
		flag = true;
	}else if(request.getAttribute("inspecao") != null){
		inspecao = (Inspecao6i) request.getAttribute("inspecao");
		flag = true;
	}
	if(flag){
		if(request.getAttribute("msg") != null)	out.print("<p class='msg'>" + request.getAttribute("msg") + "</p>");
%>

<h4 align="center">Inspe��o 6i</h4>

<div class="inspecao">

<table class="inspecao">
	<tr><td class="marco" align="center" width="400">
		<table>
			<tr><td align="center" width="400">Identifica��o da Inspe��o</td></tr>
			<tr><td align="center"><table border="1">
				<tr><th width="120">OIA</th><td width="280"><%= inspecao.getHead().getOia() %></td></tr>
				<tr><th>CIPP</th><td><%= inspecao.getHead().getCipp() %></td></tr>
				<tr><th>Tipo de Ensaio</th><td><%= inspecao.getHead().getTipoEnsaio() %></td></tr>
				<tr><th>Relat�rio/RNC</th><td><%= inspecao.getHead().getRelatorio() %></td></tr>
				<tr><th>Data da Inspe��o</th><td><%= FormatUtil.formatDate(inspecao.getHead().getDataInspecao()) %></td></tr>
				<tr><th>Data da Pr�xima Inspe��o</th><td><%= FormatUtil.formatDate(inspecao.getHead().getDataProximaInspecao()) %></td></tr>
				<tr><th>Lacre</th><td><%= inspecao.getHead().getLacre() %></td></tr>
				<tr><th>Supervisor</th><td><%= inspecao.getHead().getSupervisor() %></td></tr>
				<tr><th>Inspetor</th><td><%= inspecao.getHead().getInspetor() %></td></tr>
				<tr><th>Cliente</th><td><%= inspecao.getHead().getCliente() %></td></tr>
				<tr><th>Local da Inspe��o</th><td><%= inspecao.getHead().getLocalInspecao() %></td></tr>
				<tr><th>N�mero do Equipamento</th><td><%= inspecao.getHead().getEquipamento() %></td></tr>
				<tr><th>N�mero do Renavam</th><td><%=inspecao.getHead().getRenavam()%></td></tr>
				<tr><th>N�mero do Chassi</th><td><%= inspecao.getHead().getChassi() %></td></tr>
				<tr><th>Placa do Ve�culo</th><td><%= inspecao.getHead().getPlaca() %></td></tr>
			</table></td></tr>
			<tr><td align="center">
				<table class="marco">
					<tr><td align="center" width="400">Identifica��o do Equipamento</td></tr>
					<tr><td align="center"><table border="1">
						<tr><th width="160">Press�o do Projeto</th><td width="240"><%= inspecao.getIdentificacaoEquipamento().getPressaoProjeto() %></td></tr>
						<tr><th>Press�o do Ensaio Hidrost�tico</th><td><%= inspecao.getIdentificacaoEquipamento().getPressaoEnsaioHidrostatico() %></td></tr>
						<tr><th>Material do Costado</th><td><%= inspecao.getIdentificacaoEquipamento().getMaterialCostado() %></td></tr>
						<tr><th>Material da Calotas</th><td><%= inspecao.getIdentificacaoEquipamento().getMaterialCalotas() %></td></tr>
						<tr><th>Norma de Fabrica��o</th><td><%= inspecao.getIdentificacaoEquipamento().getNormaFabricacao() %></td></tr>
						<tr><th>Di�metro do Tanque</th><td><%= inspecao.getIdentificacaoEquipamento().getDiametroTanque() %></td></tr>
						<tr><th>Comprimento do Tanque</th><td><%= inspecao.getIdentificacaoEquipamento().getComprimentoTanque() %></td></tr>
						<tr><th>Espessura da Costado</th><td><%= inspecao.getIdentificacaoEquipamento().getEspessuraCostado() %></td></tr>
						<tr><th>Espessura da Calotas</th><td><%= inspecao.getIdentificacaoEquipamento().getEspessuraCalotas() %></td></tr>
						<tr><th>Sobreespessura de Corros�o</th><td><%= inspecao.getIdentificacaoEquipamento().getSobreespessuraCorrosao() %></td></tr>
						<tr><th>Volume do Tanque</th><td><%= inspecao.getIdentificacaoEquipamento().getVolumeTanque() %></td></tr>
						<tr><th>Radiografia</th><td><%= inspecao.getIdentificacaoEquipamento().getRadiografia() %></td></tr>
						<tr><th>Ensaio N�o-Destrutivo</th><td><%= inspecao.getIdentificacaoEquipamento().getEnsaioNaoDestrutivo() %></td></tr>
						<tr><th>Al�vio de Tens�es</th><td><%= inspecao.getIdentificacaoEquipamento().getAlivioTensoes() %></td></tr>
					</table></td></tr>
					<tr><td align="center" width="400">Ensaio Hidrost�tico</td></tr>
					<tr><td align="center"><table border="1">
						<tr><th width="220">Press�o de Ensaio</th><td width="180"><%= inspecao.getEnsaioHidrostatico().getPressaoEnsaio() %></td></tr>
						<tr><th>Tempo de Dura��o</th><td><%= inspecao.getEnsaioHidrostatico().getDuracao() %></td></tr>
						<tr><th>N� Man�metro 1</th><td><%= inspecao.getEnsaioHidrostatico().getManometro1() %></td></tr>
						<tr><th>Validade do Man�metro 1</th><td><%= inspecao.getEnsaioHidrostatico().getValidadeManometro1() %></td></tr>
						<tr><th>N� Man�metro 2</th><td><%= inspecao.getEnsaioHidrostatico().getManometro2() %></td></tr>
						<tr><th>Validade do Man�metro 2</th><td><%= inspecao.getEnsaioHidrostatico().getValidadeManometro2() %></td></tr>
					</table></td></tr>
				</table>	
			</td></tr>
			<tr><td align="center">
				<table>
					<tr><td align="center" width="400">Apto a Transportar Grupos</td></tr>
					<tr><td align="center"><table border="1">
						<tr>
						<%
							if(inspecao.getAptoTransportarGrupos() != null){
								for(String g : inspecao.getAptoTransportarGrupos().getGrupo()){
									if(cont == 6){
						%>
										</tr><tr><td class="grupo" width="50"><%= g %></td>
						<%
										cont = 0;
									}else{
						%>
										<td class="grupo" width="50"><%= g %></td>
						<%
									}
									cont++;
								}
							}else{
						%>
									<td>Nenhum grupo cadastrado</td>
						<%
							}
						%>
						</tr>
					</table></td></tr>
				</table>
			</td></tr>
		</table>
	</td><td class="marco" align="center" width="400">
		<table class="marco"><tr><td align="center">
			<table>
				<tr><td align="center">
				<table>
					<tr><td align="center" width="400">Ensaio V�lvulas</td></tr>
					<tr><td align="center"><table border="1">
						<tr><th width="250">V�lvula de Al�vio - Press�o Abertura 1</th><td width="150"><%= inspecao.getValvulas().getAlivioPressaoAbertura1() %></td></tr>
						<tr><th>V�lvula de Al�vio - Press�o Abertura Total 1</th><td><%= inspecao.getValvulas().getAlivioPressaoAberturaTotal1() %></td></tr>
						<tr><th>V�lvula de Al�vio - Press�o Fechamento 1</th><td><%= inspecao.getValvulas().getAlivioPressaoFechamento1() %></td></tr>
						<tr><th>V�lvula de Al�vio - Press�o Abertura 1</th><td><%= inspecao.getValvulas().getAlivioPressaoAbertura2() %></td></tr>
						<tr><th>V�lvula de Al�vio - Press�o Abertura Total 1</th><td><%= inspecao.getValvulas().getAlivioPressaoAberturaTotal2() %></td></tr>
						<tr><th>V�lvula de Al�vio - Press�o Fechamento 1</th><td><%= inspecao.getValvulas().getAlivioPressaoFechamento1() %></td></tr>
						<tr><th>V�lvula de Al�vio - Nome do Laborat�rio 1</th><td><%= inspecao.getValvulas().getAlivioNomeLaboratorio() %></td></tr>
						<tr><th>V�lvula de Al�vio - N� do Certificado 1</th><td><%= inspecao.getValvulas().getAlivioCertificado1() %></td></tr>
						<tr><th>V�lvula de Al�vio - N� do Certificado 2</th><td><%= inspecao.getValvulas().getAlivioCertificado2() %></td></tr>
						<tr><th>V�lvula de Seguran�a - Press�o Abertura 1</th><td><%= inspecao.getValvulas().getSegurancaPressaoAbertura1() %></td></tr>
						<tr><th>V�lvula de Seguran�a - Press�o Abertura Total 1</th><td><%= inspecao.getValvulas().getSegurancaPressaoAberturaTotal1() %></td></tr>
						<tr><th>V�lvula de Seguran�a - Press�o Fechamento 1</th><td><%= inspecao.getValvulas().getSegurancaPressaoFechamento1() %></td></tr>
						<tr><th>V�lvula de Seguran�a - Press�o Abertura 2</th><td><%= inspecao.getValvulas().getSegurancaPressaoAbertura2() %></td></tr>
						<tr><th>V�lvula de Seguran�a - Press�o Abertura Total 2</th><td><%= inspecao.getValvulas().getSegurancaPressaoAberturaTotal2() %></td></tr>
						<tr><th>V�lvula de Seguran�a - Press�o Fechamento 2</th><td><%= inspecao.getValvulas().getSegurancaPressaoFechamento2() %></td></tr>
						<tr><th>V�lvula de Seguran�a - Nome do Laborat�rio</th><td><%= inspecao.getValvulas().getSegurancaNomeLaboratorio() %></td></tr>
						<tr><th>V�lvula de Seguran�a - N� do Certificado 1</th><td><%= inspecao.getValvulas().getSegurancaCertificado1() %></td></tr>
						<tr><th>V�lvula de Seguran�a - N� do Certificado 2</th><td><%= inspecao.getValvulas().getSegurancaCertificado2() %></td></tr>
						<tr><th>V�lvula Rodovi�ria - Press�o Abertura 1</th><td><%= inspecao.getValvulas().getRodoviariaPressaoAbertura1() %></td></tr>
						<tr><th>V�lvula Rodovi�ria - Press�o Abertura Total 1</th><td><%= inspecao.getValvulas().getRodoviariaPressaoAberturaTotal1() %></td></tr>
						<tr><th>V�lvula Rodovi�ria - Press�o Fechamento 1</th><td><%= inspecao.getValvulas().getRodoviariaPressaoFechamento1() %></td></tr>
						<tr><th>V�lvula Rodovi�ria - Press�o Abertura 2</th><td><%= inspecao.getValvulas().getRodoviariaPressaoAbertura2() %></td></tr>
						<tr><th>V�lvula Rodovi�ria - Press�o Abertura Total 2</th><td><%= inspecao.getValvulas().getRodoviariaPressaoAberturaTotal2() %></td></tr>
						<tr><th>V�lvula Rodovi�ria - Press�o Fechamento 2</th><td><%= inspecao.getValvulas().getRodoviariaPressaoFechamento2() %></td></tr>
						<tr><th>V�lvula Rodovi�ria - Nome do Laborat�rio</th><td><%= inspecao.getValvulas().getRodoviariaNomeLaboratorio() %></td></tr>
						<tr><th>V�lvula Rodovi�ria - Press�o N� do Certificado 1</th><td><%= inspecao.getValvulas().getRodoviariaCertificado1() %></td></tr>
						<tr><th>V�lvula Rodovi�ria - Press�o N� do Certificado 2</th><td><%= inspecao.getValvulas().getRodoviariaCertificado2() %></td></tr>
					</table></td></tr>
				</table>
			</td></tr>
			</table>
		</td></tr></table>
</table>

<table class="marco">
	<tr><td align="center" width="800">Imagens da Inspe��o</td></tr>
	<tr><td align="center"><table border="1">
		<tr>
			<th width="150">Imagem Panor�mica do Equipamento</th>
			<th width="150">Imagem da Placa do Equipamento</th>
			<th width="150">Imagem Externa da Inspe��o</th>
			<th width="150">Imagem Interna Inicial da Inspe��o</th>
			<th width="150">Imagem Interna Final da Inspe��o</th>
		</tr>
		<tr>
			<%
				String repositoryPath = "C:\\CargasPerigosasWeb\\InspecoesValidadas\\";
		        String errorPath = "C:\\CargasPerigosasWeb\\InspecoesNaoValidadas\\";
				for(String s : inspecao.getTodasEvidencias()){
					s = repositoryPath + inspecao.getHead().getCipp() + "\\" + s;
			%>
					<td align="center"><a href="#"><img alt="" src="<%= s %>" width="150" /></a></td>
			<%
				}
			%>
		</tr>
	</table></td></tr>
</table>

</div>

<%
	}else{
%>
	<p>Inspe��o inv�lida ou ocorreu um erro.</p>
<%
	}
%>
</div>

</td></tr>

<tr><td align="center">
	<p><input type="button" value="Voltar" onclick="history.go(-1)"/></p>
</td></tr>


<c:import url="/include/inmetro/right.jsp" />
<c:import url="/include/footer.jsp" />