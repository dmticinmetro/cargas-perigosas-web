<%@ page isErrorPage="true" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

<c:import url="/include/header.jsp" />
<c:import url="/include/left.jsp" />

<div class="main">

<h1>Ocorreu um erro</h1>
<p>Entre em contato com a administração do site.</p>

<h4>Mensagem de erro:</h4>
<p><%= exception.getMessage() %></p>
<p><%= exception.getCause() %></p>
<p><%= exception.getStackTrace() %></p>

<p><input type="button" value="Voltar" onclick="history.go(-1)" /></p>

</div>

<c:import url="/include/right.jsp" />
<c:import url="/include/footer.jsp" />