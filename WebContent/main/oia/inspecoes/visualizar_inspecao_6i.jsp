<%@ page errorPage="/main/error.jsp" %>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Inspecao6i"%>
<%@page import="model.Head"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

<!-- <c:import url="/include/check_login.jsp" /> -->
<c:import url="/include/header.jsp" />
<c:import url="/include/oia/left.jsp" />

<div class="main">

<%
	int larg = 160;
	int largFig = 200;
	Inspecao6i inspecao = new Inspecao6i();	
	boolean flag = false;
	if(request.getAttribute("inspecao") != null){
		inspecao = (Inspecao6i) request.getAttribute("inspecao");
		flag = true;
	}
	if(flag){
		if(request.getAttribute("msg") != null)	out.print("<p class='msg'>" + request.getAttribute("msg") + "</p>");
%>

<h1>Inspe��o 6i</h1>

<div class="relatorio">

<p>Identifica��o da Inspe��o</p>
<table border="1">
	<tr>
		<th width="<%= larg %>">OIA</th>
		<th width="<%= larg %>">CIPP</th>
		<th width="<%= larg %>">Tipo de Ensaio</th>
		<th width="<%= larg %>">Relat�rio/RNC</th>
		<th width="<%= larg %>">Data da Inspe��o</th>
	</tr>
	<tr>
		<td><%= inspecao.getHead().getOia() %></td>
		<td><%= inspecao.getHead().getCipp() %></td>
		<td><%= inspecao.getHead().getTipoEnsaio() %></td>
		<td><%= inspecao.getHead().getRelatorio() %></td>
		<td><%= inspecao.getHead().getDataInspecao() %></td>
	</tr>
	<tr>
		<th>Data da Pr�xima Inspe��o</th>
		<th>Lacre</th>
		<th>Supervisor</th>
		<th>Inspetor</th>
		<th>Cliente</th>
	</tr>
	<tr>
		<td><%= inspecao.getHead().getDataProximaInspecao() %></td>
		<td><%= inspecao.getHead().getLacre() %></td>
		<td><%= inspecao.getHead().getSupervisor() %></td>
		<td><%= inspecao.getHead().getInspetor() %></td>
		<td><%= inspecao.getHead().getCliente() %></td>
	</tr>
	<tr>
		<th>Local da Inspe��o</th>
		<th>N�mero do Equipamento</th>
		<th>N�mero do Renavam</th>
		<th>N�mero do Chassi</th>
		<th>Placa do Ve�culo</th>
	</tr>
	<tr>
		<td><%= inspecao.getHead().getLocalInspecao() %></td>
		<td><%= inspecao.getHead().getEquipamento() %></td>
		<td><%=inspecao.getHead().getRenavam()%></td>
		<td><%= inspecao.getHead().getChassi() %></td>
		<td><%= inspecao.getHead().getPlaca() %></td>
	</tr>
</table>

<p>Identifica��o do Equipamento</p>
<table border="1">
	<tr>
		<th width="<%= larg %>">Press�o do Projeto</th>
		<th width="<%= larg %>">Press�o do Ensaio Hidrost�tico</th>
		<th width="<%= larg %>">Material do Costado</th>
		<th width="<%= larg %>">Material da Calotas</th>
		<th width="<%= larg %>">Norma de Fabrica��o</th>
	</tr>
	<tr>
		<td><%= inspecao.getIdentificacaoEquipamento().getPressaoProjeto() %></td>
		<td><%= inspecao.getIdentificacaoEquipamento().getPressaoEnsaioHidrostatico() %></td>
		<td><%= inspecao.getIdentificacaoEquipamento().getMaterialCostado() %></td>
		<td><%= inspecao.getIdentificacaoEquipamento().getMaterialCalotas() %></td>
		<td><%= inspecao.getIdentificacaoEquipamento().getNormaFabricacao() %></td>
	</tr>
	<tr>
		<th>Di�metro do Tanque</th>
		<th>Comprimento do Tanque</th>
		<th>Espessura da Costado</th>
		<th>Espessura da Calotas</th>
		<th>Sobreespessura de Corros�o</th>
	</tr>
	<tr>
		<td><%= inspecao.getIdentificacaoEquipamento().getDiametroTanque() %></td>
		<td><%= inspecao.getIdentificacaoEquipamento().getComprimentoTanque() %></td>
		<td><%= inspecao.getIdentificacaoEquipamento().getEspessuraCostado() %></td>
		<td><%= inspecao.getIdentificacaoEquipamento().getEspessuraCalotas() %></td>
		<td><%= inspecao.getIdentificacaoEquipamento().getSobreespessuraCorrosao() %></td>
	</tr>
	<tr>
		<th>Volume do Tanque</th>
		<th>Radiografia</th>
		<th>Ensaio N�o-Destrutivo</th>
		<th>Al�vio de Tens�es</th>
	</tr>
	<tr>
		<td><%= inspecao.getIdentificacaoEquipamento().getVolumeTanque() %></td>
		<td><%= inspecao.getIdentificacaoEquipamento().getRadiografia() %></td>
		<td><%= inspecao.getIdentificacaoEquipamento().getEnsaioNaoDestrutivo() %></td>
		<td><%= inspecao.getIdentificacaoEquipamento().getAlivioTensoes() %></td>
	</tr>
</table>

<p>Ensaio Hidrost�tico</p>
<table border="1">
	<tr>
		<th width="<%= larg %>">Press�o de Ensaio</th>
		<th width="<%= larg %>">Tempo de Dura��o</th>
		<th width="<%= larg %>">N� Man�metro 1</th>
		<th width="<%= larg %>">Validade do Man�metro 1</th>
	</tr>
	<tr>
		<td><%= inspecao.getEnsaioHidrostatico().getPressaoEnsaio() %></td>
		<td><%= inspecao.getEnsaioHidrostatico().getDuracao() %></td>
		<td><%= inspecao.getEnsaioHidrostatico().getManometro1() %></td>
		<td><%= inspecao.getEnsaioHidrostatico().getValidadeManometro1() %></td>
	</tr>
	<tr>
		<th width="<%= larg %>">N� Man�metro 2</th>
		<th width="<%= larg %>">Validade do Man�metro 2</th>
	</tr>
	<tr>
		<td><%= inspecao.getEnsaioHidrostatico().getManometro2() %></td>
		<td><%= inspecao.getEnsaioHidrostatico().getValidadeManometro2() %></td>
	</tr>
</table>

<p>Ensaio V�lvulas</p>
<table border="1">
	<tr>
		<th width="<%= larg %>">V�lvula de Al�vio - Press�o Abertura 1</th>
		<th width="<%= larg %>">V�lvula de Al�vio - Press�o Abertura Total 1</th>
		<th width="<%= larg %>">V�lvula de Al�vio - Press�o Fechamento 1</th>
		<th width="<%= larg %>">V�lvula de Al�vio - Press�o Abertura 1</th>
		<th width="<%= larg %>">V�lvula de Al�vio - Press�o Abertura Total 1</th>
	</tr>
	<tr>
		<td><%= inspecao.getValvulas().getAlivioPressaoAbertura1() %></td>
		<td><%= inspecao.getValvulas().getAlivioPressaoAberturaTotal1() %></td>
		<td><%= inspecao.getValvulas().getAlivioPressaoFechamento1() %></td>
		<td><%= inspecao.getValvulas().getAlivioPressaoAbertura2() %></td>
		<td><%= inspecao.getValvulas().getAlivioPressaoAberturaTotal2() %></td>
	</tr>
	<tr>
		<th>V�lvula de Al�vio - Press�o Fechamento 1</th>
		<th>V�lvula de Al�vio - Nome do Laborat�rio 1</th>
		<th>V�lvula de Al�vio - N� do Certificado 1</th>
		<th>V�lvula de Al�vio - N� do Certificado 2</th>
		<th>V�lvula de Seguran�a - Press�o Abertura 1</th>
	</tr>
	<tr>
		<td><%= inspecao.getValvulas().getAlivioPressaoFechamento1() %></td>
		<td><%= inspecao.getValvulas().getAlivioNomeLaboratorio() %></td>
		<td><%= inspecao.getValvulas().getAlivioCertificado1() %></td>
		<td><%= inspecao.getValvulas().getAlivioCertificado2() %></td>
		<td><%= inspecao.getValvulas().getSegurancaPressaoAbertura1() %></td>
	</tr>
	<tr>
		<th>V�lvula de Seguran�a - Press�o Abertura Total 1</th>
		<th>V�lvula de Seguran�a - Press�o Fechamento 1</th>
		<th>V�lvula de Seguran�a - Press�o Abertura 2</th>
		<th>V�lvula de Seguran�a - Press�o Abertura Total 2</th>
		<th>V�lvula de Seguran�a - Press�o Fechamento 2</th>
	</tr>
	<tr>
		<td><%= inspecao.getValvulas().getSegurancaPressaoAberturaTotal1() %></td>
		<td><%= inspecao.getValvulas().getSegurancaPressaoFechamento1() %></td>
		<td><%= inspecao.getValvulas().getSegurancaPressaoAbertura2() %></td>
		<td><%= inspecao.getValvulas().getSegurancaPressaoAberturaTotal2() %></td>
		<td><%= inspecao.getValvulas().getSegurancaPressaoFechamento2() %></td>
	</tr>
	<tr>
		<th>V�lvula de Seguran�a - Nome do Laborat�rio</th>
		<th>V�lvula de Seguran�a - N� do Certificado 1</th>
		<th>V�lvula de Seguran�a - N� do Certificado 2</th>
		<th>V�lvula Rodovi�ria - Press�o Abertura 1</th>
		<th>V�lvula Rodovi�ria - Press�o Abertura Total 1</th>
	</tr>
	<tr>
		<td><%= inspecao.getValvulas().getSegurancaNomeLaboratorio() %></td>
		<td><%= inspecao.getValvulas().getSegurancaCertificado1() %></td>
		<td><%= inspecao.getValvulas().getSegurancaCertificado2() %></td>
		<td><%= inspecao.getValvulas().getRodoviariaPressaoAbertura1() %></td>
		<td><%= inspecao.getValvulas().getRodoviariaPressaoAberturaTotal1() %></td>
	</tr>
	<tr>
		<th>V�lvula Rodovi�ria - Press�o Fechamento 1</th>
		<th>V�lvula Rodovi�ria - Press�o Abertura 2</th>
		<th>V�lvula Rodovi�ria - Press�o Abertura Total 2</th>
		<th>V�lvula Rodovi�ria - Press�o Fechamento 2</th>
		<th>V�lvula Rodovi�ria - Nome do Laborat�rio</th>
	</tr>
	<tr>
		<td><%= inspecao.getValvulas().getRodoviariaPressaoFechamento1() %></td>
		<td><%= inspecao.getValvulas().getRodoviariaPressaoAbertura2() %></td>
		<td><%= inspecao.getValvulas().getRodoviariaPressaoAberturaTotal2() %></td>
		<td><%= inspecao.getValvulas().getRodoviariaPressaoFechamento2() %></td>
		<td><%= inspecao.getValvulas().getRodoviariaNomeLaboratorio() %></td>
	</tr>
	<tr>
		<th>V�lvula Rodovi�ria - Press�o N� do Certificado 1</th>
		<th>V�lvula Rodovi�ria - Press�o N� do Certificado 2</th>
	</tr>
	<tr>
		<td><%= inspecao.getValvulas().getRodoviariaCertificado1() %></td>
		<td><%= inspecao.getValvulas().getRodoviariaCertificado2() %></td>
	</tr>

</table>
		
<p>Ensaio N�o-Destrutivo</p>
<table border="1">
	<tr>
		<th width="<%= larg %>">LP</th>
		<th width="<%= larg %>">% Soldas LP</th>
		<th width="<%= larg %>">PM</th>
		<th width="<%= larg %>">% Soldas PM</th>
	</tr>
	<tr>
		<td><%= inspecao.getEnsaioNaoDestrutivo().getLp() %></td>
		<td><%= inspecao.getEnsaioNaoDestrutivo().getSoldasLP() %></td>
		<td><%= inspecao.getEnsaioNaoDestrutivo().getPm() %></td>
		<td><%= inspecao.getEnsaioNaoDestrutivo().getSoldasPm() %></td>
	</tr>
	<tr>
		<th width="<%= larg %>">US</th>
		<th width="<%= larg %>">% Soldas US</th>
		<th width="<%= larg %>">RD</th>
		<th width="<%= larg %>">% Soldas RD</th>
	</tr>
	<tr>
		<td><%= inspecao.getEnsaioNaoDestrutivo().getUs() %></td>
		<td><%= inspecao.getEnsaioNaoDestrutivo().getSoldasUs() %></td>
		<td><%= inspecao.getEnsaioNaoDestrutivo().getRd() %></td>
		<td><%= inspecao.getEnsaioNaoDestrutivo().getSoldasRd() %></td>
	</tr>
</table>		
		
<p>Apto a Transportar Grupos</p>
<table border="1">
	<tr>
		<th>Grupo(s)</th>
	</tr>
	<tr>
	<td>
	<table border="1">
	<tr>
	<%
		if(inspecao.getAptoTransportarGrupos() != null){
			for(String g : inspecao.getAptoTransportarGrupos().getGrupo()){
	%>
				<td><%= g %></td>
	<%
			}
		}else{
	%>
				<td>Nenhum grupo cadastrado</td>
	<%
		}
	%>
	</tr>
	</table>
	</td>
	</tr>	
</table>
<!-- 
<p>Imagens da Inspe��o</p>
<table border="1">
	<tr>
		<th width="<%= larg %>">Imagem Panor�mica do Equipamento</th>
		<th width="<%= larg %>">Imagem da Placa do Equipamento</th>
		<th width="<%= larg %>">Imagem Externa da Inspe��o</th>
		<th width="<%= larg %>">Imagem Interna Inicial da Inspe��o</th>
		<th width="<%= larg %>">Imagem Interna Final da Inspe��o</th>
	</tr>
	<tr>
		<td><img src="<%= inspecao.getIdentificacaoEquipamento().getFotoPanoramica() %>" width="<%= largFig %>" /></td>
		<td><img src="<%= inspecao.getIdentificacaoEquipamento().getFotoPlaca() %>" width="<%= largFig %>" /></td>
		<td><img src="<%= inspecao.getInspecaoVisualExterna().getFoto() %>" width="<%= largFig %>" /></td>
		<td><img src="<%= inspecao.getInspecaoVisualInterna().getFotoPanoramica() %>" width="<%= largFig %>" /></td>
		<td><img src="<%= inspecao.getInspecaoVisualInterna().getFotoPlaca() %>" width="<%= largFig %>" /></td>
	</tr>
</table>
 -->
</div>
<%
	}else{
%>
	<p>Inspe��o inv�lida ou ocorreu um erro.</p>
<%
	}
%>
</div>

<p><input type="button" value="Voltar" onclick="history.go(-1)" /></p>

<c:import url="/include/oia/right.jsp" />
<c:import url="/include/footer.jsp" />