<%@ page errorPage="/main/error.jsp" %>
<%@page import="java.util.ArrayList"%>
<%@page import="model.EnsaioValvulas7i"%>
<%@page import="model.Inspecao7i"%>
<%@page import="model.Head"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

<!-- <c:import url="/include/check_login.jsp" /> -->
<c:import url="/include/header.jsp" />
<c:import url="/include/oia/left.jsp" />

<div class="main">

<%
	int larg = 160;
	int largFig = 200;
	Inspecao7i inspecao = new Inspecao7i();
	boolean flag = false;
	if(request.getAttribute("inspecao") != null){
		inspecao = (Inspecao7i) request.getAttribute("inspecao");
		flag = true;
	}
	if(flag){
		if(request.getAttribute("msg") != null)	out.print("<p class='msg'>" + request.getAttribute("msg") + "</p>");
%>

<h1>Inspe��o 7i</h1>

<div class="relatorio">

<p>Identifica��o da Inspe��o</p>
<table border="1">
	<tr>
		<th width="<%= larg %>">OIA</th>
		<th width="<%= larg %>">CIPP</th>
		<th width="<%= larg %>">Tipo de Ensaio</th>
		<th width="<%= larg %>">Relat�rio/RNC</th>
		<th width="<%= larg %>">Data da Inspe��o</th>
	</tr>
	<tr>
		<td><%= inspecao.getHead().getOia() %></td>
		<td><%= inspecao.getHead().getCipp() %></td>
		<td><%= inspecao.getHead().getTipoEnsaio() %></td>
		<td><%= inspecao.getHead().getRelatorio() %></td>
		<td><%= inspecao.getHead().getDataInspecao() %></td>
	</tr>
	<tr>
		<th>Data da Pr�xima Inspe��o</th>
		<th>Lacre</th>
		<th>Supervisor</th>
		<th>Inspetor</th>
		<th>Cliente</th>
	</tr>
	<tr>
		<td><%= inspecao.getHead().getDataProximaInspecao() %></td>
		<td><%= inspecao.getHead().getLacre() %></td>
		<td><%= inspecao.getHead().getSupervisor() %></td>
		<td><%= inspecao.getHead().getInspetor() %></td>
		<td><%= inspecao.getHead().getCliente() %></td>
	</tr>
	<tr>
		<th>Local da Inspe��o</th>
		<th>N�mero do Equipamento</th>
		<th>N�mero do Renavam</th>
		<th>N�mero do Chassi</th>
		<th>Placa do Ve�culo</th>
	</tr>
	<tr>
		<td><%= inspecao.getHead().getLocalInspecao() %></td>
		<td><%= inspecao.getHead().getEquipamento() %></td>
		<td><%=inspecao.getHead().getRenavam()%></td>
		<td><%= inspecao.getHead().getChassi() %></td>
		<td><%= inspecao.getHead().getPlaca() %></td>
	</tr>
</table>

<p>Identifica��o do Equipamento</p>
<table border="1">
	<tr>
		<th width="<%= larg %>">Fabricante</th>
		<th width="<%= larg %>">Data da Fabrica��o</th>
		<th width="<%= larg %>">Tipo do Equipamento</th>
		<th width="<%= larg %>">Press�o do Projeto</th>
		<th width="<%= larg %>">Press�o do Ensaio de Press�o</th>
	</tr>
	<tr>
		<td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getFabricante() %></td>
		<td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getDataFabricacao() %></td>
		<td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getTipo() %></td>
		<td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getPressaoProjeto() %></td>
		<td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getPressaoEnsaio() %></td>
	</tr>
	<tr>
		<th>Material do Costado</th>
		<th>Material da Calotas</th>
		<th>N�mero de Compartimentos</th>
		<th>Tanque Cil�ndrico</th>
		<th>Diametro Interno</th>
	</tr>
	<tr>
		<td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getMaterialCostado() %></td>
		<td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getMaterialCalotas() %></td>
		<td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getNumCompartimentos() %></td>
		<td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getTanqueCilindrico() %></td>
		<td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getDiametroInterno() %></td>
	</tr>
	<tr>
		<th>Tanque Polic�ntrico</th>
		<th>Raio de Curvatura</th>
		<th>Tanque Revestido</th>
		<th>Espessura do Costado</th>
		<th>Espessura da Calotas</th>
	</tr>
	<tr>
		<td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getTanquePolicentrico() %></td>
		<td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getRaioCurvatura() %></td>
		<td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getTanqueRevestido() %></td>
		<td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getEspessuraCostado() %></td>
		<td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getEspessuraCalotas() %></td>
	</tr>
	<tr>
		<th>Sobreespessura de Corrosao</th>
		<th>Volume do Tanque</th>
	</tr>
	<tr>
		<td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getSobreespessuraCorrosao() %></td>
		<td><%= inspecao.getMarcoIdentificacaoEquipamento7i().getVolumeTanque() %></td>
	</tr>
</table>

<p>Ensaio Hidrost�tico / Pneum�tico</p>
<table border="1">
	<tr>
		<th width="<%= larg %>">Press�o de Ensaio</th>
		<th width="<%= larg %>">Ensaio Executado</th>
		<th width="<%= larg %>">Resultado</th>
		<th width="<%= larg %>">Tempo de Dura��o</th>
	</tr>
	<tr>
		<td><%= inspecao.getMarcoEnsaioHidrostaticoPneumatico7i().getPressaoEnsaio() %></td>
		<td><%= inspecao.getMarcoEnsaioHidrostaticoPneumatico7i().getEnsaioExecutado() %></td>
		<td><%= inspecao.getMarcoEnsaioHidrostaticoPneumatico7i().getResultadoEnsaio() %></td>
		<td><%= inspecao.getMarcoEnsaioHidrostaticoPneumatico7i().getTempoDuracao() %></td>
	</tr>
</table>

<p>Ensaio V�lvulas</p>
<table border="1">
	<tr>
		<th width="<%= larg %>">V�lvula de Al�vio - Press�o Abertura</th>
		<th width="<%= larg %>">V�lvula de Al�vio - Press�o Fechamento</th>
		<th width="<%= larg %>">V�lvula de V�cuo - Press�o Abertura</th>
		<th width="<%= larg %>">V�lvula de V�cuo - Press�o M�xima</th>
	</tr>
<%
	for(EnsaioValvulas7i e : inspecao.getEnsaioValvulas7i()){
%>
	<tr>
		<td><%= e.getValvulaAlivio().getPressaoAbertura() %></td>
		<td><%= e.getValvulaAlivio().getPressaoFechamento() %></td>
		<td><%= e.getValvulaVacuo().getPressaoAbertura() %></td>
		<td><%= e.getValvulaVacuo().getPressaoMaxima() %></td>
	</tr>
<%
	}
%>
</table>

<p>Apto a Transportar Grupos</p>
<table border="1">
	<tr>
		<th>Grupo(s)</th>
	</tr>
	<tr>
	<td>
	<table border="1">
	<tr>
	<%
		if(inspecao.getAptoTransportarGrupos() != null){
			for(String g : inspecao.getAptoTransportarGrupos().getGrupo()){
	%>
				<td><%= g %></td>
	<%
			}
		}else{
	%>
				<td>Nenhum grupo cadastrado</td>
	<%
		}
	%>
	</tr>
	</table>
	</td>
	</tr>	
</table>
<!-- 
<p>Imagens da Inspe��o</p>
<table border="1">
	<tr>
		<th width="<%= larg %>">Imagem Panor�mica do Equipamento</th>
		<th width="<%= larg %>">Imagem da Placa do Equipamento</th>
		<th width="<%= larg %>">Imagem do In�cio do Ensaio</th>
		<th width="<%= larg %>">Imagem do Fim do Ensaio</th>
	</tr>
	<tr>
		<td><img src="<%= inspecao.getMarcoIdentificacaoEquipamento7i().getRef_imagem_evidencia_panoramica() %>" width="<%= largFig %>" /></td>
		<td><img src="<%= inspecao.getMarcoIdentificacaoEquipamento7i().getRef_imagem_evidencia_placa() %>" width="<%= largFig %>" /></td>
		<td><img src="<%= inspecao.getMarcoEnsaioHidrostaticoPneumatico7i().getEvidencia1() %>" width="<%= largFig %>" /></td>
		<td><img src="<%= inspecao.getMarcoEnsaioHidrostaticoPneumatico7i().getEvidencia2() %>" width="<%= largFig %>" /></td>
	</tr>
	<tr>
		<th width="<%= larg %>">Imagem Externa da Inspe��o</th>
		<th width="<%= larg %>">Imagem Interna Inicial da Inspe��o</th>
		<th width="<%= larg %>">Imagem Interna Final da Inspe��o</th>
	</tr>
	<tr>
		<td><img src="<%= inspecao.getInspecaoVisualExterna().getFoto() %>" width="<%= largFig %>" /></td>
		<td><img src="<%= inspecao.getInspecaoVisualInterna().getFotoPanoramica() %>" width="<%= largFig %>" /></td>
		<td><img src="<%= inspecao.getInspecaoVisualInterna().getFotoPlaca() %>" width="<%= largFig %>" /></td>
	</tr>
</table>
 -->
</div>
<%
	}else{
%>
	<p>Inspe��o inv�lida ou ocorreu um erro.</p>
<%
	}
%>
</div>

<p><input type="button" value="Voltar" onclick="history.go(-1)" /></p>

<c:import url="/include/oia/right.jsp" />
<c:import url="/include/footer.jsp" />