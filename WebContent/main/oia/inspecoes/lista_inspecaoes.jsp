<%@page import="model.Inspecao3i"%>
<%@page import="java.util.ArrayList"%>
<%@ page errorPage="/main/error.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- <c:import url="/include/check_login.jsp" /> -->
<c:import url="/include/header.jsp" />
<c:import url="/include/oia/left.jsp" />

<tr><td class="tituloNoticiaHP" width="100%" height="17" align="left">

<div class="main">
	<c:forEach items="${requestScope.lista1i}" var="insp">
		<hr/>
		<a href="LinkControl?action=visualizar_inspecao_1i&inspecao=<c:out value="${insp.id}"/>&cipp=<c:out value="${insp.head.cipp}"/>">
			<table>
				<tr>
					<th class="listaInspecao">CIPP: </th>
					<td class="listaInspecao"><c:out value="${insp.head.cipp}"/></td>
				</tr>
				<tr>
					<th class="listaInspecao">OIA: </th>
					<td class="listaInspecao"><c:out value="${insp.head.oia}"/></td>
				</tr>
				<tr>
					<th class="listaInspecao">Tipo de Ensaio: </th>
					<td class="listaInspecao">1i</td>
				</tr>
				<tr>
					<th class="listaInspecao">Data da Inspe��o: </th>
					<td class="listaInspecao"><c:out value="${insp.head.dataInspecao}"/></td>
				</tr>
			</table>
		</a>
	</c:forEach>
	<c:forEach items="${requestScope.lista3i}" var="insp">
		<hr/>
		<a href="LinkControl?action=visualizar_inspecao_3i&inspecao=<c:out value="${insp.id}"/>&cipp=<c:out value="${insp.head.cipp}"/>">
			<table>
				<tr>
					<th class="listaInspecao">CIPP: </th>
					<td class="listaInspecao"><c:out value="${insp.head.cipp}"/></td>
				</tr>
				<tr>
					<th class="listaInspecao">OIA: </th>
					<td class="listaInspecao"><c:out value="${insp.head.oia}"/></td>
				</tr>
				<tr>
					<th class="listaInspecao">Tipo de Ensaio: </th>
					<td class="listaInspecao">3i</td>
				</tr>
				<tr>
					<th class="listaInspecao">Data da Inspe��o: </th>
					<td class="listaInspecao"><c:out value="${insp.head.dataInspecao}"/></td>
				</tr>
			</table>
		</a>
	</c:forEach>
	<c:forEach items="${requestScope.lista6i}" var="insp">
		<hr/>
		<a href="LinkControl?action=visualizar_inspecao_6i&inspecao=<c:out value="${insp.id}"/>&cipp=<c:out value="${insp.head.cipp}"/>">	
			<table>
				<tr>
					<th class="listaInspecao">CIPP: </th>
					<td class="listaInspecao"><c:out value="${insp.head.cipp}"/></td>
				</tr>
				<tr>
					<th class="listaInspecao">OIA: </th>
					<td class="listaInspecao"><c:out value="${insp.head.oia}"/></td>
				</tr>
				<tr>
					<th class="listaInspecao">Tipo de Ensaio: </th>
					<td class="listaInspecao">6i</td>
				</tr>
				<tr>
					<th class="listaInspecao">Data da Inspe��o: </th>
					<td class="listaInspecao"><c:out value="${insp.head.dataInspecao}"/></td>
				</tr>
			</table>
		</a>
	</c:forEach>
	<c:forEach items="${requestScope.lista7i}" var="insp">
		<hr/>
		<a href="LinkControl?action=visualizar_inspecao_7i&inspecao=<c:out value="${insp.id}"/>&cipp=<c:out value="${insp.head.cipp}"/>">
			<table>
				<tr>
					<th class="listaInspecao">CIPP: </th>
					<td class="listaInspecao"><c:out value="${insp.head.cipp}"/></td>
				</tr>
				<tr>
					<th class="listaInspecao">OIA: </th>
					<td class="listaInspecao"><c:out value="${insp.head.oia}"/></td>
				</tr>
				<tr>
					<th class="listaInspecao">Tipo de Ensaio: </th>
					<td class="listaInspecao">7i</td>
				</tr>
				<tr>
					<th class="listaInspecao">Data da Inspe��o: </th>
					<td class="listaInspecao"><c:out value="${insp.head.dataInspecao}"/></td>
				</tr>
			</table>
		</a>
	</c:forEach>
</div>

</td></tr>

<tr><td align="center">
	<c:if test="${requestScope.msg != null}">
		<h3><c:out value="${requestScope.msg}" /></h3>
	</c:if>
</td></tr>

<tr><td align="center">
	<p><input type="button" value="Voltar" onclick="history.go(-1)" /></p>
</td></tr>

<c:import url="/include/oia/right.jsp" />
<c:import url="/include/footer.jsp" />