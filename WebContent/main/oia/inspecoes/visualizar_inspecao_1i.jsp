<%@ page errorPage="/main/error.jsp" %>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.File"%>
<%@ page errorPage="/main/error.jsp" %>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Inspecao1i"%>
<%@page import="model.Head"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

<!-- <c:import url="/include/check_login.jsp" /> -->
<c:import url="/include/header.jsp" />
<c:import url="/include/oia/left.jsp" />

<div class="main">

<%
	int larg = 160;
	int largFig = 200;
	Inspecao1i inspecao = new Inspecao1i();	
	boolean flag = false;
	if(request.getAttribute("inspecao") != null){
		inspecao = (Inspecao1i) request.getAttribute("inspecao");
		flag = true;
	}
	if(flag){
		if(request.getAttribute("msg") != null)	out.print("<p class='msg'>" + request.getAttribute("msg") + "</p>");
%>
<h1>Inspe��o 1i</h1>

<div class="relatorio">

<p>Identifica��o da Inspe��o</p>
<table border="1">
	<tr>
		<th width="<%= larg %>">OIA</th>
		<th width="<%= larg %>">CIPP</th>
		<th width="<%= larg %>">Tipo de Ensaio</th>
		<th width="<%= larg %>">Relat�rio/RNC</th>
		<th width="<%= larg %>">Data da Inspe��o</th>
	</tr>
	<tr>
		<td><%= inspecao.getHead().getOia() %></td>
		<td><%= inspecao.getHead().getCipp() %></td>
		<td><%= inspecao.getHead().getTipoEnsaio() %></td>
		<td><%= inspecao.getHead().getRelatorio() %></td>
		<td><%= inspecao.getHead().getDataInspecao() %></td>
	</tr>
	<tr>
		<th>Data da Pr�xima Inspe��o</th>
		<th>Lacre</th>
		<th>Supervisor</th>
		<th>Inspetor</th>
		<th>Cliente</th>
	</tr>
	<tr>
		<td><%= inspecao.getHead().getDataProximaInspecao() %></td>
		<td><%= inspecao.getHead().getLacre() %></td>
		<td><%= inspecao.getHead().getSupervisor() %></td>
		<td><%= inspecao.getHead().getInspetor() %></td>
		<td><%= inspecao.getHead().getCliente() %></td>
	</tr>
	<tr>
		<th>Local da Inspe��o</th>
		<th>N�mero do Equipamento</th>
		<th>N�mero do Renavam</th>
		<th>N�mero do Chassi</th>
		<th>Placa do Ve�culo</th>
	</tr>
	<tr>
		<td><%= inspecao.getHead().getLocalInspecao() %></td>
		<td><%= inspecao.getHead().getEquipamento() %></td>
		<td><%=inspecao.getHead().getRenavam()%></td>
		<td><%= inspecao.getHead().getChassi() %></td>
		<td><%= inspecao.getHead().getPlaca() %></td>
	</tr>
</table>

<p>Identifica��o do Equipamento</p>
<table border="1">
	<tr>
		<th width="<%= larg %>">Press�o do Projeto</th>
		<th width="<%= larg %>">Press�o do Ensaio Hidrost�tico</th>
		<th width="<%= larg %>">Material do Costado</th>
		<th width="<%= larg %>">Material da Calotas</th>
		<th width="<%= larg %>">Norma de Fabrica��o</th>
	</tr>
	<tr>
		<td><%= inspecao.getIdentificacaoEquipamento().getPressaoProjeto() %></td>
		<td><%= inspecao.getIdentificacaoEquipamento().getPressaoEnsaioHidrostatico() %></td>
		<td><%= inspecao.getIdentificacaoEquipamento().getMaterialCostado() %></td>
		<td><%= inspecao.getIdentificacaoEquipamento().getMaterialCalotas() %></td>
		<td><%= inspecao.getIdentificacaoEquipamento().getNormaFabricacao() %></td>
	</tr>
	<tr>
		<th>Temperatura de Projeto</th>
		<th>Press�o de Opera��o</th>
		<th>Espessura da Costado</th>
		<th>Espessura da Calotas</th>
		<th>Di�metro Interno</th>
	</tr>
	<tr>
		<td><%= inspecao.getIdentificacaoEquipamento().getTemperaturaProjeto() %></td>
		<td><%= inspecao.getIdentificacaoEquipamento().getPressaoOperacao() %></td>
		<td><%= inspecao.getIdentificacaoEquipamento().getEspessuraCostado() %></td>
		<td><%= inspecao.getIdentificacaoEquipamento().getEspessuraCalotas() %></td>
		<td><%= inspecao.getIdentificacaoEquipamento().getDiametroInterno() %></td>
	</tr>
	<tr>
		<th>Press�o de Abertura da V�lvula de Seguran�a</th>
		<th>Comprimento do Tanque</th>
		<th>Capacidade Geom�trica</th>
	</tr>
	<tr>
		<td><%= inspecao.getIdentificacaoEquipamento().getPressaoAberturaValvulaSeg() %></td>
		<td><%= inspecao.getIdentificacaoEquipamento().getComprimentoTanques() %></td>
		<td><%= inspecao.getIdentificacaoEquipamento().getCapacidadeGeometrica() %></td>
	</tr>
</table>

<p>Enpessuaras M�nimas</p>
<table border="1">
	<tr>
		<th width="<%= larg %>">Espessura M�nima do Costado</th>
		<th width="<%= larg %>">Espessura M�nima da Calotas Dianteira</th>
		<th width="<%= larg %>">Espessura M�nima da Calotas Traseira</th>
		<th width="<%= larg %>">Espessura M�nima da Tampa de Boca de Vista</th>
	</tr>
	<tr>
		<td><%= inspecao.getEspessurasMinimas().getEspMinCostado() %></td>
		<td><%= inspecao.getEspessurasMinimas().getEspMinCalotasDianteira() %></td>
		<td><%= inspecao.getEspessurasMinimas().getEspMinCalotasTraseira() %></td>
		<td><%= inspecao.getEspessurasMinimas().getEspMinTampaBoca() %></td>
	</tr>
</table>
		
<p>V�lvulas de Al�vio</p>
<table border="1">
	<tr>
		<th width="<%= larg %>">Al�vio de Fechamento</th>
		<th width="<%= larg %>">Al�vio de Abertura</th>
		<th width="<%= larg %>">Nome do Laborat�rio</th>
	</tr>
	<tr>
		<td><%= inspecao.getValvulaAlivio().getAlivioFechamento() %></td>
		<td><%= inspecao.getValvulaAlivio().getAlivioAbertura() %></td>
		<td><%= inspecao.getValvulaAlivio().getNomeLaboratorio() %></td>
	</tr>
</table>		
		
<p>Apto a Transportar Grupos</p>
<table border="1">
	<tr>
		<th>Grupo(s)</th>
	</tr>
	<tr>
	<td>
	<table border="1">
	<tr>
	<%
		if(inspecao.getAptoTransportarGrupos() != null){
			for(String g : inspecao.getAptoTransportarGrupos().getGrupo()){
	%>
				<td><%= g %></td>
	<%
			}
		}else{
	%>
				<td>Nenhum grupo cadastrado</td>
	<%
		}
	%>
	</tr>
	</table>
	</td>
	</tr>	
</table>
<!--
<p>Imagens da Inspe��o</p>
<table border="1">
	<tr>
		<th width="<%= larg %>">Imagem Panor�mica do Equipamento</th>
		<th width="<%= larg %>">Imagem da Placa do Equipamento</th>
		<th width="<%= larg %>">Imagem Externa da Inspe��o</th>
		<th width="<%= larg %>">Imagem Interna Inicial da Inspe��o</th>
		<th width="<%= larg %>">Imagem Interna Final da Inspe��o</th>
	</tr>
	<tr>
		<td><img src="<%= inspecao.getIdentificacaoEquipamento().getFotoPanoramica() %>" width="<%= largFig %>" /></td>
		<td><img src="<%= inspecao.getIdentificacaoEquipamento().getFotoPlaca() %>" width="<%= largFig %>" /></td>
		<td><img src="<%= inspecao.getInspecaoVisualExterna().getFoto() %>" width="<%= largFig %>" /></td>
		<td><img src="<%= inspecao.getInspecaoVisualInterna().getFotoPanoramica() %>" width="<%= largFig %>" /></td>
		<td><img src="<%= inspecao.getInspecaoVisualInterna().getFotoPlaca() %>" width="<%= largFig %>" /></td>
	</tr>
</table>
-->
</div>
<%
	}else{
%>
		<p>Inspe��o inv�lida ou ocorreu um erro.</p>
<%
	}
%>
</div>

<p><input type="button" value="Voltar" onclick="history.go(-1)" /></p>

<c:import url="/include/oia/right.jsp" />
<c:import url="/include/footer.jsp" />