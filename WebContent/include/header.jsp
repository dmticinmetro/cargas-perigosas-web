<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br">

<head>
	<link rel="shortcut icon" href="/cargasperigosas/favicon.ico" />
	<title>Cargas Perigosas | Inmetro - Instituto Nacional de Metrologia, Qualidade e Tecnologia</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<script language="JavaScript" src="/cargasperigosas/include/scripts.js" type="text/javascript"></script>
	<script language="JavaScript" src="/cargasperigosas/include/swfobject.js" type="text/javascript"></script>
	<script language="JavaScript" src="/cargasperigosas/include/jquery-1.4.2.min.js" type="text/javascript"></script>
	<meta name="keywords" content="acessibilidade, acredita��o, alimentos, an�lise, arquea��o, arranjos, artigos, barreiras, biblioteca, brasileiro, calibra��o, canal, certificadas, cient�fica, comit�, conformidade, conmetro, consultas, consumidor, consumo, credenciamento, desburocratiza��o, documentos, efici�ncia, empresas, energ�tica, energia, ensaios, eventos, exporta��o, exportador, ficacaliza��o, f�rum, g�s, gest��o, GNV, importa��o, imprensa, incubadora, indique, industrial, informa��o, instituto, instrumentos, ISO, laborat�rios, legal, legisla��o, licita��o, marcas, medi��o, medidas, metrologia, normaliza��o, normas, not�cias, oficinas, organismos, ouvidoria, painel setorial, planejamento, ponto focal, portarias, pr�-medidos, produtos, profici�ncia, programas, publica��es, qualidade, rede, regulamenta��o, regulamentos, resolu��es, selos, s�mbolos, t�cnicas, tecnol�gica, unidade" />
	<meta name="description" content="O site do Inmetro - Instituto Nacional de Metrologia, Qualidade e Tecnologia - tem por objetivo disseminar as informa��es sobre o perfil da institui��oo, as a��es desenvolvidas e os programas de trabalho produzidos por suas diversas �reas.  � um ve�culo direcionador de informa��o transparente, que incentiva a interatividade e fortalece a credibilidade promovendo a qualidade de vida do cidad�o e a competitividade da economia atrav�s da metrologia e da qualidade." />
	<link rel="stylesheet" href="/cargasperigosas/include/estilos.css" type="text/css" />
</head>

<body bgcolor="#ffffff" onLoad="MM_preloadImages();abrir()" >

<table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr valign="middle"> 
    <td align="center" valign="middle">
    	<div>
			<link type="text/css" rel="stylesheet" media="all" href="/cargasperigosas/include/barra/estilo_barragov_2011.css"  />
			<div id="barra-governo-wrap" class="first-child">
	    		<div class="wrap first-child last-child"></div>
			</div>
			<div id="barra-governo">
	    		<div class="centro first-child last-child">
	    			<a href="http://www.brasil.gov.br/"  class="logoBrasil first-child last-child"></a>
	    		</div>
			</div>
		</div>
	</td>
  	</tr>
</table>

<table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr valign="middle"> 
	<td align="center" valign="middle"><link rel="stylesheet" type="text/css" href="/cargasperigosas/include/inmetro-header/estilo.css"/>
		<div id="inmetro-logo">
			<img src="/cargasperigosas/include/inmetro-header/images/logo-inmetro.png" alt="Inmetro" onClick="location.href='/'" height="67" />
		</div>
		<div id="whitebg">
		</div>
		<div id="header">
			<div id="header-conteudo">
		        <table width="100%" cellpadding="0" cellspacing="0">
			        <tr><td id="titulo-pagina"></td>
			        <td id="header-col-right">
				        <div id="inmetro-menu">
				            <ul>
				                <li><a class="" href="#">Mapa do Site</a></li>
				                <li><a class="" href="#">Fale Conosco</a></li>
				            </ul>                
				        </div>
						<script type="text/javascript">
							// Faz a marca��o das abas do menu do header de acordo com a �rea
							alvoP = document.getElementById('inmetro-menu');
							alvoS = alvoP.getElementsByTagName('a');
						</script>
						<div id="titulo">
							<div id="texto" class="left">Cargas Perigosas</div>
							<div id="subtexto" class="right">Cgcre / Ditel</div>
						</div>
			        </td></tr>
		        </table>
		    </div>
		</div>
	</td>
	</tr>
</table>