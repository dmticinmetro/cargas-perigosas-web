package enumeration;

import javax.persistence.Entity;
import javax.persistence.Table;

@Table
@Entity
public enum Tipo {
	OIA, INMETRO;
}
