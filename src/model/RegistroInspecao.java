package model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class RegistroInspecao {
	@Id
	@GeneratedValue
	private int id;
	private Integer cipp;
	private String oia;
	private String tipoEnsaio;
	private Date dataEnvio;
	
	public RegistroInspecao(Integer cipp, String oia, String tipoEnsaio) {
		this.cipp = cipp;
		this.oia = oia;
		this.tipoEnsaio = tipoEnsaio;
		this.dataEnvio = new Date();
	}
	public RegistroInspecao() {
		this.dataEnvio = new Date();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int idRegistroInspecao) {
		this.id = idRegistroInspecao;
	}
	public Integer getCipp() {
		return cipp;
	}
	public void setCipp(Integer cipp) {
		this.cipp = cipp;
	}
	public String getOia() {
		return oia;
	}
	public void setOia(String oia) {
		this.oia = oia;
	}
	public String getTipoEnsaio() {
		return tipoEnsaio;
	}
	public void setTipoEnsaio(String tipoEnsaio) {
		this.tipoEnsaio = tipoEnsaio;
	}
	public Date getDataEnvio() {
		return dataEnvio;
	}
	public void setDataEnvio(Date dataEnvio) {
		this.dataEnvio = dataEnvio;
	}	
}
