package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table
@XmlType(name = "MARCO_VALVULAS")
@XmlAccessorType(XmlAccessType.FIELD)
public class Valvulas6i {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
    private int id;
    
	@Column
    @XmlElement(name = "ALIVIO_PRESSAO_ABERTURA1")
    private double alivioPressaoAbertura1;
    
	@Column
    @XmlElement(name = "ALIVIO_PRESSAO_ABERTURA_TOTAL1")
    private double alivioPressaoAberturaTotal1;
    
	@Column
    @XmlElement(name = "ALIVIO_PRESSAO_FECHAMENTO1")
    private double alivioPressaoFechamento1;
    
	@Column
    @XmlElement(name = "ALIVIO_PRESSAO_ABERTURA2")
    private double alivioPressaoAbertura2;
    
	@Column
    @XmlElement(name = "ALIVIO_PRESSAO_ABERTURA_TOTAL2")
    private double alivioPressaoAberturaTotal2;
    
	@Column
    @XmlElement(name = "ALIVIO_PRESSAO_FECHAMENTO2")
    private double alivioPressaoFechamento2;

	@Column
    @XmlElement(name = "ALIVIO_NOME_LABORATORIO")
    private String alivioNomeLaboratorio;
    
	@Column
    @XmlElement(name = "ALIVIO_NUMERO_CERTIFICADO1")
    private String alivioCertificado1;
    
	@Column
    @XmlElement(name = "ALIVIO_NUMERO_CERTIFICADO2")
    private String alivioCertificado2;
    
	@Column
    @XmlElement(name = "SEGURANCA_PRESSAO_ABERTURA1")
    private double segurancaPressaoAbertura1;
    
	@Column
    @XmlElement(name = "SEGURANCA_PRESSAO_ABERTURA_TOTAL1")
    private double segurancaPressaoAberturaTotal1;
    
	@Column
    @XmlElement(name = "SEGURANCA_PRESSAO_FECHAMENTO1")
    private double segurancaPressaoFechamento1;
    
	@Column
    @XmlElement(name = "SEGURANCA_PRESSAO_ABERTURA2")
    private double segurancaPressaoAbertura2;
    
	@Column
    @XmlElement(name = "SEGURANCA_PRESSAO_ABERTURA_TOTAL2")
    private double segurancaPressaoAberturaTotal2;
    
	@Column
    @XmlElement(name = "SEGURANCA_PRESSAO_FECHAMENTO2")
    private double segurancaPressaoFechamento2;

	@Column
    @XmlElement(name = "SEGURANCA_NOME_LABORATORIO")
    private String segurancaNomeLaboratorio;
    
	@Column
    @XmlElement(name = "SEGURANCA_NUMERO_CERTIFICADO1")
    private String segurancaCertificado1;
    
	@Column
    @XmlElement(name = "SEGURANCA_NUMERO_CERTIFICADO2")
    private String segurancaCertificado2;
    
	@Column
    @XmlElement(name = "RODOVIARIA_PRESSAO_ABERTURA1")
    private double rodoviariaPressaoAbertura1;
    
	@Column
    @XmlElement(name = "RODOVIARIA_PRESSAO_ABERTURA_TOTAL1")
    private double rodoviariaPressaoAberturaTotal1;
    
	@Column
    @XmlElement(name = "RODOVIARIA_PRESSAO_FECHAMENTO1")
    private double rodoviariaPressaoFechamento1;
    
	@Column
    @XmlElement(name = "RODOVIARIA_PRESSAO_ABERTURA2")
    private double rodoviariaPressaoAbertura2;
    
	@Column
    @XmlElement(name = "RODOVIARIA_PRESSAO_ABERTURA_TOTAL2")
    private double rodoviariaPressaoAberturaTotal2;
    
	@Column
    @XmlElement(name = "RODOVIARIA_PRESSAO_FECHAMENTO2")
    private double rodoviariaPressaoFechamento2;

	@Column
    @XmlElement(name = "RODOVIARIA_NOME_LABORATORIO")
    private String rodoviariaNomeLaboratorio;
    
	@Column
    @XmlElement(name = "RODOVIARIA_NUMERO_CERTIFICADO1")
    private String rodoviariaCertificado1;
    
	@Column
    @XmlElement(name = "RODOVIARIA_NUMERO_CERTIFICADO2")
    private String rodoviariaCertificado2;

	public Valvulas6i(int idValvulas6i, double alivioPressaoAbertura1,
			double alivioPressaoAberturaTotal1,
			double alivioPressaoFechamento1, double alivioPressaoAbertura2,
			double alivioPressaoAberturaTotal2,
			double alivioPressaoFechamento2, String alivioNomeLaboratorio,
			String alivioCertificado1, String alivioCertificado2,
			double segurancaPressaoAbertura1,
			double segurancaPressaoAberturaTotal1,
			double segurancaPressaoFechamento1,
			double segurancaPressaoAbertura2,
			double segurancaPressaoAberturaTotal2,
			double segurancaPressaoFechamento2,
			String segurancaNomeLaboratorio, String segurancaCertificado1,
			String segurancaCertificado2, double rodoviariaPressaoAbertura1,
			double rodoviariaPressaoAberturaTotal1,
			double rodoviariaPressaoFechamento1,
			double rodoviariaPressaoAbertura2,
			double rodoviariaPressaoAberturaTotal2,
			double rodoviariaPressaoFechamento2,
			String rodoviariaNomeLaboratorio, String rodoviariaCertificado1,
			String rodoviariaCertificado2) {
		this.id = idValvulas6i;
		this.alivioPressaoAbertura1 = alivioPressaoAbertura1;
		this.alivioPressaoAberturaTotal1 = alivioPressaoAberturaTotal1;
		this.alivioPressaoFechamento1 = alivioPressaoFechamento1;
		this.alivioPressaoAbertura2 = alivioPressaoAbertura2;
		this.alivioPressaoAberturaTotal2 = alivioPressaoAberturaTotal2;
		this.alivioPressaoFechamento2 = alivioPressaoFechamento2;
		this.alivioNomeLaboratorio = alivioNomeLaboratorio;
		this.alivioCertificado1 = alivioCertificado1;
		this.alivioCertificado2 = alivioCertificado2;
		this.segurancaPressaoAbertura1 = segurancaPressaoAbertura1;
		this.segurancaPressaoAberturaTotal1 = segurancaPressaoAberturaTotal1;
		this.segurancaPressaoFechamento1 = segurancaPressaoFechamento1;
		this.segurancaPressaoAbertura2 = segurancaPressaoAbertura2;
		this.segurancaPressaoAberturaTotal2 = segurancaPressaoAberturaTotal2;
		this.segurancaPressaoFechamento2 = segurancaPressaoFechamento2;
		this.segurancaNomeLaboratorio = segurancaNomeLaboratorio;
		this.segurancaCertificado1 = segurancaCertificado1;
		this.segurancaCertificado2 = segurancaCertificado2;
		this.rodoviariaPressaoAbertura1 = rodoviariaPressaoAbertura1;
		this.rodoviariaPressaoAberturaTotal1 = rodoviariaPressaoAberturaTotal1;
		this.rodoviariaPressaoFechamento1 = rodoviariaPressaoFechamento1;
		this.rodoviariaPressaoAbertura2 = rodoviariaPressaoAbertura2;
		this.rodoviariaPressaoAberturaTotal2 = rodoviariaPressaoAberturaTotal2;
		this.rodoviariaPressaoFechamento2 = rodoviariaPressaoFechamento2;
		this.rodoviariaNomeLaboratorio = rodoviariaNomeLaboratorio;
		this.rodoviariaCertificado1 = rodoviariaCertificado1;
		this.rodoviariaCertificado2 = rodoviariaCertificado2;
	}
	public Valvulas6i(double alivioPressaoAbertura1,
			double alivioPressaoAberturaTotal1,
			double alivioPressaoFechamento1, double alivioPressaoAbertura2,
			double alivioPressaoAberturaTotal2,
			double alivioPressaoFechamento2, String alivioNomeLaboratorio,
			String alivioCertificado1, String alivioCertificado2,
			double segurancaPressaoAbertura1,
			double segurancaPressaoAberturaTotal1,
			double segurancaPressaoFechamento1,
			double segurancaPressaoAbertura2,
			double segurancaPressaoAberturaTotal2,
			double segurancaPressaoFechamento2,
			String segurancaNomeLaboratorio, String segurancaCertificado1,
			String segurancaCertificado2, double rodoviariaPressaoAbertura1,
			double rodoviariaPressaoAberturaTotal1,
			double rodoviariaPressaoFechamento1,
			double rodoviariaPressaoAbertura2,
			double rodoviariaPressaoAberturaTotal2,
			double rodoviariaPressaoFechamento2,
			String rodoviariaNomeLaboratorio, String rodoviariaCertificado1,
			String rodoviariaCertificado2) {
		this.alivioPressaoAbertura1 = alivioPressaoAbertura1;
		this.alivioPressaoAberturaTotal1 = alivioPressaoAberturaTotal1;
		this.alivioPressaoFechamento1 = alivioPressaoFechamento1;
		this.alivioPressaoAbertura2 = alivioPressaoAbertura2;
		this.alivioPressaoAberturaTotal2 = alivioPressaoAberturaTotal2;
		this.alivioPressaoFechamento2 = alivioPressaoFechamento2;
		this.alivioNomeLaboratorio = alivioNomeLaboratorio;
		this.alivioCertificado1 = alivioCertificado1;
		this.alivioCertificado2 = alivioCertificado2;
		this.segurancaPressaoAbertura1 = segurancaPressaoAbertura1;
		this.segurancaPressaoAberturaTotal1 = segurancaPressaoAberturaTotal1;
		this.segurancaPressaoFechamento1 = segurancaPressaoFechamento1;
		this.segurancaPressaoAbertura2 = segurancaPressaoAbertura2;
		this.segurancaPressaoAberturaTotal2 = segurancaPressaoAberturaTotal2;
		this.segurancaPressaoFechamento2 = segurancaPressaoFechamento2;
		this.segurancaNomeLaboratorio = segurancaNomeLaboratorio;
		this.segurancaCertificado1 = segurancaCertificado1;
		this.segurancaCertificado2 = segurancaCertificado2;
		this.rodoviariaPressaoAbertura1 = rodoviariaPressaoAbertura1;
		this.rodoviariaPressaoAberturaTotal1 = rodoviariaPressaoAberturaTotal1;
		this.rodoviariaPressaoFechamento1 = rodoviariaPressaoFechamento1;
		this.rodoviariaPressaoAbertura2 = rodoviariaPressaoAbertura2;
		this.rodoviariaPressaoAberturaTotal2 = rodoviariaPressaoAberturaTotal2;
		this.rodoviariaPressaoFechamento2 = rodoviariaPressaoFechamento2;
		this.rodoviariaNomeLaboratorio = rodoviariaNomeLaboratorio;
		this.rodoviariaCertificado1 = rodoviariaCertificado1;
		this.rodoviariaCertificado2 = rodoviariaCertificado2;
	}
	public Valvulas6i() {
	}
	public int getId() {
		return id;
	}
	public void setId(int idValvulas6i) {
		this.id = idValvulas6i;
	}
	public double getAlivioPressaoAbertura1() {
		return alivioPressaoAbertura1;
	}
	public void setAlivioPressaoAbertura1(double alivioPressaoAbertura1) {
		this.alivioPressaoAbertura1 = alivioPressaoAbertura1;
	}
	public double getAlivioPressaoAberturaTotal1() {
		return alivioPressaoAberturaTotal1;
	}
	public void setAlivioPressaoAberturaTotal1(double alivioPressaoAberturaTotal1) {
		this.alivioPressaoAberturaTotal1 = alivioPressaoAberturaTotal1;
	}
	public double getAlivioPressaoFechamento1() {
		return alivioPressaoFechamento1;
	}
	public void setAlivioPressaoFechamento1(double alivioPressaoFechamento1) {
		this.alivioPressaoFechamento1 = alivioPressaoFechamento1;
	}
	public double getAlivioPressaoAbertura2() {
		return alivioPressaoAbertura2;
	}
	public void setAlivioPressaoAbertura2(double alivioPressaoAbertura2) {
		this.alivioPressaoAbertura2 = alivioPressaoAbertura2;
	}
	public double getAlivioPressaoAberturaTotal2() {
		return alivioPressaoAberturaTotal2;
	}
	public void setAlivioPressaoAberturaTotal2(double alivioPressaoAberturaTotal2) {
		this.alivioPressaoAberturaTotal2 = alivioPressaoAberturaTotal2;
	}
	public double getAlivioPressaoFechamento2() {
		return alivioPressaoFechamento2;
	}
	public void setAlivioPressaoFechamento2(double alivioPressaoFechamento2) {
		this.alivioPressaoFechamento2 = alivioPressaoFechamento2;
	}
	public String getAlivioNomeLaboratorio() {
		return alivioNomeLaboratorio;
	}
	public void setAlivioNomeLaboratorio(String alivioNomeLaboratorio) {
		this.alivioNomeLaboratorio = alivioNomeLaboratorio;
	}
	public String getAlivioCertificado1() {
		return alivioCertificado1;
	}
	public void setAlivioCertificado1(String alivioCertificado1) {
		this.alivioCertificado1 = alivioCertificado1;
	}
	public String getAlivioCertificado2() {
		return alivioCertificado2;
	}
	public void setAlivioCertificado2(String alivioCertificado2) {
		this.alivioCertificado2 = alivioCertificado2;
	}
	public double getSegurancaPressaoAbertura1() {
		return segurancaPressaoAbertura1;
	}
	public void setSegurancaPressaoAbertura1(double segurancaPressaoAbertura1) {
		this.segurancaPressaoAbertura1 = segurancaPressaoAbertura1;
	}
	public double getSegurancaPressaoAberturaTotal1() {
		return segurancaPressaoAberturaTotal1;
	}
	public void setSegurancaPressaoAberturaTotal1(
			double segurancaPressaoAberturaTotal1) {
		this.segurancaPressaoAberturaTotal1 = segurancaPressaoAberturaTotal1;
	}
	public double getSegurancaPressaoFechamento1() {
		return segurancaPressaoFechamento1;
	}
	public void setSegurancaPressaoFechamento1(double segurancaPressaoFechamento1) {
		this.segurancaPressaoFechamento1 = segurancaPressaoFechamento1;
	}
	public double getSegurancaPressaoAbertura2() {
		return segurancaPressaoAbertura2;
	}
	public void setSegurancaPressaoAbertura2(double segurancaPressaoAbertura2) {
		this.segurancaPressaoAbertura2 = segurancaPressaoAbertura2;
	}
	public double getSegurancaPressaoAberturaTotal2() {
		return segurancaPressaoAberturaTotal2;
	}
	public void setSegurancaPressaoAberturaTotal2(
			double segurancaPressaoAberturaTotal2) {
		this.segurancaPressaoAberturaTotal2 = segurancaPressaoAberturaTotal2;
	}
	public double getSegurancaPressaoFechamento2() {
		return segurancaPressaoFechamento2;
	}
	public void setSegurancaPressaoFechamento2(double segurancaPressaoFechamento2) {
		this.segurancaPressaoFechamento2 = segurancaPressaoFechamento2;
	}
	public String getSegurancaNomeLaboratorio() {
		return segurancaNomeLaboratorio;
	}
	public void setSegurancaNomeLaboratorio(String segurancaNomeLaboratorio) {
		this.segurancaNomeLaboratorio = segurancaNomeLaboratorio;
	}
	public String getSegurancaCertificado1() {
		return segurancaCertificado1;
	}
	public void setSegurancaCertificado1(String segurancaCertificado1) {
		this.segurancaCertificado1 = segurancaCertificado1;
	}
	public String getSegurancaCertificado2() {
		return segurancaCertificado2;
	}
	public void setSegurancaCertificado2(String segurancaCertificado2) {
		this.segurancaCertificado2 = segurancaCertificado2;
	}
	public double getRodoviariaPressaoAbertura1() {
		return rodoviariaPressaoAbertura1;
	}
	public void setRodoviariaPressaoAbertura1(double rodoviariaPressaoAbertura1) {
		this.rodoviariaPressaoAbertura1 = rodoviariaPressaoAbertura1;
	}
	public double getRodoviariaPressaoAberturaTotal1() {
		return rodoviariaPressaoAberturaTotal1;
	}
	public void setRodoviariaPressaoAberturaTotal1(
			double rodoviariaPressaoAberturaTotal1) {
		this.rodoviariaPressaoAberturaTotal1 = rodoviariaPressaoAberturaTotal1;
	}
	public double getRodoviariaPressaoFechamento1() {
		return rodoviariaPressaoFechamento1;
	}
	public void setRodoviariaPressaoFechamento1(double rodoviariaPressaoFechamento1) {
		this.rodoviariaPressaoFechamento1 = rodoviariaPressaoFechamento1;
	}
	public double getRodoviariaPressaoAbertura2() {
		return rodoviariaPressaoAbertura2;
	}
	public void setRodoviariaPressaoAbertura2(double rodoviariaPressaoAbertura2) {
		this.rodoviariaPressaoAbertura2 = rodoviariaPressaoAbertura2;
	}
	public double getRodoviariaPressaoAberturaTotal2() {
		return rodoviariaPressaoAberturaTotal2;
	}
	public void setRodoviariaPressaoAberturaTotal2(
			double rodoviariaPressaoAberturaTotal2) {
		this.rodoviariaPressaoAberturaTotal2 = rodoviariaPressaoAberturaTotal2;
	}
	public double getRodoviariaPressaoFechamento2() {
		return rodoviariaPressaoFechamento2;
	}
	public void setRodoviariaPressaoFechamento2(double rodoviariaPressaoFechamento2) {
		this.rodoviariaPressaoFechamento2 = rodoviariaPressaoFechamento2;
	}
	public String getRodoviariaNomeLaboratorio() {
		return rodoviariaNomeLaboratorio;
	}
	public void setRodoviariaNomeLaboratorio(String rodoviariaNomeLaboratorio) {
		this.rodoviariaNomeLaboratorio = rodoviariaNomeLaboratorio;
	}
	public String getRodoviariaCertificado1() {
		return rodoviariaCertificado1;
	}
	public void setRodoviariaCertificado1(String rodoviariaCertificado1) {
		this.rodoviariaCertificado1 = rodoviariaCertificado1;
	}
	public String getRodoviariaCertificado2() {
		return rodoviariaCertificado2;
	}
	public void setRodoviariaCertificado2(String rodoviariaCertificado2) {
		this.rodoviariaCertificado2 = rodoviariaCertificado2;
	}
}
