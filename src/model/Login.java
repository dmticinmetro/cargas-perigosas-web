package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table
public class Login {
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
	
	@Column(unique = true)
	private String login;
	
	@Column
	private String senha;
	
	@JoinColumn
	@OneToOne(fetch = FetchType.EAGER)
	private Entidade entidade;

	public Login(int id, String login, String senha, Entidade entidade) {
		this.id = id;
		this.login = login;
		this.senha = senha;
		this.entidade = entidade;
	}
	public Login(String login, String senha, Entidade entidade) {
		super();
		this.login = login;
		this.senha = senha;
		this.entidade = entidade;
	}

	public Login(String login, String senha) {
		this.login = login;
		this.senha = senha;
	}
	public Login() {
	}
	
	public int getId() {
		return id;
	}
	public void setId(int idLogin) {
		this.id = idLogin;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public Entidade getEntidade() {
		return entidade;
	}
	public void setEntidade(Entidade entidade) {
		this.entidade = entidade;
	}
}
