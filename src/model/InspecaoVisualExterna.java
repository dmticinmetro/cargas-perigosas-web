package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Entity
@Table
@XmlAccessorType(XmlAccessType.FIELD)
public class InspecaoVisualExterna {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
	
	@Column
	@XmlElement(name = "IMG_INSPECAO_VISUAL_EXTERNA")
	private String foto;

	public InspecaoVisualExterna(int idInspecaoVisualExterna, String foto) {
		this.id = idInspecaoVisualExterna;
		this.foto = foto;
	}
	public InspecaoVisualExterna(String foto) {
		this.foto = foto;
	}
	public InspecaoVisualExterna() {
	}
	
	public int getId() {
		return id;
	}
	public void setId(int idInspecaoVisualExterna) {
		this.id = idInspecaoVisualExterna;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
}
