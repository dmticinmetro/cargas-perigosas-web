package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table
@XmlType(name = "MARCO_VALVULAS_ALIVIO")
@XmlAccessorType(XmlAccessType.FIELD)
public class ValvulaAlivio1i {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
	
	@Column
	@XmlElement(name = "ALIVIO_FECHAMENTO")
    private double alivioFechamento;
	
	@Column
	@XmlElement(name = "ALIVIO_ABERTURA")
    private double alivioAbertura;
	
	@Column
	@XmlElement(name = "ALIVIO_LABORATORIO")
    private String nomeLaboratorio;

	public ValvulaAlivio1i(int idValvulaAlivio1i, double alivioFechamento,
			double alivioAbertura, String nomeLaboratorio) {
		this.id = idValvulaAlivio1i;
		this.alivioFechamento = alivioFechamento;
		this.alivioAbertura = alivioAbertura;
		this.nomeLaboratorio = nomeLaboratorio;
	}
	public ValvulaAlivio1i(double alivioFechamento, double alivioAbertura,
			String nomeLaboratorio) {
		this.alivioFechamento = alivioFechamento;
		this.alivioAbertura = alivioAbertura;
		this.nomeLaboratorio = nomeLaboratorio;
	}
	public ValvulaAlivio1i() {
	}
	
	public int getId() {
		return id;
	}
	public void setId(int idValvulaAlivio1i) {
		this.id = idValvulaAlivio1i;
	}
	public double getAlivioFechamento() {
		return alivioFechamento;
	}
	public void setAlivioFechamento(double alivioFechamento) {
		this.alivioFechamento = alivioFechamento;
	}
	public double getAlivioAbertura() {
		return alivioAbertura;
	}
	public void setAlivioAbertura(double alivioAbertura) {
		this.alivioAbertura = alivioAbertura;
	}
	public String getNomeLaboratorio() {
		return nomeLaboratorio;
	}
	public void setNomeLaboratorio(String nomeLaboratorio) {
		this.nomeLaboratorio = nomeLaboratorio;
	}
}
