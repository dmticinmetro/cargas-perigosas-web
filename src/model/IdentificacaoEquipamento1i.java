package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table
@XmlType(name = "MARCO_IDENTIFICACAO_EQUIPAMENTO")
@XmlAccessorType(XmlAccessType.FIELD)
public class IdentificacaoEquipamento1i {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
	
	@Column
	@XmlElement(name = "PRESSAO_PROJETO")
	private double pressaoProjeto;
	
	@Column
    @XmlElement(name = "PRESSAO_ENSAIO_HIDROSTATICO")
    private double pressaoEnsaioHidrostatico;
        
	@Column
    @XmlElement(name = "MATERIAL_CALOTAS")
    private String materialCalotas;
    
	@Column
    @XmlElement(name = "MATERIAL_COSTADO")
    private String materialCostado;
    
	@Column
    @XmlElement(name = "TEMPERATURA_PROJETO")
    private double temperaturaProjeto;
    
	@Column
    @XmlElement(name = "NORMA_FABRICACAO")
    private String normaFabricacao;
    
	@Column
    @XmlElement(name = "PRESSAO_OPERACAO")
    private double pressaoOperacao;
    
	@Column
    @XmlElement(name = "DIAMETRO_INTERNO_TANQUE")
    private double diametroInterno;
    
	@Column
    @XmlElement(name = "PRESSAO_ABERTURA_VALVULA_SEGURANCA")
    private double pressaoAberturaValvulaSeg;
    
	@Column
    @XmlElement(name = "COMPRIMENTO_TANQUE")
    private double comprimentoTanques;
    
	@Column
    @XmlElement(name = "ESPESSURA_CALOTAS")
    private double espessuraCalotas;
    
	@Column
    @XmlElement(name = "ESPESSURA_COSTADO")
    private double espessuraCostado;
    
	@Column
    @XmlElement(name = "CAPACIDADE_GEOMETRICA")
    private double capacidadeGeometrica;
    
	@Column
    @XmlElement(name = "IMG_IDENTIFICACAO_EQUIPAMENTO_PANORAMICA")
    private String fotoPanoramica;
    
	@Column
    @XmlElement(name = "IMG_IDENTIFICACAO_EQUIPAMENTO_PLACA")
    private String fotoPlaca;

	public IdentificacaoEquipamento1i(int idIdentificacao1i,
			double pressaoProjeto, double pressaoEnsaioHidrostatico,
			String materialCalotas, String materialCostado,
			double temperaturaProjeto, String normaFabricacao,
			double pressaoOperacao, double diametroInterno,
			double pressaoAberturaValvulaSeg, double comprimentoTanques,
			double espessuraCalotas, double espessuraCostado,
			double capacidadeGeometrica, String fotoPanoramica, String fotoPlaca) {
		this.id = idIdentificacao1i;
		this.pressaoProjeto = pressaoProjeto;
		this.pressaoEnsaioHidrostatico = pressaoEnsaioHidrostatico;
		this.materialCalotas = materialCalotas;
		this.materialCostado = materialCostado;
		this.temperaturaProjeto = temperaturaProjeto;
		this.normaFabricacao = normaFabricacao;
		this.pressaoOperacao = pressaoOperacao;
		this.diametroInterno = diametroInterno;
		this.pressaoAberturaValvulaSeg = pressaoAberturaValvulaSeg;
		this.comprimentoTanques = comprimentoTanques;
		this.espessuraCalotas = espessuraCalotas;
		this.espessuraCostado = espessuraCostado;
		this.capacidadeGeometrica = capacidadeGeometrica;
		this.fotoPanoramica = fotoPanoramica;
		this.fotoPlaca = fotoPlaca;
	}

	public IdentificacaoEquipamento1i(double pressaoProjeto,
			double pressaoEnsaioHidrostatico, String materialCalotas,
			String materialCostado, double temperaturaProjeto,
			String normaFabricacao, double pressaoOperacao,
			double diametroInterno, double pressaoAberturaValvulaSeg,
			double comprimentoTanques, double espessuraCalotas,
			double espessuraCostado, double capacidadeGeometrica,
			String fotoPanoramica, String fotoPlaca) {
		this.pressaoProjeto = pressaoProjeto;
		this.pressaoEnsaioHidrostatico = pressaoEnsaioHidrostatico;
		this.materialCalotas = materialCalotas;
		this.materialCostado = materialCostado;
		this.temperaturaProjeto = temperaturaProjeto;
		this.normaFabricacao = normaFabricacao;
		this.pressaoOperacao = pressaoOperacao;
		this.diametroInterno = diametroInterno;
		this.pressaoAberturaValvulaSeg = pressaoAberturaValvulaSeg;
		this.comprimentoTanques = comprimentoTanques;
		this.espessuraCalotas = espessuraCalotas;
		this.espessuraCostado = espessuraCostado;
		this.capacidadeGeometrica = capacidadeGeometrica;
		this.fotoPanoramica = fotoPanoramica;
		this.fotoPlaca = fotoPlaca;
	}

	public IdentificacaoEquipamento1i() {
	}

	public int getId() {
		return id;
	}

	public void setId(int idIdentificacao1i) {
		this.id = idIdentificacao1i;
	}

	public double getPressaoProjeto() {
		return pressaoProjeto;
	}

	public void setPressaoProjeto(double pressaoProjeto) {
		this.pressaoProjeto = pressaoProjeto;
	}

	public double getPressaoEnsaioHidrostatico() {
		return pressaoEnsaioHidrostatico;
	}

	public void setPressaoEnsaioHidrostatico(double pressaoEnsaioHidrostatico) {
		this.pressaoEnsaioHidrostatico = pressaoEnsaioHidrostatico;
	}

	public String getMaterialCalotas() {
		return materialCalotas;
	}

	public void setMaterialCalotas(String materialCalotas) {
		this.materialCalotas = materialCalotas;
	}

	public String getMaterialCostado() {
		return materialCostado;
	}

	public void setMaterialCostado(String materialCostado) {
		this.materialCostado = materialCostado;
	}

	public double getTemperaturaProjeto() {
		return temperaturaProjeto;
	}

	public void setTemperaturaProjeto(double temperaturaProjeto) {
		this.temperaturaProjeto = temperaturaProjeto;
	}

	public String getNormaFabricacao() {
		return normaFabricacao;
	}

	public void setNormaFabricacao(String normaFabricacao) {
		this.normaFabricacao = normaFabricacao;
	}

	public double getPressaoOperacao() {
		return pressaoOperacao;
	}

	public void setPressaoOperacao(double pressaoOperacao) {
		this.pressaoOperacao = pressaoOperacao;
	}

	public double getDiametroInterno() {
		return diametroInterno;
	}

	public void setDiametroInterno(double diametroInterno) {
		this.diametroInterno = diametroInterno;
	}

	public double getPressaoAberturaValvulaSeg() {
		return pressaoAberturaValvulaSeg;
	}

	public void setPressaoAberturaValvulaSeg(double pressaoAberturaValvulaSeg) {
		this.pressaoAberturaValvulaSeg = pressaoAberturaValvulaSeg;
	}

	public double getComprimentoTanques() {
		return comprimentoTanques;
	}

	public void setComprimentoTanques(double comprimentoTanques) {
		this.comprimentoTanques = comprimentoTanques;
	}

	public double getEspessuraCalotas() {
		return espessuraCalotas;
	}

	public void setEspessuraCalotas(double espessuraCalotas) {
		this.espessuraCalotas = espessuraCalotas;
	}

	public double getEspessuraCostado() {
		return espessuraCostado;
	}

	public void setEspessuraCostado(double espessuraCostado) {
		this.espessuraCostado = espessuraCostado;
	}

	public double getCapacidadeGeometrica() {
		return capacidadeGeometrica;
	}

	public void setCapacidadeGeometrica(double capacidadeGeometrica) {
		this.capacidadeGeometrica = capacidadeGeometrica;
	}

	public String getFotoPanoramica() {
		return fotoPanoramica;
	}

	public void setFotoPanoramica(String fotoPanoramica) {
		this.fotoPanoramica = fotoPanoramica;
	}

	public String getFotoPlaca() {
		return fotoPlaca;
	}

	public void setFotoPlaca(String fotoPlaca) {
		this.fotoPlaca = fotoPlaca;
	}
}
