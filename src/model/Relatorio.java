package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RELATORIO")
@XmlAccessorType(XmlAccessType.FIELD)
public class Relatorio {
	private int id;
	
	@XmlElement(name = "HASH")
	private String hash;
	
	@XmlElement(name = "SIGN")
	private String sign;
	
	@XmlElement(name = "FOTOS")
	private String fotos;
	
	@XmlElement(name = "RECIBO")
	private String recibo;

	public Relatorio(int idRelatorio, String hash, String sign, String fotos,
			String recibo) {
		this.id = idRelatorio;
		this.hash = hash;
		this.sign = sign;
		this.fotos = fotos;
		this.recibo = recibo;
	}

	public Relatorio(String hash, String sign, String fotos, String recibo) {
		this.hash = hash;
		this.sign = sign;
		this.fotos = fotos;
		this.recibo = recibo;
	}

	public Relatorio() {
	}

	public int getId() {
		return id;
	}

	public void setId(int idRelatorio) {
		this.id = idRelatorio;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getFotos() {
		return fotos;
	}

	public void setFotos(String fotos) {
		this.fotos = fotos;
	}

	public String getRecibo() {
		return recibo;
	}

	public void setRecibo(String recibo) {
		this.recibo = recibo;
	}
}
