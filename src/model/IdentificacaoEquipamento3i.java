package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table
@XmlType(name = "MARCO_IDENTIFICACAO_EQUIPAMENTO")
@XmlAccessorType(XmlAccessType.FIELD)
public class IdentificacaoEquipamento3i {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
	
	@Column
	@XmlElement(name = "PRESSAO_PROJETO")
	private double pressaoProjeto;
	
	@Column
	@XmlElement(name = "PRESSAO_ENSAIO_HIDROSTATICO")
	private double pressaoEnsaioHidrostatico;
	
	@Column
	@XmlElement(name = "MATERIAL_COSTADO")
	private String materialCostado;
	
	@Column
	@XmlElement(name = "MATERIAL_CALOTAS")
	private String materialCalotas;
	
	@Column
	@XmlElement(name = "NORMA_FABRICACAO")
	private String normaFabricacao;
	
	@Column
	@XmlElement(name = "DIAMETRO_INTERNO_TANQUE")
	private double diametroTanque;
	
	@Column
	@XmlElement(name = "COMPRIMENTO_TANQUE")
	private double comprimentoTanque;
	
	@Column
	@XmlElement(name = "ESPESSURA_CALOTAS")
	private double espessuraCalotas;
	
	@Column
	@XmlElement(name = "ESPESSURA_COSTADO")
	private double espessuraCostado;
	
	@Column
	@XmlElement(name = "NIVEL_VACUO")
	private double nivelVacuo;
	
	@Column
	@XmlElement(name = "VOLUME_TANQUE")
	private double volumeTanque;
	
	@Column
	@XmlElement(name = "IMG_IDENTIFICACAO_EQUIPAMENTO_PANORAMICA")
	private String fotoPanoramica;
	
	@Column
	@XmlElement(name = "IMG_IDENTIFICACAO_EQUIPAMENTO_PLACA")
	private String fotoPlaca;

	public IdentificacaoEquipamento3i(int idIdentificacao3i,
			double pressaoProjeto, double pressaoEnsaioHidrostatico,
			String materialCostado, String materialCalotas,
			String normaFabricacao, double diametroTanque,
			double comprimentoTanque, double espessuraCalotas,
			double espessuraCostado, double nivelVacuo, double volumeTanque,
			String fotoPanoramica, String fotoPlaca) {
		this.id = idIdentificacao3i;
		this.pressaoProjeto = pressaoProjeto;
		this.pressaoEnsaioHidrostatico = pressaoEnsaioHidrostatico;
		this.materialCostado = materialCostado;
		this.materialCalotas = materialCalotas;
		this.normaFabricacao = normaFabricacao;
		this.diametroTanque = diametroTanque;
		this.comprimentoTanque = comprimentoTanque;
		this.espessuraCalotas = espessuraCalotas;
		this.espessuraCostado = espessuraCostado;
		this.nivelVacuo = nivelVacuo;
		this.volumeTanque = volumeTanque;
		this.fotoPanoramica = fotoPanoramica;
		this.fotoPlaca = fotoPlaca;
	}

	public IdentificacaoEquipamento3i(double pressaoProjeto,
			double pressaoEnsaioHidrostatico, String materialCostado,
			String materialCalotas, String normaFabricacao,
			double diametroTanque, double comprimentoTanque,
			double espessuraCalotas, double espessuraCostado,
			double nivelVacuo, double volumeTanque, String fotoPanoramica,
			String fotoPlaca) {
		this.pressaoProjeto = pressaoProjeto;
		this.pressaoEnsaioHidrostatico = pressaoEnsaioHidrostatico;
		this.materialCostado = materialCostado;
		this.materialCalotas = materialCalotas;
		this.normaFabricacao = normaFabricacao;
		this.diametroTanque = diametroTanque;
		this.comprimentoTanque = comprimentoTanque;
		this.espessuraCalotas = espessuraCalotas;
		this.espessuraCostado = espessuraCostado;
		this.nivelVacuo = nivelVacuo;
		this.volumeTanque = volumeTanque;
		this.fotoPanoramica = fotoPanoramica;
		this.fotoPlaca = fotoPlaca;
	}

	public IdentificacaoEquipamento3i() {
	}
	public int getId() {
		return id;
	}
	public void setId(int idIdentificacao3i) {
		this.id = idIdentificacao3i;
	}
	public double getPressaoProjeto() {
		return pressaoProjeto;
	}
	public void setPressaoProjeto(double pressaoProjeto) {
		this.pressaoProjeto = pressaoProjeto;
	}
	public double getPressaoEnsaioHidrostatico() {
		return pressaoEnsaioHidrostatico;
	}
	public void setPressaoEnsaioHidrostatico(double pressaoEnsaioHidrostatico) {
		this.pressaoEnsaioHidrostatico = pressaoEnsaioHidrostatico;
	}
	public String getMaterialCostado() {
		return materialCostado;
	}
	public void setMaterialCostado(String materialCostado) {
		this.materialCostado = materialCostado;
	}
	public String getMaterialCalotas() {
		return materialCalotas;
	}
	public void setMaterialCalotas(String materialCalotas) {
		this.materialCalotas = materialCalotas;
	}
	public String getNormaFabricacao() {
		return normaFabricacao;
	}
	public void setNormaFabricacao(String normaFabricacao) {
		this.normaFabricacao = normaFabricacao;
	}
	public double getDiametroTanque() {
		return diametroTanque;
	}
	public void setDiametroTanque(double diametroTanque) {
		this.diametroTanque = diametroTanque;
	}
	public double getComprimentoTanque() {
		return comprimentoTanque;
	}
	public void setComprimentoTanque(double comprimentoTanque) {
		this.comprimentoTanque = comprimentoTanque;
	}
	public double getEspessuraCalotas() {
		return espessuraCalotas;
	}
	public void setEspessuraCalotas(double espessuraCalotas) {
		this.espessuraCalotas = espessuraCalotas;
	}
	public double getEspessuraCostado() {
		return espessuraCostado;
	}
	public void setEspessuraCostado(double espessuraCostado) {
		this.espessuraCostado = espessuraCostado;
	}
	public double getNivelVacuo() {
		return nivelVacuo;
	}
	public void setNivelVacuo(double nivelVacuo) {
		this.nivelVacuo = nivelVacuo;
	}
	public double getVolumeTanque() {
		return volumeTanque;
	}
	public void setVolumeTanque(double volumeTanque) {
		this.volumeTanque = volumeTanque;
	}
	public String getFotoPanoramica() {
		return fotoPanoramica;
	}
	public void setFotoPanoramica(String fotoPanoramica) {
		this.fotoPanoramica = fotoPanoramica;
	}
	public String getFotoPlaca() {
		return fotoPlaca;
	}
	public void setFotoPlaca(String fotoPlaca) {
		this.fotoPlaca = fotoPlaca;
	}
}
