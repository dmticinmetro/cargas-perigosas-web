package model;

import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement(name = "INSPECAO")
@XmlAccessorType(XmlAccessType.FIELD)
public class Inspecao1i {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "head")
	@XmlElement(name = "HEAD")
	private Head head;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "identificacaoEquipamento")
	@XmlElement(name = "MARCO_IDENTIFICACAO_EQUIPAMENTO")
	private IdentificacaoEquipamento1i identificacaoEquipamento;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "valvulaAlivio")
	@XmlElement(name = "MARCO_VALVULAS_ALIVIO")
	private ValvulaAlivio1i valvulaAlivio;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "espessurasMinimas")
	@XmlElement(name = "MARCO_ESPESSURAS_MINIMAS")
	private EspessurasMinimas1i espessurasMinimas;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "aptoTransportarGrupos")
	@XmlElement(name = "MARCO_APTO_TRANSPORTAR_GRUPOS")
	private Grupos aptoTransportarGrupos;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "inspecaoVisualExterna")
	@XmlElement(name = "MARCO_INSPECAO_VISUAL_EXTERNA")
	private InspecaoVisualExterna inspecaoVisualExterna;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "inspecaoVisualInterna")
	@XmlElement(name = "MARCO_INSPECAO_VISUAL_INTERNA")
	private InspecaoVisualInterna inspecaoVisualInterna;

	public Inspecao1i(int idInspecao, Head head,
			IdentificacaoEquipamento1i identificacaoEquipamento,
			ValvulaAlivio1i valvulaAlivio,
			EspessurasMinimas1i espessurasMinimas,
			Grupos aptoTransportarGrupos,
			InspecaoVisualExterna inspecaoVisualExterna,
			InspecaoVisualInterna inspecaoVisualInterna) {
		this.id = idInspecao;
		this.head = head;
		this.identificacaoEquipamento = identificacaoEquipamento;
		this.valvulaAlivio = valvulaAlivio;
		this.espessurasMinimas = espessurasMinimas;
		this.aptoTransportarGrupos = aptoTransportarGrupos;
		this.inspecaoVisualExterna = inspecaoVisualExterna;
		this.inspecaoVisualInterna = inspecaoVisualInterna;
	}
	public Inspecao1i(Head head,
			IdentificacaoEquipamento1i identificacaoEquipamento,
			ValvulaAlivio1i valvulaAlivio,
			EspessurasMinimas1i espessurasMinimas,
			Grupos aptoTransportarGrupos,
			InspecaoVisualExterna inspecaoVisualExterna,
			InspecaoVisualInterna inspecaoVisualInterna) {
		this.head = head;
		this.identificacaoEquipamento = identificacaoEquipamento;
		this.valvulaAlivio = valvulaAlivio;
		this.espessurasMinimas = espessurasMinimas;
		this.aptoTransportarGrupos = aptoTransportarGrupos;
		this.inspecaoVisualExterna = inspecaoVisualExterna;
		this.inspecaoVisualInterna = inspecaoVisualInterna;
	}
	public Inspecao1i() {
	}
	
	public int getId() {
		return id;
	}
	public void setId(int idInspecao) {
		this.id = idInspecao;
	}
	public Head getHead() {
		return head;
	}
	public void setHead(Head head) {
		this.head = head;
	}
	public IdentificacaoEquipamento1i getIdentificacaoEquipamento() {
		return identificacaoEquipamento;
	}
	public void setIdentificacaoEquipamento(
			IdentificacaoEquipamento1i identificacaoEquipamento) {
		this.identificacaoEquipamento = identificacaoEquipamento;
	}
	public ValvulaAlivio1i getValvulaAlivio() {
		return valvulaAlivio;
	}
	public void setValvulaAlivio(ValvulaAlivio1i valvulaAlivio) {
		this.valvulaAlivio = valvulaAlivio;
	}
	public EspessurasMinimas1i getEspessurasMinimas() {
		return espessurasMinimas;
	}
	public void setEspessurasMinimas(EspessurasMinimas1i espessurasMinimas) {
		this.espessurasMinimas = espessurasMinimas;
	}
	public Grupos getAptoTransportarGrupos() {
		return aptoTransportarGrupos;
	}
	public void setAptoTransportarGrupos(Grupos aptoTransportarGrupos) {
		this.aptoTransportarGrupos = aptoTransportarGrupos;
	}
	public InspecaoVisualExterna getInspecaoVisualExterna() {
		return inspecaoVisualExterna;
	}
	public void setInspecaoVisualExterna(InspecaoVisualExterna inspecaoVisualExterna) {
		this.inspecaoVisualExterna = inspecaoVisualExterna;
	}
	public InspecaoVisualInterna getInspecaoVisualInterna() {
		return inspecaoVisualInterna;
	}
	public void setInspecaoVisualInterna(InspecaoVisualInterna inspecaoVisualInterna) {
		this.inspecaoVisualInterna = inspecaoVisualInterna;
	}
	
	public ArrayList<String> getTodasEvidencias(){
		ArrayList<String> evidencias = new ArrayList<String>();
		evidencias.add(this.identificacaoEquipamento.getFotoPanoramica().replace("\\", "/").split("/")[4]);
		evidencias.add(this.identificacaoEquipamento.getFotoPlaca().replace("\\", "/").split("/")[4]);
		evidencias.add(this.inspecaoVisualExterna.getFoto().replace("\\", "/").split("/")[4]);
		evidencias.add(this.inspecaoVisualInterna.getFotoPanoramica().replace("\\", "/").split("/")[4]);
		evidencias.add(this.inspecaoVisualInterna.getFotoPlaca().replace("\\", "/").split("/")[4]);
		for (String s : evidencias) {
			System.out.println(s);
		}
		return evidencias;
	}
	
	public void setTodasEvidencias(ArrayList<String> evidencias){
		this.identificacaoEquipamento.setFotoPanoramica(evidencias.get(0));
		this.identificacaoEquipamento.setFotoPlaca(evidencias.get(1));
		this.inspecaoVisualExterna.setFoto(evidencias.get(2));
		this.inspecaoVisualInterna.setFotoPanoramica(evidencias.get(3));
		this.inspecaoVisualInterna.setFotoPlaca(evidencias.get(4));
	}
}
