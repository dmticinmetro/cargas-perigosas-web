package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table
@XmlType(name = "MARCO_IDENTIFICACAO_EQUIPAMENTO")
@XmlAccessorType(XmlAccessType.FIELD)
public class IdentificacaoEquipamento6i {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
	
	@Column
	@XmlElement(name = "PRESSAO_PROJETO")
	private double pressaoProjeto;
	
	@Column
	@XmlElement(name = "PRESSAO_ENSAIO_HIDROSTATICO")
	private double pressaoEnsaioHidrostatico;
	
	@Column
	@XmlElement(name = "MATERIAL_COSTADO")
	private String materialCostado;
	
	@Column
	@XmlElement(name = "MATERIAL_CALOTAS")
	private String materialCalotas;
	
	@Column
	@XmlElement(name = "NORMA_FABRICACAO")
	private String normaFabricacao;
	
	@Column
	@XmlElement(name = "DIAMETRO_INTERNO_TANQUE")
	private String diametroTanque;
	
	@Column
	@XmlElement(name = "COMPRIMENTO_TANQUE")
	private double comprimentoTanque;
	
	@Column
	@XmlElement(name = "ESPESSURA_CALOTAS")
	private double espessuraCalotas;
	
	@Column
	@XmlElement(name = "ESPESSURA_COSTADO")
	private double espessuraCostado;
	
	@Column
	@XmlElement(name = "SOBREESPESSURA_CORROSAO")
	private double sobreespessuraCorrosao;
	
	@Column
	@XmlElement(name = "VOLUME_TANQUE")
	private double volumeTanque;
	
	@Column
	@XmlElement(name = "RADIOGRAFIA")
	private String radiografia;
	
	@Column
	@XmlElement(name = "ENSAIO_NAO_DESTRUTIVO")
	private String ensaioNaoDestrutivo;
	
	@Column
	@XmlElement(name = "ALIVIO_TENSOES")
	private String alivioTensoes;
	
	@Column
	@XmlElement(name = "IMG_IDENTIFICACAO_EQUIPAMENTO_PANORAMICA")
	private String fotoPanoramica;
	
	@Column
	@XmlElement(name = "IMG_IDENTIFICACAO_EQUIPAMENTO_PLACA")
	private String fotoPlaca;
	
	public IdentificacaoEquipamento6i(int idIdentificacao6i,
			double pressaoProjeto, double pressaoEnsaioHidrostatico,
			String materialCostado, String materialCalotas,
			String normaFabricacao, String diametroTanque,
			double comprimentoTanque, double espessuraCalotas,
			double espessuraCostado, double sobreespessuraCorrosao,
			double volumeTanque, String radiografia,
			String ensaioNaoDestrutivo, String alivioTensoes,
			String fotoPanoramica, String fotoPlaca) {
		this.id = idIdentificacao6i;
		this.pressaoProjeto = pressaoProjeto;
		this.pressaoEnsaioHidrostatico = pressaoEnsaioHidrostatico;
		this.materialCostado = materialCostado;
		this.materialCalotas = materialCalotas;
		this.normaFabricacao = normaFabricacao;
		this.diametroTanque = diametroTanque;
		this.comprimentoTanque = comprimentoTanque;
		this.espessuraCalotas = espessuraCalotas;
		this.espessuraCostado = espessuraCostado;
		this.sobreespessuraCorrosao = sobreespessuraCorrosao;
		this.volumeTanque = volumeTanque;
		this.radiografia = radiografia;
		this.ensaioNaoDestrutivo = ensaioNaoDestrutivo;
		this.alivioTensoes = alivioTensoes;
		this.fotoPanoramica = fotoPanoramica;
		this.fotoPlaca = fotoPlaca;
	}
	public IdentificacaoEquipamento6i(double pressaoProjeto,
			double pressaoEnsaioHidrostatico, String materialCostado,
			String materialCalotas, String normaFabricacao,
			String diametroTanque, double comprimentoTanque,
			double espessuraCalotas, double espessuraCostado,
			double sobreespessuraCorrosao, double volumeTanque,
			String radiografia, String ensaioNaoDestrutivo,
			String alivioTensoes, String fotoPanoramica, String fotoPlaca) {
		this.pressaoProjeto = pressaoProjeto;
		this.pressaoEnsaioHidrostatico = pressaoEnsaioHidrostatico;
		this.materialCostado = materialCostado;
		this.materialCalotas = materialCalotas;
		this.normaFabricacao = normaFabricacao;
		this.diametroTanque = diametroTanque;
		this.comprimentoTanque = comprimentoTanque;
		this.espessuraCalotas = espessuraCalotas;
		this.espessuraCostado = espessuraCostado;
		this.sobreespessuraCorrosao = sobreespessuraCorrosao;
		this.volumeTanque = volumeTanque;
		this.radiografia = radiografia;
		this.ensaioNaoDestrutivo = ensaioNaoDestrutivo;
		this.alivioTensoes = alivioTensoes;
		this.fotoPanoramica = fotoPanoramica;
		this.fotoPlaca = fotoPlaca;
	}
	public IdentificacaoEquipamento6i() {
	}
	
	public int getId() {
		return id;
	}
	public void setId(int idIdentificacao6i) {
		this.id = idIdentificacao6i;
	}
	public double getPressaoProjeto() {
		return pressaoProjeto;
	}
	public void setPressaoProjeto(double pressaoProjeto) {
		this.pressaoProjeto = pressaoProjeto;
	}
	public double getPressaoEnsaioHidrostatico() {
		return pressaoEnsaioHidrostatico;
	}
	public void setPressaoEnsaioHidrostatico(double pressaoEnsaioHidrostatico) {
		this.pressaoEnsaioHidrostatico = pressaoEnsaioHidrostatico;
	}
	public String getMaterialCostado() {
		return materialCostado;
	}
	public void setMaterialCostado(String materialCostado) {
		this.materialCostado = materialCostado;
	}
	public String getMaterialCalotas() {
		return materialCalotas;
	}
	public void setMaterialCalotas(String materialCalotas) {
		this.materialCalotas = materialCalotas;
	}
	public String getNormaFabricacao() {
		return normaFabricacao;
	}
	public void setNormaFabricacao(String normaFabricacao) {
		this.normaFabricacao = normaFabricacao;
	}
	public String getDiametroTanque() {
		return diametroTanque;
	}
	public void setDiametroTanque(String diametroTanque) {
		this.diametroTanque = diametroTanque;
	}
	public double getComprimentoTanque() {
		return comprimentoTanque;
	}
	public void setComprimentoTanque(double comprimentoTanque) {
		this.comprimentoTanque = comprimentoTanque;
	}
	public double getEspessuraCalotas() {
		return espessuraCalotas;
	}
	public void setEspessuraCalotas(double espessuraCalotas) {
		this.espessuraCalotas = espessuraCalotas;
	}
	public double getEspessuraCostado() {
		return espessuraCostado;
	}
	public void setEspessuraCostado(double espessuraCostado) {
		this.espessuraCostado = espessuraCostado;
	}
	public double getSobreespessuraCorrosao() {
		return sobreespessuraCorrosao;
	}
	public void setSobreespessuraCorrosao(double sobreespessuraCorrosao) {
		this.sobreespessuraCorrosao = sobreespessuraCorrosao;
	}
	public double getVolumeTanque() {
		return volumeTanque;
	}
	public void setVolumeTanque(double volumeTanque) {
		this.volumeTanque = volumeTanque;
	}
	public String getRadiografia() {
		return radiografia;
	}
	public void setRadiografia(String radiografia) {
		this.radiografia = radiografia;
	}
	public String getEnsaioNaoDestrutivo() {
		return ensaioNaoDestrutivo;
	}
	public void setEnsaioNaoDestrutivo(String ensaioNaoDestrutivo) {
		this.ensaioNaoDestrutivo = ensaioNaoDestrutivo;
	}
	public String getAlivioTensoes() {
		return alivioTensoes;
	}
	public void setAlivioTensoes(String alivioTensoes) {
		this.alivioTensoes = alivioTensoes;
	}
	public String getFotoPanoramica() {
		return fotoPanoramica;
	}
	public void setFotoPanoramica(String fotoPanoramica) {
		this.fotoPanoramica = fotoPanoramica;
	}
	public String getFotoPlaca() {
		return fotoPlaca;
	}
	public void setFotoPlaca(String fotoPlaca) {
		this.fotoPlaca = fotoPlaca;
	}
}
