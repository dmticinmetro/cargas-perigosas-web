package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table
@XmlType(name = "MARCO_ESPESSURAS_MINIMAS")
@XmlAccessorType(XmlAccessType.FIELD)
public class EspessurasMinimas1i {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
	
	@Column
	@XmlElement(name = "ESPESSURA_MINIMA_COSTADO")
	private double espMinCostado; 
	
	@Column
    @XmlElement(name = "ESPESSURA_MINIMA_CALOTAS_DIANTEIRA")
    private double espMinCalotasDianteira;
    
	@Column
    @XmlElement(name = "ESPESSURA_MINIMA_CALOTAS_TRASEIRA")
    private double espMinCalotasTraseira;
    
	@Column
    @XmlElement(name = "ESPESSURA_MINIMA_TAMPA_BOCA")
    private double espMinTampaBoca;

	public EspessurasMinimas1i(int idEspessuraMinimas, double espMinCostado,
			double espMinCalotasDianteira, double espMinCalotasTraseira,
			double espMinTampaBoca) {
		this.id = idEspessuraMinimas;
		this.espMinCostado = espMinCostado;
		this.espMinCalotasDianteira = espMinCalotasDianteira;
		this.espMinCalotasTraseira = espMinCalotasTraseira;
		this.espMinTampaBoca = espMinTampaBoca;
	}
	public EspessurasMinimas1i(double espMinCostado,
			double espMinCalotasDianteira, double espMinCalotasTraseira,
			double espMinTampaBoca) {
		this.espMinCostado = espMinCostado;
		this.espMinCalotasDianteira = espMinCalotasDianteira;
		this.espMinCalotasTraseira = espMinCalotasTraseira;
		this.espMinTampaBoca = espMinTampaBoca;
	}
	public EspessurasMinimas1i() {
	}

	public int getId() {
		return id;
	}
	public void setId(int idEspessuraMinimas) {
		this.id = idEspessuraMinimas;
	}
	public double getEspMinCostado() {
		return espMinCostado;
	}
	public void setEspMinCostado(double espMinCostado) {
		this.espMinCostado = espMinCostado;
	}
	public double getEspMinCalotasDianteira() {
		return espMinCalotasDianteira;
	}
	public void setEspMinCalotasDianteira(double espMinCalotasDianteira) {
		this.espMinCalotasDianteira = espMinCalotasDianteira;
	}
	public double getEspMinCalotasTraseira() {
		return espMinCalotasTraseira;
	}
	public void setEspMinCalotasTraseira(double espMinCalotasTraseira) {
		this.espMinCalotasTraseira = espMinCalotasTraseira;
	}
	public double getEspMinTampaBoca() {
		return espMinTampaBoca;
	}
	public void setEspMinTampaBoca(double espMinTampaBoca) {
		this.espMinTampaBoca = espMinTampaBoca;
	}
}
