package model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table
@XmlType(name = "MARCO_ENSAIO_VALVULAS")
@XmlAccessorType(XmlAccessType.FIELD)
public class EnsaioValvulas7i {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn
	@XmlElement(name = "VALVULA_ALIVIO")
	private ValvulaAlivio7i valvulaAlivio;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn
	@XmlElement(name = "VALVULA_VACUO")
	private ValvulaVacuo7i valvulaVacuo;
	
	public EnsaioValvulas7i(int idValvulas7i, ValvulaAlivio7i valvulaAlivio,
			ValvulaVacuo7i valvulaVacuo) {
		this.id = idValvulas7i;
		this.valvulaAlivio = valvulaAlivio;
		this.valvulaVacuo = valvulaVacuo;
	}
	public EnsaioValvulas7i(ValvulaAlivio7i valvulaAlivio, ValvulaVacuo7i valvulaVacuo) {
		this.valvulaAlivio = valvulaAlivio;
		this.valvulaVacuo = valvulaVacuo;
	}
	public EnsaioValvulas7i() {
	}
	
	public int getId() {
		return id;
	}
	public void setId(int idValvulas7i) {
		this.id = idValvulas7i;
	}
	public ValvulaAlivio7i getValvulaAlivio() {
		return valvulaAlivio;
	}
	public void setValvulaAlivio(ValvulaAlivio7i valvulaAlivio) {
		this.valvulaAlivio = valvulaAlivio;
	}
	public ValvulaVacuo7i getValvulaVacuo() {
		return valvulaVacuo;
	}
	public void setValvulaVacuo(ValvulaVacuo7i valvulaVacuo) {
		this.valvulaVacuo = valvulaVacuo;
	}
}
