package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table
@XmlType(name = "MARCO_ENSAIO_HIDROSTARICO")
@XmlAccessorType(XmlAccessType.FIELD)
public class EnsaioHidrostatico6i {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
    private int id;
    
	@Column
    @XmlElement(name = "PRESSAO_ENSAIO")
    private double pressaoEnsaio;
    
	@Column
    @XmlElement(name = "TEMPO_DURACAO")
    private String duracao;
    
	@Column
    @XmlElement(name = "MANOMETRO1")
    private int manometro1;
    
	@Column
    @XmlElement(name = "VALIDADE_MANOMETRO1")
    private String validadeManometro1;
    
	@Column
    @XmlElement(name = "MANOMETRO2")
    private int manometro2;
    
	@Column
    @XmlElement(name = "VALIDADE_MANOMETRO2")
    private String validadeManometro2;

	public EnsaioHidrostatico6i(int idEnsaioHidrostatico, double pressaoEnsaio,
			String duracao, int manometro1, String validadeManometro1,
			int manometro2, String validadeManometro2) {
		this.id = idEnsaioHidrostatico;
		this.pressaoEnsaio = pressaoEnsaio;
		this.duracao = duracao;
		this.manometro1 = manometro1;
		this.validadeManometro1 = validadeManometro1;
		this.manometro2 = manometro2;
		this.validadeManometro2 = validadeManometro2;
	}
	public EnsaioHidrostatico6i(double pressaoEnsaio, String duracao,
			int manometro1, String validadeManometro1, int manometro2,
			String validadeManometro2) {
		this.pressaoEnsaio = pressaoEnsaio;
		this.duracao = duracao;
		this.manometro1 = manometro1;
		this.validadeManometro1 = validadeManometro1;
		this.manometro2 = manometro2;
		this.validadeManometro2 = validadeManometro2;
	}
	public EnsaioHidrostatico6i() {
	}
	
	public int getId() {
		return id;
	}
	public void setId(int idEnsaioHidrostatico) {
		this.id = idEnsaioHidrostatico;
	}
	public double getPressaoEnsaio() {
		return pressaoEnsaio;
	}
	public void setPressaoEnsaio(double pressaoEnsaio) {
		this.pressaoEnsaio = pressaoEnsaio;
	}
	public String getDuracao() {
		return duracao;
	}
	public void setDuracao(String duracao) {
		this.duracao = duracao;
	}
	public int getManometro1() {
		return manometro1;
	}
	public void setManometro1(int manometro1) {
		this.manometro1 = manometro1;
	}
	public String getValidadeManometro1() {
		return validadeManometro1;
	}
	public void setValidadeManometro1(String validadeManometro1) {
		this.validadeManometro1 = validadeManometro1;
	}
	public int getManometro2() {
		return manometro2;
	}
	public void setManometro2(int manometro2) {
		this.manometro2 = manometro2;
	}
	public String getValidadeManometro2() {
		return validadeManometro2;
	}
	public void setValidadeManometro2(String validadeManometro2) {
		this.validadeManometro2 = validadeManometro2;
	}
}
