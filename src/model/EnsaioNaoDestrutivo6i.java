package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table
@XmlType(name = "MARCO_ENSAIO_NAO_DESTRUTIVO")
@XmlAccessorType(XmlAccessType.FIELD)
public class EnsaioNaoDestrutivo6i {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
	
	@Column
	@XmlElement(name = "LP")
	private String lp;
	
	@Column
	@XmlElement(name = "PORC_SOLDAS_LP")
	private int soldasLP;
	
	@Column
	@XmlElement(name = "PM")
	private String pm;
	
	@Column
	@XmlElement(name = "PORC_SOLDAS_PM")
	private int soldasPm;
	
	@Column
	@XmlElement(name = "US")
	private String us;
	
	@Column
	@XmlElement(name = "PORC_SOLDAS_US")
	private int soldasUs;
	
	@Column
	@XmlElement(name = "RD")
	private String rd;
	
	@Column
	@XmlElement(name = "PORC_SOLDAS_RD")
	private int soldasRd;

	public EnsaioNaoDestrutivo6i(int idNaoDestrutivo6i, String lp,
			int soldasLP, String pm, int soldasPm, String us, int soldasUs,
			String rd, int soldasRd) {
		this.id = idNaoDestrutivo6i;
		this.lp = lp;
		this.soldasLP = soldasLP;
		this.pm = pm;
		this.soldasPm = soldasPm;
		this.us = us;
		this.soldasUs = soldasUs;
		this.rd = rd;
		this.soldasRd = soldasRd;
	}
	public EnsaioNaoDestrutivo6i(String lp, int soldasLP, String pm,
			int soldasPm, String us, int soldasUs, String rd, int soldasRd) {
		this.lp = lp;
		this.soldasLP = soldasLP;
		this.pm = pm;
		this.soldasPm = soldasPm;
		this.us = us;
		this.soldasUs = soldasUs;
		this.rd = rd;
		this.soldasRd = soldasRd;
	}
	public EnsaioNaoDestrutivo6i() {
	}

	public int getId() {
		return id;
	}
	public void setId(int idNaoDestrutivo6i) {
		this.id = idNaoDestrutivo6i;
	}
	public String getLp() {
		return lp;
	}
	public void setLp(String lp) {
		this.lp = lp;
	}
	public int getSoldasLP() {
		return soldasLP;
	}
	public void setSoldasLP(int soldasLP) {
		this.soldasLP = soldasLP;
	}
	public String getPm() {
		return pm;
	}
	public void setPm(String pm) {
		this.pm = pm;
	}
	public int getSoldasPm() {
		return soldasPm;
	}
	public void setSoldasPm(int soldasPm) {
		this.soldasPm = soldasPm;
	}
	public String getUs() {
		return us;
	}
	public void setUs(String us) {
		this.us = us;
	}
	public int getSoldasUs() {
		return soldasUs;
	}
	public void setSoldasUs(int soldasUs) {
		this.soldasUs = soldasUs;
	}
	public String getRd() {
		return rd;
	}
	public void setRd(String rd) {
		this.rd = rd;
	}
	public int getSoldasRd() {
		return soldasRd;
	}
	public void setSoldasRd(int soldasRd) {
		this.soldasRd = soldasRd;
	}
}
