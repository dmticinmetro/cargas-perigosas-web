package model;

import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement(name = "INSPECAO")
@XmlAccessorType(XmlAccessType.FIELD)
public class Inspecao6i {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "head")
	@XmlElement(name = "HEAD")
	private Head head;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "identificacaoEquipamento")
	@XmlElement(name = "MARCO_IDENTIFICACAO_EQUIPAMENTO")
	private IdentificacaoEquipamento6i identificacaoEquipamento;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ensaioHidrostatico")
	@XmlElement(name = "MARCO_ENSAIO_HIDROSTARICO")
	private EnsaioHidrostatico6i ensaioHidrostatico;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "valvulas")
	@XmlElement(name = "MARCO_VALVULAS")
	private Valvulas6i valvulas;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ensaioNaoDestrutivo")
	@XmlElement(name = "MARCO_ENSAIO_NAO_DESTRUTIVO")
	private EnsaioNaoDestrutivo6i ensaioNaoDestrutivo;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "aptoTransportarGrupos")
	@XmlElement(name = "MARCO_APTO_TRANSPORTAR_GRUPOS")
	private Grupos aptoTransportarGrupos;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "inspecaoVisualExterna")
	@XmlElement(name = "MARCO_INSPECAO_VISUAL_EXTERNA")
	private InspecaoVisualExterna inspecaoVisualExterna;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "inspecaoVisualInterna")
	@XmlElement(name = "MARCO_INSPECAO_VISUAL_INTERNA")
	private InspecaoVisualInterna inspecaoVisualInterna;
		
	public Inspecao6i(int idInspecao, Head head,
			IdentificacaoEquipamento6i identificacaoEquipamento,
			EnsaioHidrostatico6i ensaioHidrostatico, Valvulas6i valvulas,
			EnsaioNaoDestrutivo6i ensaioNaoDestrutivo,
			Grupos aptoTransportarGrupos,
			InspecaoVisualExterna inspecaoVisualExterna,
			InspecaoVisualInterna inspecaoVisualInterna) {
		this.id = idInspecao;
		this.head = head;
		this.identificacaoEquipamento = identificacaoEquipamento;
		this.ensaioHidrostatico = ensaioHidrostatico;
		this.valvulas = valvulas;
		this.ensaioNaoDestrutivo = ensaioNaoDestrutivo;
		this.aptoTransportarGrupos = aptoTransportarGrupos;
		this.inspecaoVisualExterna = inspecaoVisualExterna;
		this.inspecaoVisualInterna = inspecaoVisualInterna;
	}
	public Inspecao6i(Head head,
			IdentificacaoEquipamento6i identificacaoEquipamento,
			EnsaioHidrostatico6i ensaioHidrostatico, Valvulas6i valvulas,
			EnsaioNaoDestrutivo6i ensaioNaoDestrutivo,
			Grupos aptoTransportarGrupos,
			InspecaoVisualExterna inspecaoVisualExterna,
			InspecaoVisualInterna inspecaoVisualInterna) {
		this.head = head;
		this.identificacaoEquipamento = identificacaoEquipamento;
		this.ensaioHidrostatico = ensaioHidrostatico;
		this.valvulas = valvulas;
		this.ensaioNaoDestrutivo = ensaioNaoDestrutivo;
		this.aptoTransportarGrupos = aptoTransportarGrupos;
		this.inspecaoVisualExterna = inspecaoVisualExterna;
		this.inspecaoVisualInterna = inspecaoVisualInterna;
	}
	public Inspecao6i() {
	}
	
	public int getId() {
		return id;
	}
	public void setId(int idInspecao) {
		this.id = idInspecao;
	}
	public Head getHead() {
		return head;
	}
	public void setHead(Head head) {
		this.head = head;
	}
	public IdentificacaoEquipamento6i getIdentificacaoEquipamento() {
		return identificacaoEquipamento;
	}
	public void setIdentificacaoEquipamento(
			IdentificacaoEquipamento6i identificacaoEquipamento) {
		this.identificacaoEquipamento = identificacaoEquipamento;
	}
	public EnsaioHidrostatico6i getEnsaioHidrostatico() {
		return ensaioHidrostatico;
	}
	public void setEnsaioHidrostatico(EnsaioHidrostatico6i ensaioHidrostatico) {
		this.ensaioHidrostatico = ensaioHidrostatico;
	}
	public Valvulas6i getValvulas() {
		return valvulas;
	}
	public void setValvulas(Valvulas6i valvulas) {
		this.valvulas = valvulas;
	}
	public EnsaioNaoDestrutivo6i getEnsaioNaoDestrutivo() {
		return ensaioNaoDestrutivo;
	}
	public void setEnsaioNaoDestrutivo(EnsaioNaoDestrutivo6i ensaioNaoDestrutivo) {
		this.ensaioNaoDestrutivo = ensaioNaoDestrutivo;
	}
	public Grupos getAptoTransportarGrupos() {
		return aptoTransportarGrupos;
	}
	public void setAptoTransportarGrupos(Grupos aptoTransportarGrupos) {
		this.aptoTransportarGrupos = aptoTransportarGrupos;
	}
	public InspecaoVisualExterna getInspecaoVisualExterna() {
		return inspecaoVisualExterna;
	}
	public void setInspecaoVisualExterna(InspecaoVisualExterna inspecaoVisualExterna) {
		this.inspecaoVisualExterna = inspecaoVisualExterna;
	}
	public InspecaoVisualInterna getInspecaoVisualInterna() {
		return inspecaoVisualInterna;
	}
	public void setInspecaoVisualInterna(InspecaoVisualInterna inspecaoVisualInterna) {
		this.inspecaoVisualInterna = inspecaoVisualInterna;
	}
	
	public ArrayList<String> getTodasEvidencias(){
		ArrayList<String> evidencias = new ArrayList<String>();
		evidencias.add(this.identificacaoEquipamento.getFotoPanoramica());
		evidencias.add(this.identificacaoEquipamento.getFotoPlaca());
		evidencias.add(this.inspecaoVisualExterna.getFoto());
		evidencias.add(this.inspecaoVisualInterna.getFotoPanoramica());
		evidencias.add(this.inspecaoVisualInterna.getFotoPlaca());
		return evidencias;
	}
	
	public void setTodasEvidencias(ArrayList<String> evidencias){
		this.identificacaoEquipamento.setFotoPanoramica(evidencias.get(0));
		this.identificacaoEquipamento.setFotoPlaca(evidencias.get(1));
		this.inspecaoVisualExterna.setFoto(evidencias.get(2));
		this.inspecaoVisualInterna.setFotoPanoramica(evidencias.get(3));
		this.inspecaoVisualInterna.setFotoPlaca(evidencias.get(4));
	}
}
