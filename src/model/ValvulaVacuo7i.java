package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table
@XmlType(name = "VALVULA_VACUO")
@XmlAccessorType(XmlAccessType.FIELD)
public class ValvulaVacuo7i {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
	
	@Column
	@XmlElement(name = "PRESSAO_ABERTURA")
	private double pressaoAbertura;
	
	@Column
	@XmlElement(name = "PRESSAO_MAXIMA")
    private double pressaoMaxima;
	
	public ValvulaVacuo7i(int idValvulasHidrostatico, double pressaoAbertura,
			double pressaoMaxima) {
		this.id = idValvulasHidrostatico;
		this.pressaoAbertura = pressaoAbertura;
		this.pressaoMaxima = pressaoMaxima;
	}
	public ValvulaVacuo7i(double pressaoAbertura, double pressaoMaxima) {
		this.pressaoAbertura = pressaoAbertura;
		this.pressaoMaxima = pressaoMaxima;
	}
	public ValvulaVacuo7i() {
	}
	
	public int getId() {
		return id;
	}
	public void setId(int idValvulasHidrostatico) {
		this.id = idValvulasHidrostatico;
	}
	public double getPressaoAbertura() {
		return pressaoAbertura;
	}
	public void setPressaoAbertura(double pressaoAbertura) {
		this.pressaoAbertura = pressaoAbertura;
	}
	public double getPressaoMaxima() {
		return pressaoMaxima;
	}
	public void setPressaoMaxima(double pressaoMaxima) {
		this.pressaoMaxima = pressaoMaxima;
	}
}
