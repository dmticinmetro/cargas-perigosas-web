package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table
@XmlType(name = "MARCO_IDENTIFICACAO_EQUIPAMENTO")
@XmlAccessorType(XmlAccessType.FIELD)
public class IdentificacaoEquipamento7i {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
	
	@Column
	@XmlElement(name = "FABRICANTE")
	private String fabricante;
	
	@Column
	@XmlElement(name = "DATA_FABRICACAO")
	private String dataFabricacao;
	
	@Column
	@XmlElement(name = "TIPO_EQUIPAMENTO")
	private String tipo;
	
	@Column
	@XmlElement(name = "PRESSAO_PROJETO")
	private double pressaoProjeto;
	
	@Column
	@XmlElement(name = "PRESSAO_ENSAIO_HIDROSTATICO")
	private double pressaoEnsaio;
	
	@Column
	@XmlElement(name = "MATERIAL_COSTADO")
	private String materialCostado;
	
	@Column
	@XmlElement(name = "MATERIAL_CALOTAS")
	private String materialCalotas;
	
	@Column
	@XmlElement(name = "NUMERO_COMPARTIMENTOS")
	private int numCompartimentos;
	
	@Column
	@XmlElement(name = "TANQUE_CILINDRICO")
	private String tanqueCilindrico;
	
	@Column
	@XmlElement(name = "DIAMENTRO_INTERNO")
	private double diametroInterno;
	
	@Column
	@XmlElement(name = "TANQUE_POLICENTRICO")
	private String tanquePolicentrico;
	
	@Column
	@XmlElement(name = "RAIO_MAXIMO_CURVATURA")
	private double raioCurvatura;
	
	@Column
	@XmlElement(name = "TANQUE_REVESTIDO")
	private String tanqueRevestido;
	
	@Column
	@XmlElement(name = "ESPESSURA_COSTADO")
	private double espessuraCostado;
	
	@Column
	@XmlElement(name = "ESPESSURA_CALOTAS")
	private double espessuraCalotas;
	
	@Column
	@XmlElement(name = "SOBREESPESSURA_CORROSAO")
	private double sobreespessuraCorrosao;
	
	@Column
	@XmlElement(name = "VOLUME_TANQUE")
	private double volumeTanque;
	
	@Column
	@XmlElement(name = "IMG_IDENTIFICACAO_EQUIPAMENTO_PANORAMICA")
	private String ref_imagem_evidencia_panoramica;
	
	@Column
	@XmlElement(name = "IMG_IDENTIFICACAO_EQUIPAMENTO_PLACA")
	private String ref_imagem_evidencia_placa;

	public IdentificacaoEquipamento7i(int idIdentificacao7i, String fabricante,
			String dataFabricacao, String tipo, double pressaoProjeto,
			double pressaoEnsaio, String materialCostado,
			String materialCalotas, int numCompartimentos,
			String tanqueCilindrico, double diametroInterno,
			String tanquePolicentrico, double raioCurvatura,
			String tanqueRevestido, double espessuraCostado,
			double espessuraCalotas, double sobreespessuraCorrosao,
			double volumeTanque, String ref_imagem_evidencia_panoramica,
			String ref_imagem_evidencia_placa) {
		this.id = idIdentificacao7i;
		this.fabricante = fabricante;
		this.dataFabricacao = dataFabricacao;
		this.tipo = tipo;
		this.pressaoProjeto = pressaoProjeto;
		this.pressaoEnsaio = pressaoEnsaio;
		this.materialCostado = materialCostado;
		this.materialCalotas = materialCalotas;
		this.numCompartimentos = numCompartimentos;
		this.tanqueCilindrico = tanqueCilindrico;
		this.diametroInterno = diametroInterno;
		this.tanquePolicentrico = tanquePolicentrico;
		this.raioCurvatura = raioCurvatura;
		this.tanqueRevestido = tanqueRevestido;
		this.espessuraCostado = espessuraCostado;
		this.espessuraCalotas = espessuraCalotas;
		this.sobreespessuraCorrosao = sobreespessuraCorrosao;
		this.volumeTanque = volumeTanque;
		this.ref_imagem_evidencia_panoramica = ref_imagem_evidencia_panoramica;
		this.ref_imagem_evidencia_placa = ref_imagem_evidencia_placa;
	}
	public IdentificacaoEquipamento7i(String fabricante, String dataFabricacao,
			String tipo, double pressaoProjeto, double pressaoEnsaio,
			String materialCostado, String materialCalotas,
			int numCompartimentos, String tanqueCilindrico,
			double diametroInterno, String tanquePolicentrico,
			double raioCurvatura, String tanqueRevestido,
			double espessuraCostado, double espessuraCalotas,
			double sobreespessuraCorrosao, double volumeTanque,
			String ref_imagem_evidencia_panoramica,
			String ref_imagem_evidencia_placa) {
		this.fabricante = fabricante;
		this.dataFabricacao = dataFabricacao;
		this.tipo = tipo;
		this.pressaoProjeto = pressaoProjeto;
		this.pressaoEnsaio = pressaoEnsaio;
		this.materialCostado = materialCostado;
		this.materialCalotas = materialCalotas;
		this.numCompartimentos = numCompartimentos;
		this.tanqueCilindrico = tanqueCilindrico;
		this.diametroInterno = diametroInterno;
		this.tanquePolicentrico = tanquePolicentrico;
		this.raioCurvatura = raioCurvatura;
		this.tanqueRevestido = tanqueRevestido;
		this.espessuraCostado = espessuraCostado;
		this.espessuraCalotas = espessuraCalotas;
		this.sobreespessuraCorrosao = sobreespessuraCorrosao;
		this.volumeTanque = volumeTanque;
		this.ref_imagem_evidencia_panoramica = ref_imagem_evidencia_panoramica;
		this.ref_imagem_evidencia_placa = ref_imagem_evidencia_placa;
	}
	public IdentificacaoEquipamento7i() {
	}
	
	public int getId() {
		return id;
	}
	public void setId(int idIdentificacao7i) {
		this.id = idIdentificacao7i;
	}
	public String getFabricante() {
		return fabricante;
	}
	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}
	public String getDataFabricacao() {
		return dataFabricacao;
	}
	public void setDataFabricacao(String dataFabricacao) {
		this.dataFabricacao = dataFabricacao;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public double getPressaoProjeto() {
		return pressaoProjeto;
	}
	public void setPressaoProjeto(double pressaoProjeto) {
		this.pressaoProjeto = pressaoProjeto;
	}
	public double getPressaoEnsaio() {
		return pressaoEnsaio;
	}
	public void setPressaoEnsaio(double pressaoEnsaio) {
		this.pressaoEnsaio = pressaoEnsaio;
	}
	public String getMaterialCostado() {
		return materialCostado;
	}
	public void setMaterialCostado(String materialCostado) {
		this.materialCostado = materialCostado;
	}
	public String getMaterialCalotas() {
		return materialCalotas;
	}
	public void setMaterialCalotas(String materialCalotas) {
		this.materialCalotas = materialCalotas;
	}
	public int getNumCompartimentos() {
		return numCompartimentos;
	}
	public void setNumCompartimentos(int numCompartimentos) {
		this.numCompartimentos = numCompartimentos;
	}
	public String getTanqueCilindrico() {
		return tanqueCilindrico;
	}
	public void setTanqueCilindrico(String tanqueCilindrico) {
		this.tanqueCilindrico = tanqueCilindrico;
	}
	public double getDiametroInterno() {
		return diametroInterno;
	}
	public void setDiametroInterno(double diametroInterno) {
		this.diametroInterno = diametroInterno;
	}
	public String getTanquePolicentrico() {
		return tanquePolicentrico;
	}
	public void setTanquePolicentrico(String tanquePolicentrico) {
		this.tanquePolicentrico = tanquePolicentrico;
	}
	public double getRaioCurvatura() {
		return raioCurvatura;
	}
	public void setRaioCurvatura(double raioCurvatura) {
		this.raioCurvatura = raioCurvatura;
	}
	public String getTanqueRevestido() {
		return tanqueRevestido;
	}
	public void setTanqueRevestido(String tanqueRevestido) {
		this.tanqueRevestido = tanqueRevestido;
	}
	public double getEspessuraCostado() {
		return espessuraCostado;
	}
	public void setEspessuraCostado(double espessuraCostado) {
		this.espessuraCostado = espessuraCostado;
	}
	public double getEspessuraCalotas() {
		return espessuraCalotas;
	}
	public void setEspessuraCalotas(double espessuraCalotas) {
		this.espessuraCalotas = espessuraCalotas;
	}
	public double getSobreespessuraCorrosao() {
		return sobreespessuraCorrosao;
	}
	public void setSobreespessuraCorrosao(double sobreespessuraCorrosao) {
		this.sobreespessuraCorrosao = sobreespessuraCorrosao;
	}
	public double getVolumeTanque() {
		return volumeTanque;
	}
	public void setVolumeTanque(double volumeTanque) {
		this.volumeTanque = volumeTanque;
	}
	public String getRef_imagem_evidencia_panoramica() {
		return ref_imagem_evidencia_panoramica;
	}
	public void setRef_imagem_evidencia_panoramica(
			String ref_imagem_evidencia_panoramica) {
		this.ref_imagem_evidencia_panoramica = ref_imagem_evidencia_panoramica;
	}
	public String getRef_imagem_evidencia_placa() {
		return ref_imagem_evidencia_placa;
	}
	public void setRef_imagem_evidencia_placa(String ref_imagem_evidencia_placa) {
		this.ref_imagem_evidencia_placa = ref_imagem_evidencia_placa;
	}
}
