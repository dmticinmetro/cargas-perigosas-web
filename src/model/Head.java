package model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import util.JaxbDateFormat;

@Entity
@XmlType(name = "HEAD")
@XmlAccessorType(XmlAccessType.FIELD)
public class Head {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
		
	@Column(unique = true)
	@XmlElement(name = "CIPP")
	private int cipp;
	
	@Column
	@XmlElement(name = "OIA")
	private String oia;
	
	@Column
    @XmlElement(name = "TIPO_ENSAIO")
    private String tipoEnsaio;
	
	@Column
    @XmlElement(name = "DATA_INSPECAO")
	@XmlJavaTypeAdapter(JaxbDateFormat.class)
    private Date dataInspecao;
	
	@Column
    @XmlElement(name = "DATA_PROXIMA_INSPECAO")
	@XmlJavaTypeAdapter(JaxbDateFormat.class)
    private Date dataProximaInspecao;
	
	@Column
    @XmlElement(name = "SUPERVISOR")
    private String supervisor;
	
	@Column
    @XmlElement(name = "INSPETOR")
    private String inspetor;
	
	@Column
    @XmlElement(name = "CLIENTE")
    private String cliente;
	
	@Column
    @XmlElement(name = "LOCAL_INSPECAO")
    private String localInspecao;
	
	@Column
    @XmlElement(name = "LACRE")
    private String lacre;
	
	@Column
    @XmlElement(name = "NUMERO_CHASSI")
    private String chassi;
	
	@Column
    @XmlElement(name = "NUMERO_RENAVAM")
    private String renavam;
	
	@Column
    @XmlElement(name = "PLACA_VEICULO")
    private String placa;
	
	@Column
    @XmlElement(name = "NUMERO_EQUIPAMENTO")
    private String equipamento;
	
	@Column
    @XmlElement(name = "RELATORIO_RNC")
    private String relatorio;

	public int getId() {
		return id;
	}

	public void setId(int idHead) {
		this.id = idHead;
	}

	public int getCipp() {
		return cipp;
	}

	public void setCipp(int cipp) {
		this.cipp = cipp;
	}

	public String getOia() {
		return oia;
	}

	public void setOia(String oia) {
		this.oia = oia;
	}

	public String getTipoEnsaio() {
		return tipoEnsaio;
	}

	public void setTipoEnsaio(String tipoEnsaio) {
		this.tipoEnsaio = tipoEnsaio;
	}

	//@XmlJavaTypeAdapter(JaxbDateFormat.class)
	public Date getDataInspecao() {
		return dataInspecao;
	}

	//@XmlJavaTypeAdapter(JaxbDateFormat.class)
	public void setDataInspecao(Date dataInspecao) {
		this.dataInspecao = dataInspecao;
	}

	//@XmlJavaTypeAdapter(JaxbDateFormat.class)
	public Date getDataProximaInspecao() {
		return dataProximaInspecao;
	}

	//@XmlJavaTypeAdapter(JaxbDateFormat.class)
	public void setDataProximaInspecao(Date dataProximaInspecao) {
		this.dataProximaInspecao = dataProximaInspecao;
	}

	public String getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}

	public String getInspetor() {
		return inspetor;
	}

	public void setInspetor(String inspetor) {
		this.inspetor = inspetor;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getLocalInspecao() {
		return localInspecao;
	}

	public void setLocalInspecao(String localInspecao) {
		this.localInspecao = localInspecao;
	}

	public String getLacre() {
		return lacre;
	}

	public void setLacre(String lacre) {
		this.lacre = lacre;
	}

	public String getChassi() {
		return chassi;
	}

	public void setChassi(String chassi) {
		this.chassi = chassi;
	}

	public String getRenavam() {
		return renavam;
	}

	public void setRenavam(String renavan) {
		this.renavam = renavan;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getEquipamento() {
		return equipamento;
	}

	public void setEquipamento(String equipamento) {
		this.equipamento = equipamento;
	}

	public String getRelatorio() {
		return relatorio;
	}

	public void setRelatorio(String relatorio) {
		this.relatorio = relatorio;
	}
}
