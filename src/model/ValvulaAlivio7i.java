package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Entity
@Table
@XmlAccessorType(XmlAccessType.FIELD)
public class ValvulaAlivio7i {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
	
	@Column
	@XmlElement(name = "PRESSAO_ABERTURA")
	private double pressaoAbertura;
	
	@Column
	@XmlElement(name = "PRESSAO_FECHAMENTO")
    private double pressaoFechamento;

	public ValvulaAlivio7i(int idValvulaAlivio7i, double pressaoAbertura,
			double pressaoFechamento) {
		this.id = idValvulaAlivio7i;
		this.pressaoAbertura = pressaoAbertura;
		this.pressaoFechamento = pressaoFechamento;
	}

	public ValvulaAlivio7i(double pressaoAbertura, double pressaoFechamento) {
		this.pressaoAbertura = pressaoAbertura;
		this.pressaoFechamento = pressaoFechamento;
	}

	public ValvulaAlivio7i() {
	}

	public int getId() {
		return id;
	}

	public void setId(int idValvulaAlivio7i) {
		this.id = idValvulaAlivio7i;
	}

	public double getPressaoAbertura() {
		return pressaoAbertura;
	}

	public void setPressaoAbertura(double pressaoAbertura) {
		this.pressaoAbertura = pressaoAbertura;
	}

	public double getPressaoFechamento() {
		return pressaoFechamento;
	}

	public void setPressaoFechamento(double pressaoFechamento) {
		this.pressaoFechamento = pressaoFechamento;
	}
}
