package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table
@XmlType(name = "MARCO_ENSAIO_HIDROSTARICO")
@XmlAccessorType(XmlAccessType.FIELD)
public class EnsaioHidrostatico3i {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
	
	@Column
	@XmlElement(name = "PRESSAO_ENSAIO")
	private double pressaoEnsaio;
	
	@Column
	@XmlElement(name = "TEMPO_DURACAO")
	private String duracao;

	public EnsaioHidrostatico3i(int idEnsaioHidrostatico3i,
			double pressaoEnsaio, String duracao) {
		this.id = idEnsaioHidrostatico3i;
		this.pressaoEnsaio = pressaoEnsaio;
		this.duracao = duracao;
	}
	public EnsaioHidrostatico3i(double pressaoEnsaio, String duracao) {
		this.pressaoEnsaio = pressaoEnsaio;
		this.duracao = duracao;
	}
	public EnsaioHidrostatico3i() {
	}
	
	public int getId() {
		return id;
	}
	public void setId(int idEnsaioHidrostatico3i) {
		this.id = idEnsaioHidrostatico3i;
	}
	public double getPressaoEnsaio() {
		return pressaoEnsaio;
	}
	public void setPressaoEnsaio(double pressaoEnsaio) {
		this.pressaoEnsaio = pressaoEnsaio;
	}
	public String getDuracao() {
		return duracao;
	}
	public void setDuracao(String duracao) {
		this.duracao = duracao;
	}
}
