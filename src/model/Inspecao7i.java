package model;

import java.util.ArrayList;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement(name = "INSPECAO")
@XmlAccessorType(XmlAccessType.FIELD)
public class Inspecao7i {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "head")
	@XmlElement(name = "HEAD")
	private Head head;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "identificacaoEquipamento")
	@XmlElement(name = "MARCO_IDENTIFICACAO_EQUIPAMENTO")
	private IdentificacaoEquipamento7i marcoIdentificacaoEquipamento7i;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ensaioHidrostaticoPneumatico")
	@XmlElement(name = "MARCO_ENSAIO_HIDROSTATICO_PNEUMATICO")
	private EnsaioHidrostaticoPneumatico7i marcoEnsaioHidrostaticoPneumatico7i;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "ensaioValvulas")
	@XmlElements(value = { @XmlElement(name = "MARCO_ENSAIO_VALVULAS") })
	private Set<EnsaioValvulas7i> ensaioValvulas7i;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "inspecaoVisualExterna")
	@XmlElement(name = "MARCO_INSPECAO_VISUAL_EXTERNA")
	private InspecaoVisualExterna inspecaoVisualExterna;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "inspecaoVisualInterna")
	@XmlElement(name = "MARCO_INSPECAO_VISUAL_INTERNA")
	private InspecaoVisualInterna inspecaoVisualInterna;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "aptoTransportarGrupos")
	@XmlElement(name = "MARCO_APTO_TRANSPORTAR_GRUPOS")
	private Grupos aptoTransportarGrupos;
	
	public Inspecao7i(int idInspecao, Head head,
			IdentificacaoEquipamento7i marcoIdentificacaoEquipamento7i,
			EnsaioHidrostaticoPneumatico7i marcoEnsaioHidrostaticoPneumatico7i,
			Set<EnsaioValvulas7i> ensaioValvulas7i,
			InspecaoVisualExterna inspecaoVisualExterna,
			InspecaoVisualInterna inspecaoVisualInterna,
			Grupos aptoTransportarGrupos) {
		this.id = idInspecao;
		this.head = head;
		this.marcoIdentificacaoEquipamento7i = marcoIdentificacaoEquipamento7i;
		this.marcoEnsaioHidrostaticoPneumatico7i = marcoEnsaioHidrostaticoPneumatico7i;
		this.ensaioValvulas7i = ensaioValvulas7i;
		this.inspecaoVisualExterna = inspecaoVisualExterna;
		this.inspecaoVisualInterna = inspecaoVisualInterna;
		this.aptoTransportarGrupos = aptoTransportarGrupos;
	}
	public Inspecao7i(Head head,
			IdentificacaoEquipamento7i marcoIdentificacaoEquipamento7i,
			EnsaioHidrostaticoPneumatico7i marcoEnsaioHidrostaticoPneumatico7i,
			Set<EnsaioValvulas7i> ensaioValvulas7i,
			InspecaoVisualExterna inspecaoVisualExterna,
			InspecaoVisualInterna inspecaoVisualInterna,
			Grupos aptoTransportarGrupos) {
		super();
		this.head = head;
		this.marcoIdentificacaoEquipamento7i = marcoIdentificacaoEquipamento7i;
		this.marcoEnsaioHidrostaticoPneumatico7i = marcoEnsaioHidrostaticoPneumatico7i;
		this.ensaioValvulas7i = ensaioValvulas7i;
		this.inspecaoVisualExterna = inspecaoVisualExterna;
		this.inspecaoVisualInterna = inspecaoVisualInterna;
		this.aptoTransportarGrupos = aptoTransportarGrupos;
	}

	public Inspecao7i() {
	}
	
	public int getId() {
		return id;
	}
	public void setId(int idInspecao) {
		this.id = idInspecao;
	}
	public Head getHead() {
		return head;
	}
	public void setHead(Head head) {
		this.head = head;
	}
	public IdentificacaoEquipamento7i getMarcoIdentificacaoEquipamento7i() {
		return marcoIdentificacaoEquipamento7i;
	}
	public void setMarcoIdentificacaoEquipamento7i(
			IdentificacaoEquipamento7i marcoIdentificacaoEquipamento7i) {
		this.marcoIdentificacaoEquipamento7i = marcoIdentificacaoEquipamento7i;
	}
	public EnsaioHidrostaticoPneumatico7i getMarcoEnsaioHidrostaticoPneumatico7i() {
		return marcoEnsaioHidrostaticoPneumatico7i;
	}
	public void setMarcoEnsaioHidrostaticoPneumatico7i(
			EnsaioHidrostaticoPneumatico7i marcoEnsaioHidrostaticoPneumatico7i) {
		this.marcoEnsaioHidrostaticoPneumatico7i = marcoEnsaioHidrostaticoPneumatico7i;
	}
	public Set<EnsaioValvulas7i> getEnsaioValvulas7i() {
		return ensaioValvulas7i;
	}
	public void setEnsaioValvulas7i(Set<EnsaioValvulas7i> ensaioValvulas7i) {
		this.ensaioValvulas7i = ensaioValvulas7i;
	}
	public InspecaoVisualExterna getInspecaoVisualExterna() {
		return inspecaoVisualExterna;
	}
	public void setInspecaoVisualExterna(InspecaoVisualExterna inspecaoVisualExterna) {
		this.inspecaoVisualExterna = inspecaoVisualExterna;
	}
	public InspecaoVisualInterna getInspecaoVisualInterna() {
		return inspecaoVisualInterna;
	}
	public void setInspecaoVisualInterna(InspecaoVisualInterna inspecaoVisualInterna) {
		this.inspecaoVisualInterna = inspecaoVisualInterna;
	}
	public Grupos getAptoTransportarGrupos() {
		return aptoTransportarGrupos;
	}
	public void setAptoTransportarGrupos(Grupos aptoTransportarGrupos) {
		this.aptoTransportarGrupos = aptoTransportarGrupos;
	}
	
	public ArrayList<String> getTodasEvidencias(){
		ArrayList<String> evidencias = new ArrayList<String>();
		evidencias.add(this.marcoIdentificacaoEquipamento7i.getRef_imagem_evidencia_panoramica());
		evidencias.add(this.marcoIdentificacaoEquipamento7i.getRef_imagem_evidencia_placa());
		evidencias.add(this.marcoEnsaioHidrostaticoPneumatico7i.getEvidencia1());
		evidencias.add(this.marcoEnsaioHidrostaticoPneumatico7i.getEvidencia2());
		evidencias.add(this.inspecaoVisualExterna.getFoto());
		evidencias.add(this.inspecaoVisualInterna.getFotoPanoramica());
		evidencias.add(this.inspecaoVisualInterna.getFotoPlaca());
		return evidencias;
	}
	
	public void setTodasEvidencias(ArrayList<String> evidencias){
		this.marcoIdentificacaoEquipamento7i.setRef_imagem_evidencia_panoramica(evidencias.get(0));
		this.marcoIdentificacaoEquipamento7i.setRef_imagem_evidencia_placa(evidencias.get(1));
		this.marcoEnsaioHidrostaticoPneumatico7i.setEvidencia1(evidencias.get(2));
		this.marcoEnsaioHidrostaticoPneumatico7i.setEvidencia2(evidencias.get(3));
		this.inspecaoVisualExterna.setFoto(evidencias.get(4));
		this.inspecaoVisualInterna.setFotoPanoramica(evidencias.get(5));
		this.inspecaoVisualInterna.setFotoPlaca(evidencias.get(6));
	}
}
