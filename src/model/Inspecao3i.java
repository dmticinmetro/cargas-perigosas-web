package model;

import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement(name = "INSPECAO")
@XmlAccessorType(XmlAccessType.FIELD)
public class Inspecao3i {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "head")
	@XmlElement(name = "HEAD")
	private Head head;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "identificacaoEquipamento")
	@XmlElement(name = "MARCO_IDENTIFICACAO_EQUIPAMENTO")
	private IdentificacaoEquipamento3i identificacaoEquipamento;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "medicaoVacuo")
	@XmlElement(name = "MARCO_MEDICAO_VACUO")
	private MedicaoVacuo3i medicaoVacuo;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ensaioHidrostatico")
	@XmlElement(name = "MARCO_ENSAIO_HIDROSTARICO")
	private EnsaioHidrostatico3i ensaioHidrostatico;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "aptoTransportarGrupos")
	@XmlElement(name = "MARCO_APTO_TRANSPORTAR_GRUPOS")
	private Grupos aptoTransportarGrupos;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "inspecaoVisualExterna")
	@XmlElement(name = "MARCO_INSPECAO_VISUAL_EXTERNA")
	private InspecaoVisualExterna inspecaoVisualExterna;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "inspecaoVisualInterna")
	@XmlElement(name = "MARCO_INSPECAO_VISUAL_INTERNA")
	private InspecaoVisualInterna inspecaoVisualInterna;

	public Inspecao3i(int idInspecao, Head head,
			IdentificacaoEquipamento3i identificacaoEquipamento,
			MedicaoVacuo3i medicaoVacuo,
			EnsaioHidrostatico3i ensaioHidrostatico,
			Grupos aptoTransportarGrupos,
			InspecaoVisualExterna inspecaoVisualExterna,
			InspecaoVisualInterna inspecaoVisualInterna) {
		this.id = idInspecao;
		this.head = head;
		this.identificacaoEquipamento = identificacaoEquipamento;
		this.medicaoVacuo = medicaoVacuo;
		this.ensaioHidrostatico = ensaioHidrostatico;
		this.aptoTransportarGrupos = aptoTransportarGrupos;
		this.inspecaoVisualExterna = inspecaoVisualExterna;
		this.inspecaoVisualInterna = inspecaoVisualInterna;
	}
	public Inspecao3i(Head head,
			IdentificacaoEquipamento3i identificacaoEquipamento,
			MedicaoVacuo3i medicaoVacuo,
			EnsaioHidrostatico3i ensaioHidrostatico,
			Grupos aptoTransportarGrupos,
			InspecaoVisualExterna inspecaoVisualExterna,
			InspecaoVisualInterna inspecaoVisualInterna) {
		this.head = head;
		this.identificacaoEquipamento = identificacaoEquipamento;
		this.medicaoVacuo = medicaoVacuo;
		this.ensaioHidrostatico = ensaioHidrostatico;
		this.aptoTransportarGrupos = aptoTransportarGrupos;
		this.inspecaoVisualExterna = inspecaoVisualExterna;
		this.inspecaoVisualInterna = inspecaoVisualInterna;
	}
	public Inspecao3i() {
	}

	public int getId() {
		return id;
	}
	public void setId(int idInspecao) {
		this.id = idInspecao;
	}
	public Head getHead() {
		return head;
	}
	public void setHead(Head head) {
		this.head = head;
	}
	public IdentificacaoEquipamento3i getIdentificacaoEquipamento() {
		return identificacaoEquipamento;
	}
	public void setIdentificacaoEquipamento(
			IdentificacaoEquipamento3i identificacaoEquipamento) {
		this.identificacaoEquipamento = identificacaoEquipamento;
	}
	public MedicaoVacuo3i getMedicaoVacuo() {
		return medicaoVacuo;
	}
	public void setMedicaoVacuo(MedicaoVacuo3i medicaoVacuo) {
		this.medicaoVacuo = medicaoVacuo;
	}
	public EnsaioHidrostatico3i getEnsaioHidrostatico() {
		return ensaioHidrostatico;
	}
	public void setEnsaioHidrostatico(EnsaioHidrostatico3i ensaioHidrostatico) {
		this.ensaioHidrostatico = ensaioHidrostatico;
	}
	public Grupos getAptoTransportarGrupos() {
		return aptoTransportarGrupos;
	}

	public void setAptoTransportarGrupos(Grupos aptoTransportarGrupos) {
		this.aptoTransportarGrupos = aptoTransportarGrupos;
	}
	public InspecaoVisualExterna getInspecaoVisualExterna() {
		return inspecaoVisualExterna;
	}
	public void setInspecaoVisualExterna(InspecaoVisualExterna inspecaoVisualExterna) {
		this.inspecaoVisualExterna = inspecaoVisualExterna;
	}
	public InspecaoVisualInterna getInspecaoVisualInterna() {
		return inspecaoVisualInterna;
	}
	public void setInspecaoVisualInterna(InspecaoVisualInterna inspecaoVisualInterna) {
		this.inspecaoVisualInterna = inspecaoVisualInterna;
	}
	
	public ArrayList<String> getTodasEvidencias(){
		ArrayList<String> evidencias = new ArrayList<String>();
		evidencias.add(this.identificacaoEquipamento.getFotoPanoramica());
		evidencias.add(this.identificacaoEquipamento.getFotoPlaca());
		evidencias.add(this.inspecaoVisualExterna.getFoto());
		evidencias.add(this.inspecaoVisualInterna.getFotoPanoramica());
		evidencias.add(this.inspecaoVisualInterna.getFotoPlaca());
		return evidencias;
	}
	
	public void setTodasEvidencias(ArrayList<String> evidencias){
		this.identificacaoEquipamento.setFotoPanoramica(evidencias.get(0));
		this.identificacaoEquipamento.setFotoPlaca(evidencias.get(1));
		this.inspecaoVisualExterna.setFoto(evidencias.get(2));
		this.inspecaoVisualInterna.setFotoPanoramica(evidencias.get(3));
		this.inspecaoVisualInterna.setFotoPlaca(evidencias.get(4));
	}
}
