package model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.hibernate.annotations.CollectionOfElements;

@Entity
@Table
@XmlType(name = "MARCO_APTO_TRANSPORTAR_GRUPOS")
@XmlAccessorType(XmlAccessType.FIELD)
public class Grupos {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
	
	@CollectionOfElements(fetch = FetchType.EAGER)
	@JoinTable(name = "grupo", joinColumns = @JoinColumn(name = "idGrupo"))
	@Column(name = "grupo")
	@XmlElement(name = "GRUPO")
	private Set<String> grupo;
	
	public Grupos(int idAptoTransportarGrupos, Set<String> grupo) {
		this.id = idAptoTransportarGrupos;
		this.grupo = grupo;
	}
	public Grupos(Set<String> grupo) {
		this.grupo = grupo;
	}
	public Grupos() {
	}
	
	public int getId() {
		return id;
	}
	public void setId(int idAptoTransportarGrupos) {
		this.id = idAptoTransportarGrupos;
	}
	public Set<String> getGrupo() {
		return grupo;
	}
	public void setGrupo(Set<String> grupos) {
		this.grupo = grupos;
	}
}
