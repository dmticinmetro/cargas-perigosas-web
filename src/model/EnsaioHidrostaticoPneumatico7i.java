package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table
@XmlType(name = "MARCO_ENSAIO_HIDROSTATICO_PNEUMATICO")
@XmlAccessorType(XmlAccessType.FIELD)
public class EnsaioHidrostaticoPneumatico7i {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
	
	@Column
	@XmlElement(name = "PRESSAO_ENSAIO")
	private double pressaoEnsaio;
	
	@Column
	@XmlElement(name = "ENSAIO_EXECUTADO")
	private String ensaioExecutado;
	
	@Column
	@XmlElement(name = "RESULTADO_ENSAIO")
	private String resultadoEnsaio;
	
	@Column
	@XmlElement(name = "TEMPO_DURACAO")
	private String tempoDuracao;
	
	@Column
    @XmlElement(name = "IMG_ENSAIO_HID_PNE_INICIAL")
    private String evidencia1;
    
	@Column
    @XmlElement(name = "IMG_ENSAIO_HID_PNE_FINAL")
    private String evidencia2;
    
    
	public EnsaioHidrostaticoPneumatico7i(int idEnsaioHidPen,
			double pressaoEnsaio, String ensaioExecutado,
			String resultadoEnsaio, String tempoDuracao, String evidencia1,
			String evidencia2) {
		this.id = idEnsaioHidPen;
		this.pressaoEnsaio = pressaoEnsaio;
		this.ensaioExecutado = ensaioExecutado;
		this.resultadoEnsaio = resultadoEnsaio;
		this.tempoDuracao = tempoDuracao;
		this.evidencia1 = evidencia1;
		this.evidencia2 = evidencia2;
	}
	public EnsaioHidrostaticoPneumatico7i(double pressaoEnsaio,
			String ensaioExecutado, String resultadoEnsaio,
			String tempoDuracao, String evidencia1, String evidencia2) {
		this.pressaoEnsaio = pressaoEnsaio;
		this.ensaioExecutado = ensaioExecutado;
		this.resultadoEnsaio = resultadoEnsaio;
		this.tempoDuracao = tempoDuracao;
		this.evidencia1 = evidencia1;
		this.evidencia2 = evidencia2;
	}
	public EnsaioHidrostaticoPneumatico7i() {
	}
	
	public int getId() {
		return id;
	}
	public void setId(int idEnsaioHidPen) {
		this.id = idEnsaioHidPen;
	}
	public double getPressaoEnsaio() {
		return pressaoEnsaio;
	}
	public void setPressaoEnsaio(double pressaoEnsaio) {
		this.pressaoEnsaio = pressaoEnsaio;
	}
	public String getEnsaioExecutado() {
		return ensaioExecutado;
	}
	public void setEnsaioExecutado(String ensaioExecutado) {
		this.ensaioExecutado = ensaioExecutado;
	}
	public String getResultadoEnsaio() {
		return resultadoEnsaio;
	}
	public void setResultadoEnsaio(String resultadoEnsaio) {
		this.resultadoEnsaio = resultadoEnsaio;
	}
	public String getTempoDuracao() {
		return tempoDuracao;
	}
	public void setTempoDuracao(String tempoDuracao) {
		this.tempoDuracao = tempoDuracao;
	}
	public String getEvidencia1() {
		return evidencia1;
	}
	public void setEvidencia1(String evidencia1) {
		this.evidencia1 = evidencia1;
	}
	public String getEvidencia2() {
		return evidencia2;
	}
	public void setEvidencia2(String evidencia2) {
		this.evidencia2 = evidencia2;
	}    
}
