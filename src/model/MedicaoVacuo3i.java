package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table
@XmlType(name = "MARCO_MEDICAO_VACUO")
@XmlAccessorType(XmlAccessType.FIELD)
public class MedicaoVacuo3i {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
	
	@Column
	@XmlElement(name = "MEDICAO_VACUO")
	private double medicaoVacuo;

	public MedicaoVacuo3i(int idMedicaoVacuo3i, double medicaoVacuo) {
		this.id = idMedicaoVacuo3i;
		this.medicaoVacuo = medicaoVacuo;
	}
	public MedicaoVacuo3i(double medicaoVacuo) {
		this.medicaoVacuo = medicaoVacuo;
	}
	public MedicaoVacuo3i() {
	}

	public int getId() {
		return id;
	}
	public void setId(int idMedicaoVacuo3i) {
		this.id = idMedicaoVacuo3i;
	}
	public double getMedicaoVacuo() {
		return medicaoVacuo;
	}
	public void setMedicaoVacuo(double medicaoVacuo) {
		this.medicaoVacuo = medicaoVacuo;
	}
}
