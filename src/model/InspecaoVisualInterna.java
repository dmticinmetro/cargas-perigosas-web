package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Entity
@Table
@XmlAccessorType(XmlAccessType.FIELD)
public class InspecaoVisualInterna {
	
	@Id
	@GeneratedValue
	@Column(unique = true)
	private int id;
	
	@Column
	@XmlElement(name = "IMG_INSPECAO_VISUAL_INTERNA_INICIAL")
	private String fotoPanoramica;

	@Column
	@XmlElement(name = "IMG_INSPECAO_VISUAL_INTERNA_FINAL")
	private String fotoPlaca;
	
	public InspecaoVisualInterna(int idInspecaoVisualInterna,
			String fotoPanoramica, String fotoPlaca) {
		this.id = idInspecaoVisualInterna;
		this.fotoPanoramica = fotoPanoramica;
		this.fotoPlaca = fotoPlaca;
	}
	public InspecaoVisualInterna(String fotoPanoramica, String fotoPlaca) {
		this.fotoPanoramica = fotoPanoramica;
		this.fotoPlaca = fotoPlaca;
	}
	public InspecaoVisualInterna() {
	}
	
	public int getId() {
		return id;
	}
	public void setId(int idInspecaoVisualInterna) {
		this.id = idInspecaoVisualInterna;
	}
	public String getFotoPanoramica() {
		return fotoPanoramica;
	}
	public void setFotoPanoramica(String fotoPanoramica) {
		this.fotoPanoramica = fotoPanoramica;
	}
	public String getFotoPlaca() {
		return fotoPlaca;
	}
	public void setFotoPlaca(String fotoPlaca) {
		this.fotoPlaca = fotoPlaca;
	}
}
