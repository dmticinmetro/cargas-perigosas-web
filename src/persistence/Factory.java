package persistence;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class Factory {
	private static SessionFactory factory;
	
	private Factory(){
		factory = new AnnotationConfiguration().addAnnotatedClass(model.EnsaioHidrostatico3i.class)
											   .addAnnotatedClass(model.EnsaioHidrostatico6i.class)
											   .addAnnotatedClass(model.EnsaioHidrostaticoPneumatico7i.class)
											   .addAnnotatedClass(model.EnsaioNaoDestrutivo6i.class)
											   .addAnnotatedClass(model.EnsaioValvulas7i.class)
											   .addAnnotatedClass(model.Entidade.class)
											   .addAnnotatedClass(model.EspessurasMinimas1i.class)
											   .addAnnotatedClass(model.Grupos.class)
											   .addAnnotatedClass(model.Head.class)
											   .addAnnotatedClass(model.IdentificacaoEquipamento1i.class)
											   .addAnnotatedClass(model.IdentificacaoEquipamento3i.class)
											   .addAnnotatedClass(model.IdentificacaoEquipamento6i.class)
											   .addAnnotatedClass(model.IdentificacaoEquipamento7i.class)
											   .addAnnotatedClass(model.Inspecao1i.class)
											   .addAnnotatedClass(model.Inspecao3i.class)
											   .addAnnotatedClass(model.Inspecao6i.class)
											   .addAnnotatedClass(model.Inspecao7i.class)
											   .addAnnotatedClass(model.InspecaoVisualExterna.class)
											   .addAnnotatedClass(model.InspecaoVisualInterna.class)
											   .addAnnotatedClass(model.Login.class)
											   .addAnnotatedClass(model.MedicaoVacuo3i.class)
											   .addAnnotatedClass(model.RegistroInspecao.class)
											   .addAnnotatedClass(model.ValvulaAlivio1i.class)
											   .addAnnotatedClass(model.ValvulaAlivio7i.class)
											   .addAnnotatedClass(model.Valvulas6i.class)
											   .addAnnotatedClass(model.ValvulaVacuo7i.class)
											   .configure("persistence/hibernate.cfg.xml")
											   .buildSessionFactory();
	}
	
	public static SessionFactory getFactory(){
		if(factory == null){
			new Factory();
			return factory;
		}else{
			return factory;
		}
	}
}