package persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import model.Head;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import control.ErrorControl;

public class DAO<T> {
	private SessionFactory factory;
	private Class<T> c;
	Session session;
	
	public DAO(Class<T> c){
		this.c = c;
		factory = Factory.getFactory();
		this.session = factory.openSession();
	}	
	public DAO(){
	}
	
	public void cadastrar(Object object) throws Exception{
		Transaction transacao = session.beginTransaction();
		session.save(object);
		transacao.commit();
		session.flush();
		session.close();
	}
	
	public void alterar(Object object) throws Exception{
		Transaction transacao = session.beginTransaction();
		session.update(object);
		transacao.commit();
		session.flush();
		session.close();
	}
	
	public void excluir(Object object) throws Exception{
		Transaction transacao = session.beginTransaction();
		session.delete(object);
		transacao.commit();
		session.flush();
		session.close();
	}
	
	@SuppressWarnings("unchecked")
	public T consultar(String campo, Object object) throws Exception{
		Criteria cr = session.createCriteria(c).add(Restrictions.eq(campo, object));
		T aux = (T) cr.uniqueResult();
		session.flush();
		session.close();
		return aux;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<T> consultarTodos(String tabela, String campo, Date inicio, Date fim) throws Exception{
		Query query = session.createQuery("FROM " + tabela + " WHERE " + campo + " BETWEEN :inicio AND :fim");    
	    query.setParameter("inicio", inicio);
	    query.setParameter("fim", fim);
	    ArrayList<T> result = (ArrayList<T>) query.list();
	    return result;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<T> consultarTodos(String campo, Object object) throws Exception{
		Criteria cr = session.createCriteria(c).add(Restrictions.eq(campo, object));
		ArrayList<T> lista = (ArrayList<T>)cr.list();
		session.flush();
		session.close();
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<T> consultarTodos(String tabela) throws Exception{
		Query q = session.createQuery("FROM modelo." + tabela);
		ArrayList<T> lista = (ArrayList<T>)q.list();
		session.flush();
		session.close();
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<T> consultarTodos() throws Exception{
		Criteria criteria = session.createCriteria(c);
		ArrayList<T> list = (ArrayList<T>) criteria.list();
		session.flush();
		session.close();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<T> consultarTodosGroupBy(String group) throws Exception{
		Criteria criteria = session.createCriteria(c).setProjection(Projections.groupProperty(group));
		ArrayList<T> list = (ArrayList<T>) criteria.list();
		session.flush();
		session.close();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<T> consultarTodosOrderBy(String order) throws Exception{
		Criteria criteria = session.createCriteria(c).addOrder(Order.asc(order));
		ArrayList<T> list = (ArrayList<T>) criteria.list();
		session.flush();
		session.close();
		return list;
	}
		
	@SuppressWarnings("unchecked")
	public ArrayList<T> consultarTodosGroupOrderBy(String group, String order) throws Exception{
		Criteria criteria = session.createCriteria(c).setProjection(Projections.groupProperty(group)).addOrder(Order.asc(order));
		ArrayList<T> list = (ArrayList<T>) criteria.list();
		session.flush();
		session.close();
		return list;
	}
	
	public int queryCountDistinctSQL(String column, String table){
		Object result = session.createSQLQuery("SELECT COUNT(DISTINCT " + column + ") FROM " + table).uniqueResult();
		session.flush();
		session.close();
		return Integer.valueOf(String.valueOf(result));
	}
	
	public int queryCountSQL(String column, String table){
		Object result = session.createSQLQuery("SELECT COUNT(" + column + ") FROM " + table).uniqueResult();
		session.flush();
		session.close();
		return Integer.valueOf(String.valueOf(result));
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<T> querySQLPuro(String query) throws SQLException{
		Connection connection = ConnectionDB.openConnection();
		PreparedStatement statement = connection.prepareStatement(query);
		ResultSet resultSet = statement.executeQuery();
		
		ArrayList<T> result = new ArrayList<T>();
		while(resultSet.next()){
			result.add((T) resultSet);
		}
		
		statement.close();
		connection.close();		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<T> querySQL(String query){
		ArrayList<T> result = (ArrayList<T>) session.createSQLQuery(query).list();
		session.flush();
		session.close();
		return result;
	}
	
	public static void main(String[] args) {
		try{
			ArrayList<Head> result = new DAO<Head>(Head.class).querySQLPuro("SELECT cipp FROM head");
			
			System.out.println("Quant.: " + result.size());
			
			for(Head o : result){
				System.out.println(o);
			}
			
		}catch (Exception e) {
			new ErrorControl(e);
		}
	}
}