package persistence;

import java.sql.*;

public class ConnectionDB {
	public static String status = "N�o conectou...";
	 
	public ConnectionDB(){
	}
	
	public static Connection openConnection() {
		try {                        
			Class.forName("com.mysql.jdbc.Driver");          
            Connection connection = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/cargasperigosas", "root", "1234"); 
            if (connection != null) {
            	status = ("STATUS--->Conectado com sucesso!");
            } else {
            	status = ("STATUS--->N�o foi possivel realizar conex�o");
            }
            return connection;
		} catch (ClassNotFoundException e) {
			System.out.println("O driver expecificado nao foi encontrado.");
            return null;
        } catch (SQLException e) {
        	System.out.println("Nao foi possivel conectar ao Banco de Dados.");
            return null;
        }
    }

    public static String statusConnection() {
    	return status;
    }
    
    public static boolean closeConnection() {
    	try {
    		ConnectionDB.openConnection().close();
    		return true;
    	} catch (SQLException e) {
    		return false;
        }
    }
    
    public static Connection restartConnection() {
    	closeConnection();
        return ConnectionDB.openConnection();
    }
}
