package security;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import control.ErrorControl;

public class CryptUtil {
	public static String generateMD5(String file){
		try{
			MessageDigest digest = MessageDigest.getInstance("MD5");      
			BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(file));      
			StringBuilder hash = new StringBuilder();
			int b = 0;
			while ((b = inputStream.read()) != -1) {    
				digest.update((byte) b);
			}
			inputStream.close();       
			byte[] byteDigest = digest.digest();  
	  		for(int i = 0; i < byteDigest.length; i++){      
		     	if ((0xff & byteDigest[i]) < 0x10){     
		        	hash.append("0" + Integer.toHexString((0xFF & byteDigest[i])));  
		     	}else{     
		        	hash.append(Integer.toHexString(0xFF & byteDigest[i]));  
		     	}
			}
	  		return hash.toString();
		}catch (NoSuchAlgorithmException e) {
			new ErrorControl("Ocorreu um erro com o algoritmo do hash.", e);
			return null;
		}catch (IOException e) {
			new ErrorControl(e);
			return null;
		}catch (Exception e) {
			new ErrorControl(e);
			return null;
		}
	}
	
/* 
 * ==================================================    RSA Inspe��o    ====================================================================================================
 */
	public static String encryptInspecao(String text) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
    	BigInteger m = new BigInteger(text, 16);
    	BigInteger n = new BigInteger("00000001df50344d5fa162539df65d7524d2409649308738bad26e0b4b0b3164497a6610c0905cbd4d32901608b75d62f28f4e9dcd9fd0870039edeb5dad391e0b5ea5163f110830c8d37206d5c36a5e48122ac93ee1a8a66ae07d97cf1b2e10575609e3e4a3d1b28510637371b455f67090390763268c030e3ba34223a6530e52e8cb59", 16);
    	BigInteger e = new BigInteger("00000003", 16);
    	
    	BigInteger result = m.modPow(e, n);
    	
    	return result.toString(16);
    }
    
    public static String decryptInspecao(String text) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
    	BigInteger m = new BigInteger(text, 16);
    	BigInteger n = new BigInteger("00000001df50344d5fa162539df65d7524d2409649308738bad26e0b4b0b3164497a6610c0905cbd4d32901608b75d62f28f4e9dcd9fd0870039edeb5dad391e0b5ea5163f110830c8d37206d5c36a5e48122ac93ee1a8a66ae07d97cf1b2e10575609e3e4a3d1b28510637371b455f67090390763268c030e3ba34223a6530e52e8cb59", 16);
    	BigInteger d = new BigInteger("000000013f8acd88ea6b96e2694ee8f8c336d5b986205a25d1e19eb232077642dba6eeb5d5b59328de21b56405cf93974c5f89be891535af557bf3f23e737b695ce9c36237425a34d12de5d706df5dcfe791d65de51175416d78f81c73c48f7ae9b5873b84225aefd5f2fc899ab40f2f6af44ced96e5df70591b074fc5637c1573120423", 16);
    	
    	BigInteger result = m.modPow(d, n);
    	
    	return result.toString(16);
    }
    
/* 
 * ==================================================    RSA Recibo    ====================================================================================================
 */
    public static String encryptRecibo(String text) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
    	BigInteger m = new BigInteger(text, 16);
    	BigInteger n = new BigInteger("00000001df50344d5fa162539df65d7524d2409649308738bad26e0b4b0b3164497a6610c0905cbd4d32901608b75d62f28f4e9dcd9fd0870039edeb5dad391e0b5ea5163f110830c8d37206d5c36a5e48122ac93ee1a8a66ae07d97cf1b2e10575609e3e4a3d1b28510637371b455f67090390763268c030e3ba34223a6530e52e8cb59", 16);
    	BigInteger e = new BigInteger("00000003", 16);
    	
    	BigInteger result = m.modPow(e, n);
    	
    	return result.toString(16);
    }
    
    public static String decryptRecibo(String text) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
    	BigInteger m = new BigInteger(text, 16);
    	BigInteger n = new BigInteger("00000001df50344d5fa162539df65d7524d2409649308738bad26e0b4b0b3164497a6610c0905cbd4d32901608b75d62f28f4e9dcd9fd0870039edeb5dad391e0b5ea5163f110830c8d37206d5c36a5e48122ac93ee1a8a66ae07d97cf1b2e10575609e3e4a3d1b28510637371b455f67090390763268c030e3ba34223a6530e52e8cb59", 16);
    	BigInteger d = new BigInteger("000000013f8acd88ea6b96e2694ee8f8c336d5b986205a25d1e19eb232077642dba6eeb5d5b59328de21b56405cf93974c5f89be891535af557bf3f23e737b695ce9c36237425a34d12de5d706df5dcfe791d65de51175416d78f81c73c48f7ae9b5873b84225aefd5f2fc899ab40f2f6af44ced96e5df70591b074fc5637c1573120423", 16);
    	
    	BigInteger result = m.modPow(d, n);
    	
    	return result.toString(16);
    }
    
/* 
 * ==================================================    RSA Foto    ====================================================================================================
 */
    /* 
     * ====================    ERRO NA CHAVE    ====================
    public static String encryptFoto(String text) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
    	BigInteger m = new BigInteger(text, 16);
    	BigInteger n = new BigInteger("00cadb0dc00ba9d1cc73ba0c5d200c23a9005c7dcf6429c0cf76fc44edf042dd77d3e7b8ccdb1a8523d2c4bfa9f61d1af1b77d59f58910b8fdc55803f070c12c3f", 16);
    	BigInteger e = new BigInteger("65537", 16);
    	
    	BigInteger result = m.modPow(e, n);
    	
    	return result.toString(16);
    }
    */
    
    public static String decryptFoto(String text) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
    	BigInteger m = new BigInteger(text, 16);
    	BigInteger n = new BigInteger("00cadb0dc00ba9d1cc73ba0c5d200c23a9005c7dcf6429c0cf76fc44edf042dd77d3e7b8ccdb1a8523d2c4bfa9f61d1af1b77d59f58910b8fdc55803f070c12c3f", 16);
    	BigInteger d = new BigInteger("0081b1433d4137b5766d96c948d0b0722c1e9b03a49079515852ce3e9391446eaa86534d349b5a6bd1639dd23e7feb51b23850ba0ee1e2826567875794669ef341", 16);
    	
    	BigInteger result = m.modPow(d, n);
    	
    	return result.toString(16);
    }
	
    
    
	@SuppressWarnings("static-access")
	public static void main(String[] args) {
		try {
			/*
			String sign = new CryptUtil().encryptInspecao("86ac45ea7e4a9a67a50d9e7ef54d850b");
			System.out.println("Sign: " + sign);
			
			String hash = new CryptUtil().decryptInspecao("25452861a3d66be5f6490bcba71913acca1402bb72ec9faab357badb1d2f572b504889264581c1a57b9bceeefc249c33");
			System.out.println("Hash: " + hash);
			*/
		    
			String recibo = new CryptUtil().encryptRecibo("2");
			System.out.println("Recibo Encrypt: " + recibo);
			System.out.println("Recibo Decrypt: " + new CryptUtil().decryptRecibo(recibo));
			
			System.out.println("");
			
			System.out.println("Inspecao Decrypt: " + new CryptUtil().encryptInspecao("d1d9b23f82d4d1a5f16165e80a9431a8"));
			System.out.println("Inspecao Decrypt: " + new CryptUtil().decryptInspecao("8d0282dbd2f1aa943374e3d131a33c0ae7ff397fef774223d08edc870c3daeedc00a2e8db7833e5db63a4c2fbe8f1a00"));
			
			/*
			System.out.println("\n====================================================================================================\n");
			
			String hashFoto = generateMD5("C:\\CargasPerigosasWeb\\Temp\\23425\\86ac45ea7e4a9a67a50d9e7ef54d850b\\70059acd0c50e3550e6f07f58a986393297124a111722829c799c9678a9f5d01619bf2d5d70c2a98e60dd0caf75afbe1a96d40bdbdfdda070530aafa9c6b0899.jpg");
			System.out.println("Hash Foto: " + hashFoto);
						
			String hashDecrypt = new CryptUtil().decryptFoto("70059acd0c50e3550e6f07f58a986393297124a111722829c799c9678a9f5d01619bf2d5d70c2a98e60dd0caf75afbe1a96d40bdbdfdda070530aafa9c6b0899");
			System.out.println("Hash Decrypt Foto: " + hashDecrypt);
			*/
			
		} catch (Exception e) {
			e.getMessage();
			e.getCause();
			e.printStackTrace();
		}
	}
}