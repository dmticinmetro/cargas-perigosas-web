package security;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyStore;
import java.security.Provider;
import java.util.StringTokenizer;

import javax.net.ServerSocketFactory;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLSessionContext;

public class SSLServer {
	String keystore = "kservidor";
	char keystorepass[];
	char keypassword[] ;
	public static final int HTTPS_PORT = 443;
	boolean autCliente; 
	String nome;
	ObjectOutputStream out;
	ObjectInputStream in;
	
	public SSLServer(String nome, boolean autCliente, String password){
		this.nome = nome;
		this.autCliente = autCliente;
		keystorepass = password.toCharArray();
		keypassword = password.toCharArray();
	}
	 
	public ServerSocket criaSSLServerSocket() throws Exception{  
		KeyStore ks = SSLUtil.getKeyStore("JKS");
		ks.load(new FileInputStream(keystore), keystorepass);
	  
		KeyManagerFactory kmf = SSLUtil.getKMFactory("SunX509");
		kmf.init(ks, keypassword);
	 
		SSLContext contextoSSL = SSLUtil.criaSSLContext("SSLv3");
		contextoSSL.init(kmf.getKeyManagers(), null, null);
	  
		showPropSSLContext(contextoSSL);
	  
		ServerSocketFactory ssf = contextoSSL.getServerSocketFactory();
		SSLServerSocket servidorSSL = (SSLServerSocket) ssf.createServerSocket(HTTPS_PORT); 
		//Se necess�rio, autentica o cliente
		if (autCliente){
			servidorSSL.setNeedClientAuth(autCliente);
		}  
		return servidorSSL;
	}

	@SuppressWarnings("unused")
	public void run(){
		ServerSocket listen;
		try{
			//vai criar um SSLServerSocket
			listen = criaSSLServerSocket();
			System.out.println(this.nome + " executando na porta " + HTTPS_PORT);
			System.out.println("Aguardando conexao...");
			//espera por uma conex�o do cliente
			Socket cliente = listen.accept();
			Conexao con = new Conexao(cliente);
		}catch(Exception e){
			System.out.println("Exception "+e.getMessage());
			e.printStackTrace();
		}
	}
	 
	 
	@SuppressWarnings("unused")
	private void showPropSSLContext(SSLContext contextoSSL){ 
		System.out.println("-------Informa�oes de contexto SSL-------");
		String protocol = contextoSSL.getProtocol();
		System.out.println("Protocolo : " + protocol);

		Provider provider = contextoSSL.getProvider();
	  	System.out.println("Nome do provedor : " + provider.getName());
	  	System.out.println("Versao do provedor : " + provider.getVersion());
	  	SSLSessionContext sslsessioncontext = contextoSSL.getServerSessionContext();
	}
	
	
	
	/*
	 * FUNCTION TESTE
	 */
	public static void main(String[] args) throws Exception{
		System.out.print("Informe o password para o keystore do servidor: ");
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String password = in.readLine();
		SSLServer servidor = new SSLServer("Servidor HTTPs", false, password);
		servidor.run();
	}
	 
	//classe interna
	class Conexao extends Thread {
		Socket cliente;
		BufferedReader in;
		DataOutputStream out;
	 
		public Conexao(Socket s) {
			cliente = s;
			try {
				in = new BufferedReader(new InputStreamReader (cliente.getInputStream()));
				out = new DataOutputStream(cliente.getOutputStream());
			}catch (IOException e) {
				System.out.println("Excecao lancada: "+e.getMessage());
			}
			this.start(); // chama o m�todo run
		}
	   
		public void run(){
			try {
				String request = in.readLine();
				System.out.println("Request: " + request);
				StringTokenizer st = new StringTokenizer(request);
				if ((st.countTokens() >= 2) && st.nextToken().equals("GET")) {
					if ((request = st.nextToken()).startsWith("/"))
						request = request.substring( 1 );
					if (request.equals(""))
						request = request + "index.html";
					File arq = new File(request);
					leDocumento(out, arq);
				}
				else{
					out.writeBytes("Erro 400: arquivo nao encontrado.");
				}
				cliente.close();
			}catch (Exception e) {
				System.out.println("Excecao lancada: " + e.getMessage());
			}
		}
	          
		// L� o arquivo e o envia para o cliente
		public void leDocumento(DataOutputStream out, File arq) throws Exception {
			try {
				DataInputStream in = new DataInputStream(new FileInputStream(arq));
				int tam = (int) arq.length();
				byte[] buffer = new byte[tam];
				in.readFully(buffer);
				in.close();
				out.writeBytes("HTTP/1.0 200 OK\r\n");
				out.writeBytes("Tamanho do conte�do: " + tam + "\r\n");
				out.writeBytes("Tipo do conte�do: text/html\r\n\r\n");
				out.write(buffer);
				out.flush();
			}catch (Exception e) {
				out.writeBytes("<html><head><title>Erro</title></head><body>\r\n\r\n");
				out.writeBytes("HTTP/1.0 400 " + e.getMessage() + "\r\n");
				out.writeBytes("Tipo do conte�do: text/html\r\n\r\n");
				out.writeBytes("</body></html>");
				out.flush();
			}finally {
				out.close();
			}
		}
	}
}
