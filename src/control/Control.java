package control;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import action.AbstractAction;
import action.Login;
import action.Logoff;
import action.RefinoLista;
import action.RelatorioSql;

@SuppressWarnings("serial")
public class Control extends HttpServlet{
	private Hashtable<String, AbstractAction> actions = new Hashtable<String, AbstractAction>();
	
	public void init(ServletConfig config) throws ServletException {
		actions.put("login", new Login());
		actions.put("logoff", new Logoff());
		actions.put("refinarListaInspecoes", new RefinoLista());
		actions.put("relatorio_sql", new RelatorioSql());
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		AbstractAction ac = actions.get(action);
		ac.execute(request, response);
	}
}
