package control;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Inspecao1i;
import model.Inspecao3i;
import model.Inspecao6i;
import model.Inspecao7i;
import model.RegistroInspecao;
import persistence.DAO;
import security.CryptUtil;
import util.DirectoryUtil;
import util.FileUtil;
import util.ZipIO;

public class UploadControl extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public UploadControl() {
        super();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	@SuppressWarnings("static-access")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
/* 
 * ==================================================    DEFINE DIRETORIOS    ====================================================================================================
 */
		
		String tempFilesPath = "C:\\CargasPerigosasWeb\\Inspecoes\\";
        String repositoryPath = "C:\\CargasPerigosasWeb\\InspecoesValidadas\\";
        String errorPath = "C:\\CargasPerigosasWeb\\InspecoesNaoValidadas\\";
        String tempPath = "C:\\CargasPerigosasWeb\\Temp\\";
		
        
		/*
		String tempFilesPath = "/home/ditel/CargasPerigosasWeb/Inspecoes/";
        String repositoryPath = "/home/ditel/CargasPerigosasWeb/InspecoesValidadas/";
        String errorPath = "/home/ditel/CargasPerigosasWeb/InspecoesNaoValidadas/";
        String tempPath = "/home/ditel/CargasPerigosasWeb/Temp/";
        */
		
        /*
        String tempFilesPath = "/home/vagner/CargasPerigosasWeb/Inspecoes/";
        String repositoryPath = "/home/vagner/CargasPerigosasWeb/InspecoesValidadas/";
        String errorPath = "/home/vagner/CargasPerigosasWeb/InspecoesNaoValidadas/";
        String tempPath = "/home/vagner/CargasPerigosasWeb/Temp/";
        */
        
/* 
 * ==================================================    CRIA OS DIRETORIOS    ====================================================================================================
 */
        new DirectoryUtil().createDirectory(tempFilesPath, repositoryPath, errorPath, tempPath);
        
/* 
 * ==================================================    RECUPERA O ARQUIVO ENVIADO PELO CLIENTE    ====================================================================================================
 */
        String cipp = "";
		String msg = "";
		String hash = "";
		String sign = "";
		String filename = "";
		
        try{
			ServletInputStream in = request.getInputStream();  
			byte[] line = new byte[128];
			int i = in.readLine(line, 0, 128);
			int boundaryLength = i - 2;
			String boundary = new String(line, 0, boundaryLength);
			while (i != -1) {
				String newLine = new String(line, 0, i);
				if (newLine.startsWith("Content-Disposition: form-data; name=\"")) {
					String s = new String(line, 0, i-2);
					int pos = s.indexOf("filename=\"");
					if (pos != -1) {
						String filepath = s.substring(pos+10, s.length()-1);
						pos = filepath.lastIndexOf("/");
						if (pos != -1){
							filename = filepath.substring(pos + 1);
						}else{
							filename = filepath;
						}
					}     
					i = in.readLine(line, 0, 128);
					i = in.readLine(line, 0, 128);
					i = in.readLine(line, 0, 128);
					ByteArrayOutputStream buffer = new  ByteArrayOutputStream();
					newLine = new String(line, 0, i);
					while (i != -1 && !newLine.startsWith(boundary)) {
						buffer.write(line, 0, i);
						i = in.readLine(line, 0, 128);
						newLine = new String(line, 0, i);
					}
					try {
						RandomAccessFile f = new RandomAccessFile(tempFilesPath + filename, "rw");
						byte[] bytes = buffer.toByteArray();
						f.write(bytes, 0, bytes.length - 2);
						f.close();
					}catch (Exception e) {
						System.out.println(e);
					}
				}
				i = in.readLine(line, 0, 128);
	        }
			cipp = filename.split("\\.")[0].replace("\\", "/");
			if(cipp.contains("/")){
				cipp = cipp.split("/")[cipp.split("/").length -1];
			}
		
// ======== RETIRA OS ARQUIVO DO ZIP, DIRECIONA PARA PASTA TEMP ==== //
			if(new File(filename).isFile()){
				ZipIO.unzip(new File(filename), new File(tempPath));
			}else if(new File(tempFilesPath + filename).isFile()){
				ZipIO.unzip(new File(tempFilesPath + filename), new File(tempPath));
			}else{
				msg = "Ocorreu um erro no upload do arquivo.";
			}
        }catch (Exception e) {
			new ErrorControl(e);
			msg = "Ocorreu um erro no upload do arquivo.";
		}
		
// ==== BUSCA O NOME DO ZIP E LE A ASSINATURA ==== //
        if(msg.isEmpty()){        	
			for (File f : new File(tempPath + cipp).listFiles()) {
				if(f.getName().contains(".zip")){
					hash = f.getName().split("\\.")[0];
				}
				if(f.getName().contains("sign")){
					try{
						sign = readSign(f.getAbsolutePath());					
					}catch (FileNotFoundException e) {
						new ErrorControl(e);
						msg = "<p>Ocorreu um erro na leitura do arquivo da assinatura.</p>";
					}catch (IOException e) {
						new ErrorControl(e);
						msg = "<p>Ocorreu um erro na leitura da dos dados do arquivo da assinatura.</p>";
					}catch (Exception e) {
						new ErrorControl(e);
						msg = "<p>Ocorreu um erro na leitura da assinatura.</p>";
					}
				}
			}
        }
		
// ==== VALIDA O HASH E A ASSINATURA ==== //
		if(!hash.isEmpty() && !sign.isEmpty() && msg.isEmpty()){
			boolean validaHash = false;
			boolean validaSign = false;
			validaHash = validaHash(tempPath + cipp + "/" + hash + ".zip", hash);
			try{
				if(validaHash){
					validaSign = validaSign(hash, sign);
				}else{
					validaSign = validaSign(tempPath + cipp + "/" + hash + ".zip", sign);
				}
				
				if(validaHash && validaSign){
					new ZipIO().unzip(new File(tempPath + cipp + "/" + hash + ".zip"), new File(tempPath));    // SE HASH E ASSINATURA FORAM VALIDOS, UNZIP A INSPECAO
				}else if(!validaHash){
					msg = "<p>Arquivo corrompido.</p>";
				}else if(!validaSign){
					msg = "<p>Assinatura inv�lida.</p>"; 
				}else if(!validaHash && !validaSign){
					msg = "<p>Arquivo corrompido.</p><p>Assinatura inv�lida.</p>";
				}				
			}catch (Exception e) {
				new ErrorControl(e);
				validaSign = false;
				msg = "<p>Ocorreu um erro na valida��o da assinatura.</p>";
			}
		}else{
			msg = "<p>Arquivo incompleto.</p>";			
		}
		
// ==== DELETA A PASTA DO PRIMEIRO ZIP ==== //
		new FileUtil().deleteDirectory(new File(tempPath + cipp));
	
		if(msg.isEmpty()){    // VERIFICANDO HASH E ASSINATURA
			String tipo = "";
			Object inspecao = new Object();
			
// ======== VERIFICA SE A INSPECAO NAO HAVIA SIDO ENVIADA ==== //		
			try{
				/*
				if(!new DAO<Inspecao1i>(Inspecao1i.class).querySQL("SELECT cipp FROM Head WHERE cipp=" + cipp).isEmpty()){
					msg = "<p>A inspe��o j� havia sido enviada.</p>";
				}
				*/
			}catch (Exception e) {
				new ErrorControl(e);
				msg = "<p>Ocorreu um erro com o banco de dados.</p>";
			}
			
			if(msg.isEmpty()){    // VERIFICANDO HASH, ASSINATURA E SE JA FOI ENVIADA
				
// ============ CARREGA O MODELO DA INSPECAO COM O XML| VERIFICA SE O NOME DO PRIMEIRO ZIP E O MESMO DA INSPECAO XML ==== //
				boolean arquivoInvalido = true;
				for(File file : new File(tempPath + hash).listFiles()){
					String f = file.getName();
					if(f.contains("_1i.xml")){
						Inspecao1i insp = (Inspecao1i) new util.XmlIO<Inspecao1i>(Inspecao1i.class).carregarXml(file);
						if(cipp.equals(String.valueOf(insp.getHead().getCipp()))){
							tipo = "1i";
							inspecao = insp;
							arquivoInvalido = false;
						}
					}else if(f.contains("_3i.xml")){
						Inspecao3i insp = (Inspecao3i) new util.XmlIO<Inspecao3i>(Inspecao3i.class).carregarXml(file);
						if(cipp.equals(String.valueOf(insp.getHead().getCipp()))){
							tipo = "3i";
							inspecao = insp;
							arquivoInvalido = false;
						}
					}else if(f.contains("_6i.xml")){
						Inspecao6i insp = (Inspecao6i) new util.XmlIO<Inspecao6i>(Inspecao6i.class).carregarXml(file);
						if(cipp.equals(String.valueOf(insp.getHead().getCipp()))){
							tipo = "6i";
							inspecao = insp;
							arquivoInvalido = false;
						}
					}else if(f.contains("_7i.xml")){
						Inspecao7i insp = (Inspecao7i) new util.XmlIO<Inspecao7i>(Inspecao7i.class).carregarXml(file);
						if(cipp.equals(String.valueOf(insp.getHead().getCipp()))){
							tipo = "7i";
							inspecao = insp;
							arquivoInvalido = false;
						}
					}	
				}
				if(arquivoInvalido){
					msg = "Arquivo foi alterado.";
				}
				
				
				if(msg.isEmpty()){    // VERIFICANDO HASH, ASSINATURA, SE JA FOI ENVIADA E XML VALIDO
					
// ================ CADASTRA A INSPECAO NO BANCO DE DADOS ==== //
					
					try{
						if(tipo.equals("1i")){
							Inspecao1i insp = (Inspecao1i) inspecao;
							new DAO<Inspecao1i>(Inspecao1i.class).cadastrar(insp);
							new DAO<RegistroInspecao>(RegistroInspecao.class).cadastrar(new RegistroInspecao(insp.getHead().getCipp(), insp.getHead().getOia(), insp.getHead().getTipoEnsaio()));
						}else if(tipo.equals("3i")){
							Inspecao3i insp = (Inspecao3i) inspecao;
							new DAO<Inspecao3i>(Inspecao3i.class).cadastrar((Inspecao3i) inspecao);
							new DAO<RegistroInspecao>(RegistroInspecao.class).cadastrar(new RegistroInspecao(insp.getHead().getCipp(), insp.getHead().getOia(), insp.getHead().getTipoEnsaio()));
						}else if(tipo.equals("6i")){
							Inspecao6i insp = (Inspecao6i) inspecao;
							new DAO<Inspecao6i>(Inspecao6i.class).cadastrar((Inspecao6i) inspecao);
							new DAO<RegistroInspecao>(RegistroInspecao.class).cadastrar(new RegistroInspecao(insp.getHead().getCipp(), insp.getHead().getOia(), insp.getHead().getTipoEnsaio()));
						}else if(tipo.equals("7i")){
							Inspecao7i insp = (Inspecao7i) inspecao;
							new DAO<Inspecao7i>(Inspecao7i.class).cadastrar((Inspecao7i) inspecao);
							new DAO<RegistroInspecao>(RegistroInspecao.class).cadastrar(new RegistroInspecao(insp.getHead().getCipp(), insp.getHead().getOia(), insp.getHead().getTipoEnsaio()));
						}
					}catch (Exception e) {
						new ErrorControl(e);
						msg = "<p>Ocorreu um erro na grava��o dos dados no banco de dados.</p>";
					}
					
				}
				
				if(msg.isEmpty()){    // VERIFICANDO HASH, ASSINATURA, SE JA FOI ENVIADA, XML VALIDO E SE FOI CADASTRADO
					
// ================ PASSA OS ARQUIVOS PARA A PASTA DAS INSPECOES VALIDA ==== //					
					for(File f : new File(tempPath + hash).listFiles()){
						new File(repositoryPath + cipp).mkdir();
						FileUtil.cutFile(f, new File(repositoryPath + cipp + "/" + f.getName()));
					}
					FileUtil.deleteDirectory(new File(tempPath + hash)); // DELETA A PASTA DO SEGUNDO ZIP
					
// ================ VALIDA AS FOTOS ==== //
					for(File file : new File(repositoryPath + cipp).listFiles()){
						String f = file.getName();
						if(f.contains(".jpg") || f.contains(".jpeg") || f.contains(".bmp") || f.contains(".png") || f.contains(".dib") ||
						           f.contains(".jpe") || f.contains(".jfif") || f.contains(".gif") || f.contains(".tif") || f.contains(".tiff") ||
						           f.contains(".JPG") || f.contains(".JPEG") || f.contains(".BMP") || f.contains(".PNG") || f.contains(".DIB") ||
						           f.contains(".JPE") || f.contains(".JFIF") || f.contains(".GIF") || f.contains(".TIF") || f.contains(".TIFF")){
							boolean fotoInvalida = false;
							try{
								if(f.split("\\.")[0].length() == 128){
									if(!validaFoto(file.getAbsolutePath(), f.split("\\.")[0])){
										fotoInvalida = true;
									}
								}else{
									fotoInvalida = true;
								}
							}catch (Exception e) {
								new ErrorControl(e);
								fotoInvalida = true;
							}
							if(fotoInvalida){
								File pathNaoValidadas = new File(repositoryPath + cipp + "/" + "FotosNaoValidadas");
								if(!pathNaoValidadas.exists()){
									pathNaoValidadas.mkdir();
								}
								FileUtil.cutFile(file, new File(pathNaoValidadas.getAbsolutePath() + "/" + f));
							}
						}
					}
					
// ================ AP�S TUDO VALIDADO, GERA O RECIBO E DIRECIONA PARA A JSP DA INSPECAO ENVIADA ==== //
					try{
						new File(tempFilesPath + cipp + ".zip").delete();
						
						String recibo = "";
						for(File f : new File(repositoryPath + cipp).listFiles()){
							if(f.getName().contains(".xml")){
								recibo = gerarRecibo(f.getAbsolutePath());
							}
						}
						request.setAttribute("msg", "<p>Inspe��o enviada com sucesso.</p><p>Recibo: " + recibo + "</p>");
						request.getRequestDispatcher("main/oia/inspecao_enviada.jsp").forward(request, response);
					}catch (Exception e) {
						new ErrorControl(e);
						request.setAttribute("msg", "<p>Ocorreu um erro na gera��o do recibo. Entre em contato com os administradores do sistema.</p>");
						request.getRequestDispatcher("main/oia/inspecao_enviada.jsp").forward(request, response);
					}
					
				}
			}
		}
		
// ==== SE ALGUMA VALIDACAO APRESENTAR ALGUM ERRO OU OCORRER ALGUM ERRO/EXCEPTION, RETORNAR A JSP DE ENVIO DE INSPECAO ==== //
		if(!msg.isEmpty()){			
			new File(errorPath + cipp).mkdir();
			if(new File(tempFilesPath + cipp + ".zip").exists()){
				FileUtil.cutFile(new File(tempFilesPath + cipp + ".zip"), new File(errorPath + cipp + "/" + cipp + ".zip"));
			}
			request.setAttribute("msg", "<p>O arquivo foi considerado inv�lido, pelo(s) seguinte(s) motivos(s):</p>" + msg);
			request.getRequestDispatcher("main/oia/enviar_inspecao.jsp").forward(request, response);
		}
		FileUtil.deleteDirectory(new File(tempPath + cipp));
		FileUtil.deleteDirectory(new File(tempPath + hash));
	}
	
/*
 * ==================================================    METODOS DE VALIDACAO    ====================================================================================================
 */
	private static String readSign(String file) throws FileNotFoundException, IOException{
		InputStream isSign = new FileInputStream(file);
		ByteArrayOutputStream bufferSign = new ByteArrayOutputStream();
		int nReadSign;
		byte[] dataSign = new byte[132];

		while ((nReadSign = isSign.read(dataSign, 0, dataSign.length)) != -1){
			bufferSign.write(dataSign, 0, nReadSign);
		}
		return bufferSign.toString();
	}
	
	private static boolean validaHash(String file, String hash){
		String hashCorreto = CryptUtil.generateMD5(file);		
		if(hashCorreto.equals(hash)){
			return true;
		}else{
			return false;
		}
	}
	
	private static boolean validaSign(String hash, String sign) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, IllegalBlockSizeException, BadPaddingException{
		String hashFile = "";
		if(new File(hash).isFile()){
			hashFile = CryptUtil.generateMD5(hash);
		}else{
			hashFile = hash;
		}
		if(hashFile.equals(CryptUtil.decryptInspecao(sign))){
			return true;
		}else{
			return false;
		}
	}
	
	private static String gerarRecibo(String file) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, IllegalBlockSizeException, BadPaddingException{
		String hash = CryptUtil.generateMD5(file);
		return CryptUtil.encryptRecibo(hash);
	}
	
	private static boolean validaFoto(String filePath, String sign) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, IllegalBlockSizeException, BadPaddingException{
		String hash = CryptUtil.generateMD5(filePath);
		if(hash.equals(CryptUtil.decryptFoto(sign))){
			return true;
		}else{
			return false;
		}
	}
}
