package control;

public class ErrorControl extends Exception{
	private static final long serialVersionUID = 1L;

	public ErrorControl(Exception e){
		this("Ocorreu um erro.", e);
	}
	
	public ErrorControl(String msg, Exception e) {
		this.mensagem(msg, e);
	}
	
	private void mensagem(String msg, Exception e){
		System.out.println("\nMensagem do desenvolvedor: " + msg);
		System.out.println("\nMensagem de erro do sistema: " + e.getMessage()+ ", " + e.getClass());
		System.out.println("\nCausa: " + e.getCause());
		System.out.println("\nPista: ");
		e.printStackTrace();
	}
}
