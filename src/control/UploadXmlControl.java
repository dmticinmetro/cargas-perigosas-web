package control;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Inspecao1i;
import model.Inspecao3i;
import model.Inspecao6i;
import model.Inspecao7i;
import model.RegistroInspecao;
import persistence.DAO;
import util.DirectoryUtil;
import util.XmlIO;

public class UploadXmlControl extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public UploadXmlControl() {
        super();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
        String repositoryPath = "C:\\CargasPerigosasWeb\\InspecoesValidadas\\";
		// String repositoryPath = "/home/ditel/CargasPerigosasWeb/InspecoesValidadas/";
        // String repositoryPath = "/home/vagner/CargasPerigosasWeb/InspecoesValidadas/";
        
        new DirectoryUtil().createDirectory(repositoryPath);
   
        String file = request.getParameter("file");
        String cipp = file.split("_")[1];
		String tipo = "";
		String msg = "";
		
		if(file.contains("1i")) tipo = "1i";
		else if (file.contains("3i")) tipo = "3i";
		else if (file.contains("6i")) tipo = "6i";
		else if (file.contains("7i")) tipo = "7i";
				
		if(msg.isEmpty()){			
			try{
				if(tipo.equals("1i")){
					Inspecao1i insp = (Inspecao1i) new XmlIO<Inspecao1i>(Inspecao1i.class).carregarXml(repositoryPath + cipp + "/" + file);
					new DAO<Inspecao1i>(Inspecao1i.class).cadastrar(insp);
					new DAO<RegistroInspecao>(RegistroInspecao.class).cadastrar(new RegistroInspecao(insp.getHead().getCipp(), insp.getHead().getOia(), insp.getHead().getTipoEnsaio()));
				}else if(tipo.equals("3i")){
					Inspecao3i insp = (Inspecao3i) new XmlIO<Inspecao3i>(Inspecao3i.class).carregarXml(repositoryPath + cipp + "/" + file);
					new DAO<Inspecao3i>(Inspecao3i.class).cadastrar(insp);
					new DAO<RegistroInspecao>(RegistroInspecao.class).cadastrar(new RegistroInspecao(insp.getHead().getCipp(), insp.getHead().getOia(), insp.getHead().getTipoEnsaio()));
				}else if(tipo.equals("6i")){
					Inspecao6i insp = (Inspecao6i) new XmlIO<Inspecao6i>(Inspecao6i.class).carregarXml(repositoryPath + cipp + "/" + file);
					new DAO<Inspecao6i>(Inspecao6i.class).cadastrar(insp);
					new DAO<RegistroInspecao>(RegistroInspecao.class).cadastrar(new RegistroInspecao(insp.getHead().getCipp(), insp.getHead().getOia(), insp.getHead().getTipoEnsaio()));
				}else if(tipo.equals("7i")){
					Inspecao7i insp = (Inspecao7i) new XmlIO<Inspecao7i>(Inspecao7i.class).carregarXml(repositoryPath + cipp + "/" + file);
					new DAO<Inspecao7i>(Inspecao7i.class).cadastrar(insp);
					new DAO<RegistroInspecao>(RegistroInspecao.class).cadastrar(new RegistroInspecao(insp.getHead().getCipp(), insp.getHead().getOia(), insp.getHead().getTipoEnsaio()));
				}
			}catch (Exception e) {
				new ErrorControl(e);
				msg = "<p>Ocorreu um erro na grava��o dos dados no banco de dados.</p>";
			}	
		}
		
		if(msg.isEmpty()){
			request.setAttribute("msg", "<p>Inspe��o enviada com sucesso.</p>");
			request.getRequestDispatcher("main/inmetro/inspecao_enviada.jsp").forward(request, response);
		}else {
			request.getRequestDispatcher("main/inmetro/enviar_inspecao.jsp").forward(request, response);
		}
	}
}
