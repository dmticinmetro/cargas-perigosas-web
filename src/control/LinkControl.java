package control;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Entidade;
import model.Head;
import model.Inspecao1i;
import model.Inspecao3i;
import model.Inspecao6i;
import model.Inspecao7i;
import persistence.DAO;
import util.XmlIO;

@SuppressWarnings("serial")
@WebServlet("/LinkControl")
public class LinkControl extends HttpServlet {
	private Hashtable<String, String> links = new Hashtable<String, String>();

	public void init() throws ServletException {
		links.put("enviar_inspecao", "/main/oia/enviar_inspecao.jsp");
		
		links.put("lista_inspecoes_oia", "/main/oia/inspecoes/lista_inspecaoes.jsp");
		links.put("visualizar_inspecao_1i_oia", "/main/oia/inspecoes/visualizar_inspecao_1i.jsp");
		links.put("visualizar_inspecao_3i_oia", "/main/oia/inspecoes/visualizar_inspecao_3i.jsp");
		links.put("visualizar_inspecao_6i_oia", "/main/oia/inspecoes/visualizar_inspecao_6i.jsp");
		links.put("visualizar_inspecao_7i_oia", "/main/oia/inspecoes/visualizar_inspecao_7i.jsp");
		
		links.put("lista_inspecoes_inmetro", "/main/inmetro/inspecoes/lista_inspecaoes.jsp");
		links.put("visualizar_inspecao_1i_inmetro", "/main/inmetro/inspecoes/visualizar_inspecao_1i.jsp");
		links.put("visualizar_inspecao_3i_inmetro", "/main/inmetro/inspecoes/visualizar_inspecao_3i.jsp");
		links.put("visualizar_inspecao_6i_inmetro", "/main/inmetro/inspecoes/visualizar_inspecao_6i.jsp");
		links.put("visualizar_inspecao_7i_inmetro", "/main/inmetro/inspecoes/visualizar_inspecao_7i.jsp");
		
		links.put("relatorios", "/main/inmetro/relatorios/relatorios.jsp");
		links.put("relatorio_contagem", "/main/inmetro/relatorios/relatorio_contagem.jsp");
		links.put("relatorio_sql", "/main/inmetro/relatorios/relatorio_sql.jsp");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			/*
			HttpSession session = request.getSession();
			Entidade entidade = (Entidade) session.getAttribute("login");
			*/
			
			String action = request.getParameter("action");
			
			
			if(action.equals("lista_inspecoes")){
				/*
				if(entidade.getTipo() == Tipo.INMETRO){
					lista_inspecoes_inmetro(request, response);
					action = "lista_inspecoes_inmetro";
				}
				else if(entidade.getTipo() == Tipo.OIA){
					lista_inspecoes_oia(request, response);
					action = "lista_inspecoes_oia";
				}
				*/
				action = "lista_inspecoes_inmetro";
				lista_inspecoes_inmetro(request, response);
			}
			else if(action.contains("visualizar_inspecao")){
				visualizar_inspecao(request, response);
			}
			
			request.getRequestDispatcher(links.get(action)).forward(request, response);
		}catch (Exception e) {
			new ErrorControl(e);
		}
	}

/*
 * ==================================================    METODOS AUXILIARES     ==================================================
 */
	private static void lista_inspecoes_inmetro(HttpServletRequest request, HttpServletResponse response){
		try{
			request.setAttribute("head", new DAO<Head>(Head.class).consultarTodosOrderBy("cipp"));
		}catch (Exception e) {
			new ErrorControl(e);
			
			ArrayList<Head> head = new ArrayList<Head>();
			
			File repositoryFiles = new File("C:\\CargasPerigosasWeb\\InspecoesValidadas\\");
			// File repositoryFiles = new File("/home/vagner/CargasPerigosasWeb/InspecoesValidadas/");
	        // File repositoryFiles = new File("/home/ditel/CargasPerigosasWeb/InspecoesValidadas/");
				        
	        for(File file : repositoryFiles.listFiles()){
	        	for(File f : file.listFiles()){	        		
		        	if(f.getName().contains("_1i.xml")){
		        		Inspecao1i i = (Inspecao1i) new XmlIO<Inspecao1i>(Inspecao1i.class).carregarXml(f);
		        		head.add(i.getHead());
		        	}
		        	if(f.getName().contains("_3i.xml")){		        		
		        		Inspecao3i i = (Inspecao3i) new XmlIO<Inspecao3i>(Inspecao3i.class).carregarXml(f);
		        		head.add(i.getHead());
		        	}
		        	if(f.getName().contains("_6i.xml")){
		        		Inspecao6i i = (Inspecao6i) new XmlIO<Inspecao6i>(Inspecao6i.class).carregarXml(f);
		        		head.add(i.getHead());
		        	}
		        	if(f.getName().contains("_7i.xml")){
		        		Inspecao7i i = (Inspecao7i) new XmlIO<Inspecao7i>(Inspecao7i.class).carregarXml(f);
		        		head.add(i.getHead());
		        	}
	        	}
	        }
	        	        
	        request.setAttribute("head", head);
		}
	}
	
	@SuppressWarnings("unused")
	private static void lista_inspecoes_oia(HttpServletRequest request, HttpServletResponse response){
		HttpSession session = request.getSession();
		Entidade entidade = (Entidade) session.getAttribute("login");
		try{
			request.setAttribute("head", new DAO<Head>(Head.class).consultarTodosOrderBy("cipp"));
		}catch (Exception e) {
			new ErrorControl(e);
		}
	}
	
	private static void visualizar_inspecao(HttpServletRequest request, HttpServletResponse response){		
		try{
			if(request.getParameter("action").contains("1i")){
				request.setAttribute("inspecao", new DAO<Inspecao1i>(Inspecao1i.class).consultar("head.id", Integer.parseInt(request.getParameter("head"))));
			}
			if(request.getParameter("action").contains("3i")){
				request.setAttribute("inspecao", new DAO<Inspecao3i>(Inspecao3i.class).consultar("head.id", Integer.parseInt(request.getParameter("head"))));
			}
			if(request.getParameter("action").contains("6i")){
				request.setAttribute("inspecao", new DAO<Inspecao6i>(Inspecao6i.class).consultar("head.id", Integer.parseInt(request.getParameter("head"))));
			}
			if(request.getParameter("action").contains("7i")){
				request.setAttribute("inspecao", new DAO<Inspecao7i>(Inspecao7i.class).consultar("head.id", Integer.parseInt(request.getParameter("head"))));
			}
		}catch (Exception e) {
			new ErrorControl(e);
			
			File pathFile = new File("C:\\CargasPerigosasWeb\\InspecoesValidadas\\" + request.getParameter("cipp"));
			// File pathFile = new File("/home/ditel/CargasPerigosasWeb/InspecoesValidadas/" + request.getParameter("cipp"));
			// File pathFile = new File("/home/vagner/CargasPerigosasWeb/InspecoesValidadas/" + request.getParameter("cipp"));
			String file = "";
			
			for(File f : pathFile.listFiles()){
				if(f.getName().contains(".xml")){
					file = f.getAbsolutePath();
				}
			}
			
			if(request.getParameter("action").contains("1i")){
				request.setAttribute("inspecao", new XmlIO<Inspecao1i>(Inspecao1i.class).carregarXml(file));
			}
			if(request.getParameter("action").contains("3i")){
				request.setAttribute("inspecao", new XmlIO<Inspecao3i>(Inspecao3i.class).carregarXml(file));
			}
			if(request.getParameter("action").contains("6i")){
				request.setAttribute("inspecao", new XmlIO<Inspecao6i>(Inspecao6i.class).carregarXml(file));
			}
			if(request.getParameter("action").contains("7i")){
				request.setAttribute("inspecao", new XmlIO<Inspecao7i>(Inspecao7i.class).carregarXml(file));
			}
		}
	}
}
