package action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.exception.GenericJDBCException;

import control.ErrorControl;

public class Login extends AbstractAction{
	public Object execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		try {
			model.Login login = new model.Login(request.getParameter("login"), request.getParameter("password"));
			
			HttpSession session = request.getSession();
			session.setAttribute("login", login);
			request.getRequestDispatcher("/main/inmetro/inmetro.jsp").forward(request, response);
			
			/*
			if(login.getLogin().isEmpty() || login.getSenha().isEmpty()){
				request.setAttribute("msg", "Por favor digite o login e a senha.");
				request.setAttribute("login", login.getLogin());
				request.getRequestDispatcher("/login.jsp").forward(request, response);
			}else{
				if(!login.getLogin().equals("oia") && !login.getLogin().equals("inmetro")){
					request.setAttribute("msg", "Login errado.");
					request.setAttribute("login", login.getLogin());
					request.getRequestDispatcher("/login.jsp").forward(request, response);
				}
				else if(login.getLogin().equals("oia") && !login.getSenha().equals("oiateste0207")){
					request.setAttribute("msg", "Senha errada.");
					request.setAttribute("login", login.getLogin());
					request.getRequestDispatcher("/login.jsp").forward(request, response);
				}
				else if(login.getLogin().equals("inmetro") && !login.getSenha().equals("inmetroteste0207")){
					request.setAttribute("msg", "Senha errada.");
					request.setAttribute("login", login.getLogin());
					request.getRequestDispatcher("/login.jsp").forward(request, response);
				}
				else{
					if(login.getLogin().equals("oia")){
						HttpSession session = request.getSession();
						session.setAttribute("login", login);
						request.getRequestDispatcher("/main/oia/enviar_inspecao.jsp").forward(request, response);
					}
					if(login.getLogin().equals("inmetro")){
						HttpSession session = request.getSession();
						session.setAttribute("login", login);
						request.getRequestDispatcher("/main/inmetro/inmetro.jsp").forward(request, response);
					}
				}
			}
			*/
			
			/*
			model.Login login = new model.Login(request.getParameter("login"), request.getParameter("password"));
			
			if(login.getLogin().isEmpty() || login.getSenha().isEmpty()){
				request.setAttribute("msg", "Por favor digite o login e a senha.");
				request.getRequestDispatcher("/login.jsp").forward(request, response);
			}else{
				model.Login selected = new DAO<model.Login>(model.Login.class).consultar("login", login.getLogin());
				if(selected == null){
					request.setAttribute("msg", "Login inexistente.");
					request.getRequestDispatcher("/login.jsp").forward(request, response);
				}else if(!selected.getSenha().equals(login.getSenha())){					
					request.setAttribute("msg", "Senha errada.");
					request.getRequestDispatcher("/login.jsp").forward(request, response);
				}else{
					HttpSession session = request.getSession();
					session.setAttribute("login", login);
					request.getRequestDispatcher("/main/enviar_inspecao.jsp").forward(request, response);
				}
			}
			*/	
		}catch (GenericJDBCException e) {
			new ErrorControl(e);
			request.setAttribute("msg", "Ocorreu um erro na conex�o com o banco de dados.");
			request.getRequestDispatcher("/login.jsp").forward(request, response);
		} /*catch (SQLException e) {
			new ErrorControl(e);
			request.setAttribute("msg", "Ocorreu um erro na consulta com o banco de dados.");
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}*/ catch (ServletException e) {
			new ErrorControl(e);
		} catch (IOException e) {
			new ErrorControl(e);
		} catch (Exception e) {
			new ErrorControl(e);
		}
		return null;
	}
}
