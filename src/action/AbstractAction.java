package action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class AbstractAction {
	public abstract Object execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException;
}
