package action;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Head;
import model.Inspecao1i;
import model.Inspecao3i;
import model.Inspecao6i;
import model.Inspecao7i;
import persistence.DAO;
import util.XmlIO;
import control.ErrorControl;

public class RefinoLista extends AbstractAction{
	public Object execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		Integer inspecao = Integer.parseInt(request.getParameter("inspecao"));
		String oia = request.getParameter("oia");
		String diainicio = request.getParameter("diainicio");
		String mesinicio = request.getParameter("mesinicio");
		String anoinicio = request.getParameter("anoinicio");
		String diafim = request.getParameter("diafim");
		String mesfim = request.getParameter("mesfim");
		String anofim = request.getParameter("anofim");
		
		File repositoryFiles = new File("C:\\CargasPerigosasWeb\\InspecoesValidadas\\");
		// File repositoryFiles = new File("/home/vagner/CargasPerigosasWeb/InspecoesValidadas/");
		// File repositoryFiles = new File("/home/ditel/CargasPerigosasWeb/InspecoesValidadas/");
		
		ArrayList<Head> head = new ArrayList<Head>();
		ArrayList<Inspecao7i> lista7i = new ArrayList<Inspecao7i>();
		ArrayList<Inspecao6i> lista6i = new ArrayList<Inspecao6i>();
		ArrayList<Inspecao3i> lista3i = new ArrayList<Inspecao3i>();
		ArrayList<Inspecao1i> lista1i = new ArrayList<Inspecao1i>();
		
		boolean refinaData = false;
		Date inicio = new Date();
		Date fim = new Date();
		/*
		if(!diainicio.equals("0") && !mesinicio.equals("0") && !anofim.equals("0") && !diafim.equals("0") && !mesfim.equals("0") && !anofim.equals("0")){
			if(Integer.parseInt(diainicio) < 10) diainicio = "0" + diainicio;
			if(Integer.parseInt(mesinicio) < 10) mesinicio = "0" + mesinicio;
			if(Integer.parseInt(anoinicio) < 10) anoinicio = "0" + anoinicio;
			if(Integer.parseInt(diafim) < 10) diafim = "0" + diafim;
			if(Integer.parseInt(mesfim) < 10) mesfim = "0" + mesfim;
			if(Integer.parseInt(anofim) < 10) anofim = "0" + anofim;
			
			inicio = FormatUtil.formatDate(diainicio + "-" + mesinicio + "-" + anoinicio);
			fim = FormatUtil.formatDate(diafim + "-" + mesfim + "-" + anofim);
			
			if(inicio.before(fim)){
				refinaData = true;
			}
		}
		*/
		
		switch (inspecao) {
		case 7:
			try{
				if(refinaData){
					lista7i = new DAO<Inspecao7i>(Inspecao7i.class).consultarTodos("Head", "dataInspecao", inicio, fim);
				}
				else{
					lista7i = new DAO<Inspecao7i>(Inspecao7i.class).consultarTodosOrderBy("head.cipp");
				}
			}catch (Exception e) {
				new ErrorControl(e);
			    for(File file : repositoryFiles.listFiles()){
		        	for(File f : file.listFiles()){	        		
			        	if(f.getName().contains("_7i.xml")) lista7i.add((Inspecao7i) new XmlIO<Inspecao7i>(Inspecao7i.class).carregarXml(f));
		        	}
		        }
			}
			break;
			
		case 6:
			try{
				if(refinaData){
					lista6i = new DAO<Inspecao6i>(Inspecao6i.class).consultarTodos("Head", "dataInspecao", inicio, fim);
				}
				else{
					lista6i = new DAO<Inspecao6i>(Inspecao6i.class).consultarTodosOrderBy("head.cipp");
				}
			}catch (Exception e) {
				new ErrorControl(e);
			    for(File file : repositoryFiles.listFiles()){
		        	for(File f : file.listFiles()){	        		
			        	if(f.getName().contains("_6i.xml")) lista6i.add((Inspecao6i) new XmlIO<Inspecao6i>(Inspecao6i.class).carregarXml(f));
		        	}
		        }
			}
			break;
			
		case 3:
			try{
				if(refinaData){
					lista3i = new DAO<Inspecao3i>(Inspecao3i.class).consultarTodos("Head", "dataInspecao", inicio, fim);
				}
				else{
					lista3i = new DAO<Inspecao3i>(Inspecao3i.class).consultarTodosOrderBy("head.cipp");
				}
			}catch (Exception e) {
				new ErrorControl(e);
			    for(File file : repositoryFiles.listFiles()){
		        	for(File f : file.listFiles()){	        		
			        	if(f.getName().contains("_3i.xml")) lista3i.add((Inspecao3i) new XmlIO<Inspecao3i>(Inspecao3i.class).carregarXml(f));
		        	}
		        }
			}
			break;
			
		case 1:
			try{
				if(refinaData){
					lista1i = new DAO<Inspecao1i>(Inspecao1i.class).consultarTodos("Head", "dataInspecao", inicio, fim);
				}
				else{
					lista1i = new DAO<Inspecao1i>(Inspecao1i.class).consultarTodosOrderBy("head.cipp");
				}
			}catch (Exception e) {
				new ErrorControl(e);
			    for(File file : repositoryFiles.listFiles()){
		        	for(File f : file.listFiles()){	        		
			        	if(f.getName().contains("_1i.xml")) lista1i.add((Inspecao1i) new XmlIO<Inspecao1i>(Inspecao1i.class).carregarXml(f));
		        	}
		        }		        
			}
			break;
			
		case 0:
			try{
				if(refinaData){
					lista7i = new DAO<Inspecao7i>(Inspecao7i.class).consultarTodos("Head", "dataInspecao", inicio, fim);
					lista6i = new DAO<Inspecao6i>(Inspecao6i.class).consultarTodos("Head", "dataInspecao", inicio, fim);
					lista3i = new DAO<Inspecao3i>(Inspecao3i.class).consultarTodos("Head", "dataInspecao", inicio, fim);
					lista1i = new DAO<Inspecao1i>(Inspecao1i.class).consultarTodos("Head", "dataInspecao", inicio, fim);
				}
				else{
					head = new DAO<Head>(Head.class).consultarTodosOrderBy("cipp");
				}
			}catch (Exception e) {
				new ErrorControl(e);
			    for(File file : repositoryFiles.listFiles()){
		        	for(File f : file.listFiles()){	        		
			        	if(f.getName().contains("_1i.xml")) lista1i.add((Inspecao1i) new XmlIO<Inspecao1i>(Inspecao1i.class).carregarXml(f));
			        	else if(f.getName().contains("_3i.xml")) lista3i.add((Inspecao3i) new XmlIO<Inspecao3i>(Inspecao3i.class).carregarXml(f));
			        	else if(f.getName().contains("_6i.xml")) lista6i.add((Inspecao6i) new XmlIO<Inspecao6i>(Inspecao6i.class).carregarXml(f));
			        	else if(f.getName().contains("_7i.xml")) lista7i.add((Inspecao7i) new XmlIO<Inspecao7i>(Inspecao7i.class).carregarXml(f));
		        	}
		        }		        
			}
			break;
			
		default:
			break;
		}
		
		if(!oia.isEmpty()){
			ArrayList<Inspecao7i> remove7i = new ArrayList<Inspecao7i>();
			for(Inspecao7i i : lista7i){
				if(!i.getHead().getOia().toUpperCase().contains(oia.toUpperCase())) remove7i.add(i);
			}
			lista7i.removeAll(remove7i);
			
			ArrayList<Inspecao6i> remove6i = new ArrayList<Inspecao6i>();
			for(Inspecao6i i : lista6i){
				if(!i.getHead().getOia().toUpperCase().contains(oia.toUpperCase())) remove6i.add(i);
			}
			lista6i.removeAll(remove6i);
			
			ArrayList<Inspecao3i> remove3i = new ArrayList<Inspecao3i>();
			for(Inspecao3i i : lista3i){
				if(!i.getHead().getOia().toUpperCase().contains(oia.toUpperCase())) remove3i.add(i);
			}
			lista3i.removeAll(remove3i);
			
			ArrayList<Inspecao1i> remove1i = new ArrayList<Inspecao1i>();
			for(Inspecao1i i : lista1i){
				if(!i.getHead().getOia().toUpperCase().contains(oia.toUpperCase())) remove1i.add(i);
			}
			lista1i.removeAll(remove1i);
		}
		
		for(Inspecao1i i : lista1i){
			head.add(i.getHead());
		}
		for(Inspecao3i i : lista3i){
			head.add(i.getHead());
		}
		for(Inspecao6i i : lista6i){
			head.add(i.getHead());
		}
		for(Inspecao7i i : lista7i){
			head.add(i.getHead());
		}
		
		request.setAttribute("head", head);
		
		request.setAttribute("inspecao", inspecao);
		request.setAttribute("oia", oia);
		request.setAttribute("diainicio", diainicio);
		request.setAttribute("mesinicio", mesinicio);
		request.setAttribute("anoinicio", anoinicio);
		request.setAttribute("diafim", diafim);
		request.setAttribute("mesfim", mesfim);
		request.setAttribute("anofim", anofim);
		
		request.getRequestDispatcher("/main/inmetro/inspecoes/lista_inspecaoes.jsp").forward(request, response);
		return null;
	}
}
