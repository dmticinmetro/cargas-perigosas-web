package action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Head;
import persistence.DAO;
import control.ErrorControl;

public class RelatorioSql extends AbstractAction {
	public Object execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String query = request.getParameter("query");
		DAO<Head> dao = new DAO<Head>(Head.class);
		
		try {
			if(!query.isEmpty()){
				ArrayList<Head> result = dao.querySQL(query);
				request.setAttribute("head", result);
				request.getRequestDispatcher("/main/inmetro/inspecoes/lista_inspecaoes.jsp").forward(request, response);
			}else{
				request.setAttribute("msg", "Digite um comando em SQL.");
				request.getRequestDispatcher("/main/inmetro/relatorios/relatorio_sql.jsp").forward(request, response);	
			}
		} catch (Exception e) {
			new ErrorControl(e);
			request.setAttribute("msg", "Ocorreu um erro.");
			request.getRequestDispatcher("/main/inmetro/relatorios/relatorio_sql.jsp").forward(request, response);
		}
		
		return null;
	}

}
