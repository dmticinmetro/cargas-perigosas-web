package util;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class JaxbDateFormat extends XmlAdapter<String, Date>{
 
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
 
    @Override
    public String marshal(Date date) throws Exception {
        return dateFormat.format(date);
    }
    
    @Override
    public Date unmarshal(String date) throws Exception {
    	String dia = date.split(" ")[2];
		String mes = "";
		
		if(date.split(" ")[1].toLowerCase().contains("jan") || date.split(" ")[1].toLowerCase().contains("janeiro")){
			mes = "01";	
		}
		if(date.split(" ")[1].toLowerCase().contains("fev") || date.split(" ")[1].toLowerCase().contains("fevereiro")){
			mes = "02";
		}
		if(date.toLowerCase().split(" ")[1].contains("mar") || date.toLowerCase().split(" ")[1].contains("marco") || date.toLowerCase().contains("mar�o")){
			mes = "03";
		}
		if(date.toLowerCase().split(" ")[1].contains("abr") || date.toLowerCase().split(" ")[1].contains("abril")){
			mes = "04";
		}
		if(date.toLowerCase().split(" ")[1].contains("mai") || date.toLowerCase().split(" ")[1].contains("maio")){
			mes = "05";
		}
		if(date.toLowerCase().split(" ")[1].contains("jun") || date.toLowerCase().split(" ")[1].contains("junho")){
			mes = "06";
		}
		if(date.toLowerCase().split(" ")[1].contains("jul") || date.toLowerCase().split(" ")[1].contains("julho")){
			mes = "07";
		}
		if(date.toLowerCase().split(" ")[1].contains("ago") || date.toLowerCase().split(" ")[1].contains("agosto")){
			mes = "08";
		}
		if(date.toLowerCase().split(" ")[1].contains("set") || date.toLowerCase().split(" ")[1].contains("setembro")){
			mes = "09";
		}
		if(date.toLowerCase().split(" ")[1].contains("out") || date.toLowerCase().split(" ")[1].contains("outubro")){
			mes = "10";
		}
		if(date.toLowerCase().split(" ")[1].contains("nov") || date.toLowerCase().split(" ")[1].contains("novembro")){
			mes = "11";
		}
		if(date.toLowerCase().contains("dez") || date.toLowerCase().contains("dezembro")){
			mes = "12";
		}
		
		String ano = date.split(" ")[3];
		
        return new SimpleDateFormat("dd-MM-yyyy").parse(dia + "-" + mes + "-" + ano);
    }
}