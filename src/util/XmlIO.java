package util;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;

import control.ErrorControl;

public class XmlIO<T> {
	private Class<T> c;
	
	public XmlIO(Class<T> c){
		this.c = c;
	}
	
	public Object carregarXml(File file){
		try{
			JAXBContext jaxbContext = JAXBContext.newInstance(c);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			Object object = unmarshaller.unmarshal(file);
			return object;
		}catch (UnmarshalException e) {
			new ErrorControl("Erro no arquivo XML.", e);
			return null;
		}catch (JAXBException e) {
			new ErrorControl("Erro na leitura do XML.", e);
			return null;
		}catch (Exception e) {
			new ErrorControl("Ocorreu um erro.", e);
			return null;
		}
	}
	
	public Object carregarXml(String fileName){
		try{
			File file = new File(fileName);
			JAXBContext jaxbContext = JAXBContext.newInstance(c);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			Object object = unmarshaller.unmarshal(file);
			return object;
		}catch (JAXBException e) {
			new ErrorControl("Erro na leitura do XML.", e);
			return null;
		}catch (Exception e) {
			new ErrorControl("Ocorreu um erro.", e);
			return null;
		}
	}
}
