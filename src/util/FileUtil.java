package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileUtil{
	public static void copyDirectory(File sourceDir, File destDir) throws IOException{
		if(!destDir.exists()){
			destDir.mkdir();
		}
		File[] children = sourceDir.listFiles();
		for(File sourceChild : children){
			String name = sourceChild.getName();
			File destChild = new File(destDir, name);
			if(sourceChild.isDirectory()){
				copyDirectory(sourceChild, destChild);
			}else{
				copyFile(sourceChild, destChild);
			}
		}
	}
	
	public static void cutDirectory(File sourceDir, File destDir) throws IOException{
		if(!destDir.exists()){
			destDir.mkdir();
		}
		File[] children = sourceDir.listFiles();
		for(File sourceChild : children){
			String name = sourceChild.getName();
			File destChild = new File(destDir, name);
			if(sourceChild.isDirectory()){
				copyDirectory(sourceChild, destChild);
			}else{
				copyFile(sourceChild, destChild);
			}
			sourceChild.delete();
		}
		sourceDir.delete();
	}
	
	public static void copyFile(File source, File dest) throws IOException{
		if(!dest.exists()){
			dest.createNewFile();
		}
		InputStream in = null;
		OutputStream out = null;
		try{
			in = new FileInputStream(source);
			out = new FileOutputStream(dest);
			byte[] buf = new byte[1024];
			int len;
			while((len = in.read(buf)) > 0){
				out.write(buf, 0, len);
			}
		}finally{
			in.close();
			out.close();
		}
	}
	
	public static void cutFile(File source, File dest) throws IOException{
		copyFile(source, dest);
		source.delete();
	}
	
	public static void deleteDirectory(File dir) throws IOException{
		if(dir.isDirectory()){
			for(File f : dir.listFiles()){
				f.delete();
			}
			dir.delete();
		}
	}
}
