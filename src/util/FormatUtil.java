package util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import control.ErrorControl;

/**
 * @author Vagner Praia da Silva
 */
public class FormatUtil {
	public static String formatGet(String string) {
		if(string.length() != 0){
			String[] palavra = string.split(" ");
			List<String> r = new ArrayList<String>();
			if (palavra[0].equalsIgnoreCase("DA")
					|| palavra[0].equalsIgnoreCase("DO")
					|| palavra[0].equalsIgnoreCase("DAS")
					|| palavra[0].equalsIgnoreCase("DOS")
					|| palavra[0].equalsIgnoreCase("DE")
					|| palavra[0].equalsIgnoreCase("A")
					|| palavra[0].equalsIgnoreCase("E")
					|| palavra[0].equalsIgnoreCase("O")) {
				r.add(palavra[0].substring(0, 1).toUpperCase()
						+ palavra[0].substring(1).toLowerCase());
			}
			for (int i = 0; i < palavra.length; i++) {
				if (palavra[i].equalsIgnoreCase("DA")
						|| palavra[i].equalsIgnoreCase("DO")
						|| palavra[i].equalsIgnoreCase("DAS")
						|| palavra[i].equalsIgnoreCase("DOS")
						|| palavra[i].equalsIgnoreCase("DE")
						|| palavra[i].equalsIgnoreCase("A")
						|| palavra[i].equalsIgnoreCase("E")
						|| palavra[i].equalsIgnoreCase("O")) {
					r.add(palavra[i].toLowerCase());
				} else {
					if (palavra[i].contains("'")) {
						String[] p = palavra[i].split("'");
						r.add(p[0].substring(0, 1) + "'"
								+ p[1].substring(0, 1).toUpperCase()
								+ p[1].substring(1).toLowerCase());
					} else {
						r.add(palavra[i].substring(0, 1).toUpperCase()
								+ palavra[i].substring(1).toLowerCase());
					}
				}
			}
			String retorno = r.get(0);
			if (r.size() > 1) {
				for (int i = 1; i < r.size(); i++) {
					retorno += " " + r.get(i);
				}
			}
			return retorno.trim();
		}else{
			return string;
		}
	}

	public static String formatSet(String string) {
		return string.trim().toUpperCase();
	}
	
	public static String formatDate(Date date){
		try {
			return new SimpleDateFormat("dd-MM-yyyy").format(date);
		} catch (Exception e) {
			new ErrorControl("Ocorreu um erro na formata��o da data", e);
			return null;
		}
	}
	
	public static Date formatDate(String date){ 
        try {
        	return new SimpleDateFormat("dd-MM-yyyy").parse(date);
		} catch (ParseException e) {
			new ErrorControl("Erro na formata��o da string para data", e);
			return null;
		} catch (Exception e) {
			new ErrorControl("Ocorreu um erro na formata��o da data", e);
			return null;
		}
	}
	
	public static Date formatDateExtensive(String date){
		String dia = date.split(" ")[2];
		String mes = "";
		
		if(date.toLowerCase().contains("jan") || date.toLowerCase().contains("janeiro")){
			mes = "01";	
		}
		if(date.toLowerCase().contains("fev") || date.toLowerCase().contains("fevereiro")){
			mes = "02";
		}
		if(date.toLowerCase().contains("mar") || date.toLowerCase().contains("marco") || date.toLowerCase().contains("mar�o")){
			mes = "03";
		}
		if(date.toLowerCase().contains("abr") || date.toLowerCase().contains("abril")){
			mes = "04";
		}
		if(date.toLowerCase().contains("mai") || date.toLowerCase().contains("maio")){
			mes = "05";
		}
		if(date.toLowerCase().contains("jun") || date.toLowerCase().contains("junho")){
			mes = "06";
		}
		if(date.toLowerCase().contains("jul") || date.toLowerCase().contains("julho")){
			mes = "07";
		}
		if(date.toLowerCase().contains("ago") || date.toLowerCase().contains("agosto")){
			mes = "08";
		}
		if(date.toLowerCase().contains("set") || date.toLowerCase().contains("setembro")){
			mes = "09";
		}
		if(date.toLowerCase().contains("out") || date.toLowerCase().contains("outubro")){
			mes = "10";
		}
		if(date.toLowerCase().contains("nov") || date.toLowerCase().contains("novembro")){
			mes = "11";
		}
		if(date.toLowerCase().contains("dez") || date.toLowerCase().contains("dezembro")){
			mes = "12";
		}
		
		String ano = date.split(" ")[3];
		
        try {
        	return new SimpleDateFormat("dd-MM-yyyy").parse(dia + "-" + mes + "-" + ano);
		} catch (ParseException e) {
			new ErrorControl("Erro na formata��o da string para data", e);
			return null;
		} catch (Exception e) {
			new ErrorControl("Ocorreu um erro na formata��o da data", e);
			return null;
		}
	}
	
	public static String formatDateHour(Date date){
        try {
        	return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(date);
		} catch (Exception e) {
			new ErrorControl("Ocorreu um erro na formata��o da data", e);
			return null;
		}
	}
	
	public static Date formatDateHour(String date){
        try {
        	return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(date);
		} catch (ParseException e) {
			new ErrorControl("Erro na formata��o da string para data", e);
			return null;
		} catch (Exception e) {
			new ErrorControl("Ocorreu um erro na formata��o da data", e);
			return null;
		}
	}
	
	public static String formatValue(Double valor){
		try{
			NumberFormat nf = new DecimalFormat("R$ ##0.00");
			return nf.format(valor);
		}catch (Exception e) {
			new ErrorControl("Ocorreu um erro na formata��o da data", e);
			return null;
		}
	}
	
	public static double formatValue(String valor){
		try{
			String[] v1 = valor.split(" ");
			String v2[] = v1[1].split(",");
			return Double.parseDouble(String.valueOf(v2[0] + "." + v2[1]));
		}catch (Exception e) {
			new ErrorControl("Ocorreu um erro na formata��o da data", e);
			return 0.0;
		}
	}
}