package util;

import java.io.File;
import java.io.IOException;

public class DirectoryUtil {
	public void createDirectory(String... directories) throws IOException{
		for(String d : directories){
			File dir = new File(d);
	        if(!dir.exists()){
	        	dir.mkdir();
	        }
			if(!dir.exists() || !dir.isDirectory()){
				throw new IOException("O diret�rio " + dir + " n�o � um diret�rio v�lido");
			}
		}		
	}
}
